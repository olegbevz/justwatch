﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace JustWatch.Common.Tests
{
    [TestFixture]
    public class StringExtensionsTests
    {
        [TestCase]
        public void ShouldNotRemoveRedundantWhiteSpacesFromNull()
        {
            string inputString = null;

            Assert.Throws<Exception>(() => inputString.RemoveRedundantWhiteSpaces());
        }

        [TestCase("", "")]
        [TestCase(" ", "")]
        [TestCase("  ", "")]
        [TestCase("Латунь ", "Латунь")]
        [TestCase(" Латунь ", "Латунь")]
        [TestCase("Латунь  ", "Латунь")]
        [TestCase(" Латунь", "Латунь")]
        [TestCase("  Латунь", "Латунь")]
        [TestCase("Механические  (автоматические)", "Механические (автоматические)")]
        [TestCase("Механические   (автоматические)", "Механические (автоматические)")]
        [TestCase("26x7.4 ", "26x7.4")]
        public void ShouldRemoveRedundantWhiteSpaces(string inputString, string outputString)
        {
            Assert.AreEqual(outputString, inputString.RemoveRedundantWhiteSpaces());
        }

        [TestCase("", "")]
        [TestCase("SM30137.02", "SM30137-02")]
        [TestCase("DL 2133S MW", "DL-2133S-MW")]
        [TestCase("16021/B", "16021-B")]
        [TestCase("DL 2133S MW(BK)", "DL-2133S-MW-BK")]
        [TestCase(" Romanson  DL5163SMW", "Romanson-DL5163SMW")]
        [TestCase("  Romanson DL5163SMW", "Romanson-DL5163SMW")]
        [TestCase("DL 2133S MW(BK) ", "DL-2133S-MW-BK")]
        public void ShouldReplaceUrlIncompatibleSymbols(string input, string output)
        {
            Assert.AreEqual(output, input.ReplaceUrlIncompatibleSymbols('-'));
        }
    }
}
