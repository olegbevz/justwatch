﻿namespace JustWatch.BestWatch
{
    public class Product
    {
        public string Type { get; set; }
        public string Brand { get; set; }
        public string Articul { get; set; }
        public string ModelLine { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }
        public string Sex { get; set; }
        public string Material { get; set; }
        public string Waterproof { get; set; }
        public string Glass { get; set; }
        public string Mechanism { get; set; }
        public double? Width { get; set; }
        public double? Depth { get; set; }
    }
}
