﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using JustWatch.Common;

namespace JustWatch.BestWatch
{
    public class ProductsDocumentReader
    {
        private readonly string fileName;

        public ProductsDocumentReader(string fileName)
        {
            this.fileName = fileName;
        }

        public IEnumerable<Product> ReadProducts()
        {
            using (var fileStream = File.OpenRead(this.fileName))
            {
                var xmlReaderSettings = new XmlReaderSettings
                {
                    MaxCharactersFromEntities = int.MaxValue,
                    MaxCharactersInDocument = int.MaxValue
                };

                var xmlReader = XmlReader.Create(fileName, xmlReaderSettings);
               
                xmlReader.ReadStartElement("products");

                while (xmlReader.ReadToNextSibling("product"))
                {
                    xmlReader.ReadStartElement("product");

                    var product = new Product();

                    while (!(xmlReader.NodeType == XmlNodeType.EndElement && xmlReader.Name == "product") && !xmlReader.EOF)
                    {
                        xmlReader.Read();

                        switch (xmlReader.Name)
                        {
                            case "type":
                                product.Type = xmlReader.ReadElementString("type"); 
                                break;
                            case "brand": 
                                product.Brand = xmlReader.ReadElementString("brand"); 
                                break;
                            case "art":
                                product.Articul = xmlReader.ReadElementString("art");
                                break;
                            case "modelline":
                                product.ModelLine = xmlReader.ReadElementString("modelline"); 
                                break;
                            case "image":
                                product.Image = xmlReader.ReadElementString("image"); 
                                break;
                            case "price":
                                product.Price = int.Parse(xmlReader.ReadElementString("price")); break;
                            case "sex":
                                product.Sex = xmlReader.ReadElementString("sex"); break;
                            case "material":
                                product.Material = xmlReader.ReadElementString("material"); break;
                            case "waterproof":
                                product.Waterproof = xmlReader.ReadElementString("waterproof"); break;
                            case "glass": 
                                product.Glass = xmlReader.ReadElementString("glass"); 
                                break;
                            case "mechanism":
                                product.Mechanism = xmlReader.ReadElementString("mechanism").RemoveRedundantWhiteSpaces(); 
                                break;
                            case "width":
                                var width = xmlReader.ReadElementString("width");
                                if (!string.IsNullOrEmpty(width))
                                    product.Width = double.Parse(width, CultureInfo.InvariantCulture); 
                                break;
                            case "depth":
                                var depth = xmlReader.ReadElementString("depth");
                                if (!string.IsNullOrEmpty(depth))
                                    product.Depth = double.Parse(depth, CultureInfo.InvariantCulture); 
                                break;
                        }
                    }
                    
                    xmlReader.ReadEndElement();

                    yield return product;
                }

                xmlReader.ReadEndElement();

                xmlReader.Close();
            }
        }
    }
}
