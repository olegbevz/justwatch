﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using JustWatch.Common;
using System.Text.RegularExpressions;
using FluentFTP;

namespace JustWatch.OpenCartExport
{
    public class ImageCatalog
    {
        private readonly IFtpClient ftpClient;
        private readonly string catalogPath;

        private Dictionary<string, string[]> catalog;
        private const string imageNameRegex = @"(\#\d+)?\.(jpg|png|jpeg)\z";

        public ImageCatalog(IFtpClient ftpClient, string catalogPath)
        {
            this.ftpClient = ftpClient;
            this.catalogPath = catalogPath;
        }

        public string[] GetImages(string brand, string model)
        {
            if (catalog == null)
            {
                catalog = ftpClient.GetListing(catalogPath)
                    .Select(x => x.Name)
                    .Where(x => string.IsNullOrEmpty(Path.GetExtension(x)))
                    .ToDictionary(x => x, x => (string[])null);
            }

            brand = brand.ReplaceUrlIncompatibleSymbols('-').ToLowerInvariant();

            if (!catalog.ContainsKey(brand))
            {
                return null;
            }

            if (catalog[brand] == null)
            {
                catalog[brand] = ftpClient.GetListing(string.Concat(catalogPath, "/", brand))
                    .Select(x => x.Name)
                    .ToArray();
            }

            model = model.ReplaceUrlIncompatibleSymbols('-').ToLowerInvariant();

            var regex = new Regex(string.Concat(model, imageNameRegex));
            return catalog[brand].Where(image => regex.IsMatch(image))
                .OrderBy(image => image.Length).ThenBy(image => image)
                .Select(image => string.Format("catalog/watch/{0}/{1}", brand, image))
                .ToArray();
        }
    }
}
