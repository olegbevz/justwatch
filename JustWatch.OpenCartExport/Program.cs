﻿using JustWatch.Common;
using OpenCart.Entities;
using JustWatch.OpenCartExport.Commands;
using JustWatch.WatchBook;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.IO;
using JustWatch.Prices;
using FluentFTP;
using System.Net;

namespace JustWatch.OpenCartExport
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ConsoleUtils.WriteApplicationInfo();

                using (var logger = new Logger())
                {
                    using (var openCartDomain = CreateOpenCartDomain(logger))
                    {
                        foreach (var command in GetOpenCartCommands(openCartDomain))
                        {
                            command.Execute(openCartDomain);

                            if (!command.IsReadOnly)
                            {
                                if (DisplayChanges(openCartDomain))
                                {
                                    SetProductsChangeTime(openCartDomain);
                                    SaveChanges(openCartDomain);
                                }
                            }

                            Console.WriteLine();
                        }
                    }
                }

                Console.WriteLine("OpenCart export successfully completed");
                ConsoleUtils.AskToPressAnyKey();
            }
            catch (Exception ex)
            {                
                ConsoleUtils.WriteException(ex);
                Common.Logger.LogException(ex);
                ConsoleUtils.AskToPressAnyKey();
            }
        }

        private static IEnumerable<IOpenCartCommand> GetOpenCartCommands(IOpenCartDomain openCartDomain)
        {
            yield return new WireUpConnection();

            var productMapper = CreateProductMapper(openCartDomain);
            var watchRepository = CreateWatchRepository();
            
            yield return new ExportProducts(productMapper, watchRepository);

            SaveProductsWithoutImage(productMapper.ProductsWithoutImages);

            yield return new DisableObsoleteProducts();

            yield return new UpdateUrlAliases();

            yield return new CheckDataIntegrity();

            yield return new CreateYmlDocument();
        }

        private static void SaveChanges(IOpenCartDomain openCartDomain)
        {
            try
            {
                Console.WriteLine("Pushing changes to OpenCart database...");

                openCartDomain.SaveChanges();

                Console.WriteLine("Changes were successfuly pushed to OpenCart database");
            }
            catch (DbEntityValidationException ex)
            {
                HandleValidationException(ex);
            }
        }

        private static void HandleValidationException(DbEntityValidationException ex)
        {
            var validationResult = ex.EntityValidationErrors.FirstOrDefault();
            if (validationResult != null)
            {
                var errorMessage = new StringBuilder();
                errorMessage.AppendLine("Database validation error");
                foreach (var error in validationResult.ValidationErrors)
                    errorMessage.AppendLine($"{error.PropertyName}: {error.ErrorMessage}");

                throw new Exception(errorMessage.ToString());
            }

            throw ex;
        }

        private static bool DisplayChanges(IOpenCartDomain openCartDomain)
        {
            Console.WriteLine("Collecting change tracker data...");

            var context = openCartDomain as DbContext;
            var changeTracker = context.ChangeTracker;

            if (changeTracker.HasChanges())
            {
                var entries = changeTracker.Entries().ToArray();

                foreach (var entities in entries.GroupBy(x => GetTypeOrBaseType(x.Entity)))
                {
                    if (entities.All(x => x.State == EntityState.Unchanged || x.State == EntityState.Detached))
                        continue;

                    Console.WriteLine(string.Format("{0}: {1} added, {2} modified, {3} deleted",
                        entities.Key.Name,
                        entities.Count(x => x.State == EntityState.Added),
                        entities.Count(x => x.State == EntityState.Modified),
                        entities.Count(x => x.State == EntityState.Deleted)));
                }
                return true;
            }

            Console.WriteLine("No changes has been detected.");
            return false;
        }

        private static Type GetTypeOrBaseType(object entity)
        {
            var type = entity.GetType();
            if (type.Name.Contains('_') && type.BaseType != null)
                return type.BaseType;

            return type;
        }

        private static void SetProductsChangeTime(IOpenCartDomain openCartDomain)
        {
            Console.WriteLine("Assign change time for products...");

            var context = openCartDomain as DbContext;
            var changeTracker = context.ChangeTracker;

            var currentTime = DateTime.Now;

            var productEntries = changeTracker.Entries<Product>();

            foreach (var addedProduct in productEntries.Where(x => x.State == EntityState.Added).Select(x => x.Entity))
            {
                addedProduct.DateAdded = currentTime;
                addedProduct.DateModified = currentTime;
            }

            foreach (var modifiedProduct in productEntries.Where(x => x.State == EntityState.Modified).Select(x => x.Entity))
            {
                modifiedProduct.DateModified = currentTime;
            }
        }

        private static WatchRepository CreateWatchRepository()
        {
            return new WatchRepository(PathUtils.CombineWithCurrentDirectory(AppSettings.WatchBook));
        }

        private static WatchToProductMapper CreateProductMapper(IOpenCartDomain openCartDomain)
        {
            var priceProvider = CreatePriceProvider();

            var imageCatalog = CreateImageCatalog();

            var mapper = new WatchToProductMapper(
                openCartDomain,
                priceProvider,
                imageCatalog);

            mapper.PaymentText = GetPaymentText();
            mapper.ShippingText = GetShippingText();

            return mapper;
        }

        private static IPriceProvider CreatePriceProvider()
        {
            var deltaPriceReader = new DeltaPriceReader(PathUtils.CombineWithCurrentDirectory(AppSettings.DeltaPrices));
            var bestWatchPriceReader = new BestWatchPriceReader(PathUtils.CombineWithCurrentDirectory(AppSettings.BestWatchProducts));
            var compositePriceReader = new CompositePriceReader(deltaPriceReader, bestWatchPriceReader);

            var brandFormulaRepository = new BrandFormulaRepository(PathUtils.CombineWithCurrentDirectory(AppSettings.PriceCalculation));
            var priceCalculator = new PriceCalculator(brandFormulaRepository.GetAll());

            return new PriceCalculationProvider(new PriceDictionary(compositePriceReader), priceCalculator);
        }

        private static ImageCatalog CreateImageCatalog()
        {
            var ftpClient = new FtpClient(
                AppSettings.FtpHost,
                new NetworkCredential(AppSettings.FtpUser, AppSettings.FtpPassword));

            ftpClient.Connect();

            return new ImageCatalog(ftpClient, AppSettings.FtpImageDirectory);
        }

        private static string GetPaymentText()
        {
            return File.ReadAllText(PathUtils.CombineWithCurrentDirectory(AppSettings.PaymentText));
        }

        private static string GetShippingText()
        {
            return File.ReadAllText(PathUtils.CombineWithCurrentDirectory(AppSettings.ShippingText));
        }

        private static IOpenCartDomain CreateOpenCartDomain(Logger logger)
        {
            var openCartDomain = new OpenCartDomain();

            openCartDomain.Database.CommandTimeout = (int)TimeSpan.FromMinutes(10).TotalSeconds;
            openCartDomain.Database.Log = logger.Log;

            return openCartDomain;
        }

        private static void SaveProductsWithoutImage(IEnumerable<Product> products)
        {
            File.WriteAllLines(
                PathUtils.CombineWithCurrentDirectory(AppSettings.ProductsWithoutImage),
                products.Select(product => string.Format("{0} {1}", product.Manufacturer.Name, product.Model)));
        }
    }
}
