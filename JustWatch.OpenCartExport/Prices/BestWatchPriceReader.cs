﻿using System.Collections.Generic;
using System.Linq;
using JustWatch.BestWatch;

namespace JustWatch.OpenCartExport
{
    internal sealed class BestWatchPriceReader : IProductPriceReader
    {
        private readonly ProductsDocumentReader documentReader;

        public BestWatchPriceReader(string fileName)
        {
            this.documentReader = new ProductsDocumentReader(fileName);
        }

        public IEnumerable<ProductPrice> ReadPrices()
        {
            return documentReader.ReadProducts().Select(x => new ProductPrice(x.Brand, x.Articul, x.Price));
        }
    }
}