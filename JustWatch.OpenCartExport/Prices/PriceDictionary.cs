﻿using System;
using JustWatch.Common;

namespace JustWatch.OpenCartExport
{
    internal sealed class PriceDictionary : CacheBase<ProductPrice>, IPriceProvider
    {
        public PriceDictionary(IProductPriceReader reader) : base(reader.ReadPrices())
        {
        }

        public double? GetPrice(string brand, string model)
        {
            var productPrice = this.GetByKey(CreateKey(brand, model));

            return productPrice?.Price;
        }

        protected override string CreateKey(ProductPrice priceRecord)
        {
            return CreateKey(priceRecord.Brand, priceRecord.Model);
        }

        private string CreateKey(string brand, string model)
        {
            if (string.IsNullOrEmpty(brand))
                throw new Exception("Не задано название бренда.");

            if (string.IsNullOrEmpty(model))
                throw new Exception(string.Format("Не задано название модели часов с брендом {0}.", brand));

            return brand.Simplify() + model.Simplify();
        }

        protected override void OnDuplicateKeyCreated(string key, ProductPrice element)
        {
            // Do nothing
        }
    }
}