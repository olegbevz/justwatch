﻿using System.Collections.Generic;

namespace JustWatch.OpenCartExport
{
    internal interface IProductPriceReader
    {
        IEnumerable<ProductPrice> ReadPrices();
    }
}