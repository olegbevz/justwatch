﻿namespace JustWatch.OpenCartExport
{
    interface IPriceProvider
    {
        double? GetPrice(string brand, string model);
    }
}
