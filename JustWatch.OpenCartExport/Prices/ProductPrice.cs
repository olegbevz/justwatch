﻿namespace JustWatch.OpenCartExport
{
    internal class ProductPrice
    {
        public ProductPrice(string brand, string model, double price)
        {
            Brand = brand;
            Model = model;
            Price = price;
        }

        public string Brand { get; private set; }

        public string Model { get; private set; }

        public double Price { get; private set; }
    }
}