﻿using System.Collections.Generic;
using System.Linq;

namespace JustWatch.OpenCartExport
{
    internal sealed class CompositePriceReader : IProductPriceReader
    {
        private readonly IEnumerable<IProductPriceReader> innerReaders;

        public CompositePriceReader(params IProductPriceReader[] priceProviders) : this(priceProviders.AsEnumerable())
        {
        }

        public CompositePriceReader(IEnumerable<IProductPriceReader> priceProviders)
        {
            this.innerReaders = priceProviders;
        }

        public IEnumerable<ProductPrice> ReadPrices()
        {
            return innerReaders.SelectMany(x => x.ReadPrices());
        }
    }
}