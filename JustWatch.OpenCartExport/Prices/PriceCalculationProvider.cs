﻿using JustWatch.Prices;

namespace JustWatch.OpenCartExport
{
    internal sealed class PriceCalculationProvider : IPriceProvider
    {
        private readonly IPriceProvider priceProvider;
        private readonly PriceCalculator calculator;

        public PriceCalculationProvider(IPriceProvider priceProvider, PriceCalculator calculator)
        {
            this.priceProvider = priceProvider;
            this.calculator = calculator;
        }

        public double? GetPrice(string brand, string model)
        {
            var originalPrice = priceProvider.GetPrice(brand, model);

            if (originalPrice != null)
            {
                return calculator.CalculatePrice(brand, originalPrice.Value);
            }

            return null;
        }
    }
}