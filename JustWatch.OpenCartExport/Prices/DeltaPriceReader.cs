﻿using System.Collections.Generic;
using System.Linq;
using JustWatch.Delta;

namespace JustWatch.OpenCartExport
{
    internal sealed class DeltaPriceReader : IProductPriceReader
    {
        private readonly PricesReader pricesReader;

        public DeltaPriceReader(string folderName)
        {
            this.pricesReader = new PricesReader(folderName);
        }

        public IEnumerable<ProductPrice> ReadPrices()
        {
            return pricesReader.ReadPrices().Select(x => new ProductPrice(x.Brand, x.Model, x.Price));
        }
    }
}