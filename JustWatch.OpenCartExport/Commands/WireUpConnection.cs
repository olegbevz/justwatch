﻿using System;
using System.Linq;
using OpenCart.Entities;

namespace JustWatch.OpenCartExport.Commands
{
    class WireUpConnection : ReadOnlyCommand
    {
        public override void Execute(IOpenCartDomain openCartDomain)
        {
            Console.WriteLine("Connecting to OpenCart database...");
            openCartDomain.Manufactorers.ToArray();
            Console.WriteLine("Connection to OpenCart database is established.");
        }
    }
}
