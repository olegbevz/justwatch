﻿using System;
using System.Linq;
using OpenCart.Entities;

namespace JustWatch.OpenCartExport.Commands
{
    class CheckDataIntegrity : ReadOnlyCommand
    {
        public override void Execute(IOpenCartDomain openCartDomain)
        {
            Console.WriteLine("Checking data integrity...");

            foreach (var manufacturer in openCartDomain.Manufactorers)
            {
                if (!manufacturer.Products.Any(x => x.Status))
                {
                    Console.WriteLine(
                        "Отсутствуют товары для производителя \"{0}\". Удалите производителя или добавьте для него товары.",
                        manufacturer.Name);
                }
            }

            foreach (var category in openCartDomain.Categories)
            {
                if (!category.Categories.Any() && !category.Products.Any())
                {
                    var russian = openCartDomain.Languages.FindByName("Russian");

                    Console.WriteLine(
                        "Отсутствуют товары для категории \"{0}\". Удалите категорию или добавьте в нее товары.",
                        category.Descriptions.Single(x => x.LanguageId == russian.Id).Name);
                }
            }
        }
    }
}
