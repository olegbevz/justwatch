﻿using OpenCart.Entities;

namespace JustWatch.OpenCartExport.Commands
{
    interface IOpenCartCommand
    {
        bool IsReadOnly { get; }

        void Execute(IOpenCartDomain openCartDomain);
    }
}
