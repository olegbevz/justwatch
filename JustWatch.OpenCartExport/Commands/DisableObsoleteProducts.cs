﻿using System;
using OpenCart.Entities;
using JustWatch.WatchBook;
using JustWatch.Common;
using System.Data.Entity;

namespace JustWatch.OpenCartExport.Commands
{
    class DisableObsoleteProducts : CommandBase
    {
        public override void Execute(IOpenCartDomain openCartDomain)
        {
            Console.WriteLine("Disabling obsolete products...");

            var watchRepository = CreateWatchRepository();

            var watchBook = new WatchCache(watchRepository.GetWatches());

            foreach (var product in openCartDomain.Products.Include(x => x.Manufacturer))
            {
                var watch = watchBook.GetWatch(product.Manufacturer.Name, product.Model);
                if (watch == null && product.Status)
                {
                    product.Status = false;
                }
            }
        }

        private static WatchRepository CreateWatchRepository()
        {
            return new WatchRepository(PathUtils.CombineWithCurrentDirectory(AppSettings.WatchBook));
        }
    }
}
