﻿using System;
using System.Linq;
using OpenCart.Entities;
using System.Collections.ObjectModel;
using JustWatch.Common;

namespace JustWatch.OpenCartExport.Commands
{
    class UpdateUrlAliases : CommandBase
    {
        public override void Execute(IOpenCartDomain openCartDomain)
        {
            Console.WriteLine("Updating url aliases for products...");

            var urlAliasDictionary = openCartDomain.UrlAliases
                .Where(alias => alias.Query.StartsWith("product_id="))
                .ToDictionary(alias => alias.Query, alias => alias);
            var urlAliasesToAdd = new Collection<UrlAlias>();

            foreach (var product in openCartDomain.Products)
            {
                if (string.IsNullOrEmpty(product.Model))
                    throw new Exception("Could not create url alias for a product without model name.");

                var query = string.Format("product_id={0}", product.Id);
                var keyword = product.Model.ReplaceUrlIncompatibleSymbols('-').ToLowerInvariant();

                UrlAlias urlAlias;
                if (urlAliasDictionary.TryGetValue(query, out urlAlias))
                {
                    urlAlias.Keyword = keyword;
                }
                else
                {
                    urlAliasesToAdd.Add(new UrlAlias(query, keyword));
                }
            }

            openCartDomain.UrlAliases.AddRange(urlAliasesToAdd);
        }
    }
}
