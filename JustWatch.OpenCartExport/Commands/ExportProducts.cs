﻿using OpenCart.Entities;
using JustWatch.WatchBook;
using System;
using System.Linq;
using JustWatch.Common;

namespace JustWatch.OpenCartExport.Commands
{
    class ExportProducts : CommandBase
    {
        private readonly WatchToProductMapper mapper;
        private readonly WatchRepository watchRepository;

        public ExportProducts(WatchToProductMapper mapper, WatchRepository watchRepository)
        {
            this.mapper = mapper;
            this.watchRepository = watchRepository;
        }

        public override void Execute(IOpenCartDomain openCartDomain)
        {
            foreach (var manufacturer in openCartDomain.Manufactorers.OrderBy(x => x.Name))
            {
                Console.WriteLine($"Loading products for '{manufacturer.Name}' manufaturer from OpenCart...");
                var productCache = new OpenCartProductCache(openCartDomain, manufacturer);
                productCache.Init();

                Console.WriteLine($"Updating products of '{manufacturer.Name}' manufaturer...");

                var manufacturerHasProducts =  false;

                foreach (var watch in watchRepository.GetWatches().Where(watch => watch.Brand == manufacturer.Name))
                {
                    var existingProduct = productCache.GetProduct(watch.Brand, watch.Model);
                    if (existingProduct != null)
                    {
                        mapper.MapForward(existingProduct, watch);
                    }
                    else
                    {
                        openCartDomain.Products.Add(
                            mapper.MapForward(watch));
                    }

                    manufacturerHasProducts = true;
                }

                if (!manufacturerHasProducts)
                {
                    throw new Exception($"Отсутствуют товары для производителя \"{manufacturer.Name}\". Удалите производителя или добавьте для него товары.");
                }
            }
        }
    }
}
