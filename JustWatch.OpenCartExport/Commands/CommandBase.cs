﻿using OpenCart.Entities;

namespace JustWatch.OpenCartExport.Commands
{
    abstract class CommandBase : IOpenCartCommand
    {
        public virtual bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public abstract void Execute(IOpenCartDomain openCartDomain);
    }
}
