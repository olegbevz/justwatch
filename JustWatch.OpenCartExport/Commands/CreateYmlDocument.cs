﻿using System;
using OpenCart.Entities;
using System.Linq;
using JustWatch.EbayExport.YmlDocument;
using JustWatch.Yml;
using JustWatch.Common;
using System.Data.Entity;
using System.IO;
using System.Text;
using JustWatch.OpenCartExport.Mapping;
using FluentFTP;
using System.Net;

namespace JustWatch.OpenCartExport.Commands
{
    class CreateYmlDocument : ReadOnlyCommand
    {
        public override void Execute(IOpenCartDomain openCartDomain)
        {
            using (var memoryStream = new MemoryStream())
            {
                Console.WriteLine("Creating yml document based on OpenCart database...");
                CreateYmlStream(openCartDomain, memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);

                Console.WriteLine("Validating yml document...");
                ValidateYmlStream(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);

                Console.WriteLine("Exporting yml document to ftp server...");
                UploadStreamToFtpServer(memoryStream);
                Console.WriteLine($"Document '{AppSettings.YmlFileName}' is successfully exported to ftp server.");
            }
        }

        private static void CreateYmlStream(IOpenCartDomain openCartDomain, MemoryStream memoryStream)
        {
            using (var documentWriter = new YmlDocumentWriter(memoryStream))
            {
                var shop = new Shop("justwatches.ru", "justwatches.ru", "https://justwatches.ru");
                shop.Currencies = new[] { new Yml.Currency("RUR", 1) };
                shop.Categories = new[] { new Yml.Category(14324, "г. Санкт-Петербург. Телефон для связи: +7-999-219-4135") };

                documentWriter.WriteHeader(shop);

                var mapper = new ProductToOfferMapper(openCartDomain);

                foreach (var product in openCartDomain.Products
                    .Include(product => product.Manufacturer)
                    .Include(product => product.Attributes)
                    .Where(product => product.Status && product.Quantity > 0 && product.Price > 0))
                {
                    documentWriter.WriteOffer(mapper.MapForward(product));
                }

                documentWriter.WriteFooter();
            }
        }

        private static void UploadStreamToFtpServer(Stream stream)
        {
            var credentials = new NetworkCredential(AppSettings.FtpUser, AppSettings.FtpPassword);

            using (var ftpClient = new FtpClient(AppSettings.FtpHost, credentials))
            {
                ftpClient.Connect();

                ftpClient.Upload(
                    stream,
                    Path.Combine(AppSettings.FtpExportDirectory, AppSettings.YmlFileName),
                    FtpExists.Overwrite);
            }
        }

        private static void ValidateYmlStream(Stream stream)
        {
            var schemaValidator = new DtdSchemaValidator();
            var validationResult = schemaValidator.Validate(stream);
            if (validationResult.Errors.Count > 0)
            {
                var messageBuilder = new StringBuilder();
                messageBuilder.AppendLine("Схема yml документа некорректна.");
                foreach (var error in validationResult.Errors)
                    messageBuilder.AppendLine(error);
                throw new Exception(messageBuilder.ToString());
            }
            else
            {
                Console.WriteLine("Схема yml документа корректна.");
            }
        }
    }
}
