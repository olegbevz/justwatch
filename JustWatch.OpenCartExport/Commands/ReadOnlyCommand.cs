﻿namespace JustWatch.OpenCartExport.Commands
{
    abstract class ReadOnlyCommand : CommandBase
    {
        public override bool IsReadOnly
        {
            get
            {
                return true;
            }
        }
    }
}
