﻿using JustWatch.Common;
using System;
using System.IO;

namespace JustWatch.OpenCartExport
{
    /// <summary>
    /// The most simple logger
    /// </summary>
    internal class Logger : IDisposable
    {
        private const string FileName = "log.txt";

        private readonly FileStream fileStream;

        private readonly StreamWriter writer;

        public Logger()
        {
            fileStream = File.Open(
                PathUtils.CombineWithCurrentDirectory(FileName), 
                FileMode.Create, 
                FileAccess.Write, 
                FileShare.Read);

            writer = new StreamWriter(fileStream);
        }

        public void Log(string message)
        {
            writer.WriteLine(message);
        }

        public void Dispose()
        {
            fileStream.Flush();
            fileStream.Dispose();
        }
    }
}
