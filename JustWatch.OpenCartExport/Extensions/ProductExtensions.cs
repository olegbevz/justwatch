﻿using System.Linq;
using OpenCart.Entities;

namespace JustWatch.OpenCartExport
{
    public static class ProductExtensions
    {
        public static void AddToCategory(this Product product, Category category, bool isMainCategory = false)
        {
            var existing = product.Categories.FirstOrDefault(x => x.CategoryId == category.Id);
            if (existing != null)
            {
                existing.IsMainCategory = isMainCategory;
            }
            else
            {
                product.Categories.Add(new ProductToCategory(category, isMainCategory));
            }
        }

        public static bool RemoveFromCategory(this Product product, Category category)
        {
            var existing = product.Categories.FirstOrDefault(x => x.CategoryId == category.Id);
            if (existing != null)
                return product.Categories.Remove(existing);

            return false;
        }

        public static void SetImage(this Product product, string image, int sortOrder)
        {
            var productImage = product.Images.FirstOrDefault(x => x.Image == image);
            if (productImage == null)
                product.Images.Add(new ProductImage(image, sortOrder));
            else
                productImage.SortOrder = sortOrder;
        }
    }
}