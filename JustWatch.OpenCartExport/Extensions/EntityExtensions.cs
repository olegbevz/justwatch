﻿using OpenCart.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JustWatch.OpenCartExport
{
    public static class EntityExtensions
    {
        public static T Localize<T>(this IEnumerable<T> localizableObjects, Language language) where T : Localizable
        {
            return localizableObjects.FirstOrDefault(x => x.LanguageId == language.Id);
        }

        public static Category FindByName(this IEnumerable<Category> categories, string name, Language language)
        {
            return categories.FirstOrDefault(category => category.Descriptions.GetName(language) == name);
        }
    }
}
