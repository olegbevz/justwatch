﻿using JustWatch.Common;
using OpenCart.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace JustWatch.OpenCartExport
{
    public class OpenCartProductCache : CacheBase<Product>
    {
        public OpenCartProductCache(IOpenCartDomain openCartDomain, Manufacturer manufacturer) 
            :this(LoadProducts(openCartDomain, manufacturer))
        {
        }

        public OpenCartProductCache(IEnumerable<Product> products) : base(products)
        {
        }

        public Product GetProduct(string manufacturer, string model)
        {
            return GetByKey(CreateKey(manufacturer, model));
        }

        protected override string CreateKey(Product product)
        {
            return CreateKey(product.Manufacturer.Name, product.Model);
        }

        private string CreateKey(string manufacturer, string model)
        {
            return manufacturer.Simplify() + model.Simplify();
        }

        private static IEnumerable<Product> LoadProducts(IOpenCartDomain openCartDomain, Manufacturer manufacturer)
        {
            return openCartDomain.Products
                .Include(x => x.Attributes)
                .Include(x => x.Categories)
                .Include(x => x.Descriptions)
                .Include(x => x.ExtraTabs)
                .Include(x => x.Images)
                .Include(x => x.Layouts)
                .Include(x => x.Manufacturer)
                .Include(x => x.Stores)
                .Where(x => x.Manufacturer.Id == manufacturer.Id);
        }

        protected override void OnDuplicateKeyCreated(string key, Product product)
        {
            throw new Exception($"Duplicate product in OpenCart database: {product.Manufacturer.Name} {product.Model}");            
        }
    }
}
