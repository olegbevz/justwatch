﻿using JustWatch.Common;
using OpenCart.Entities;
using JustWatch.WatchBook;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data.Entity;
using System.Collections.ObjectModel;
using System.Text;

using Attribute = OpenCart.Entities.Attribute;

namespace JustWatch.OpenCartExport
{
    internal class WatchToProductMapper : IMapper<Product, Watch>
    {
        private readonly Category handWatchCategory;
        private readonly CategoryTree femaleCategoryTree;
        private readonly CategoryTree maleCategoryTree;

        private readonly StockStatus availableStatus;
        private readonly StockStatus absentStatus;
        private readonly Language russian;
        private readonly WeightClass weightUnit;
        private readonly LengthClass lenghtUnit;
        private readonly Store defaultStore;
        
        private readonly Dictionary<string, Attribute> attributes;
        private readonly Dictionary<string, Manufacturer> manufacturers;
        private readonly Dictionary<string, ExtraTab> extraTabs;

        private readonly IPriceProvider priceProvider;
        private readonly ImageCatalog imageCatalog;

        private readonly ExtraTab paymentExtraTab;
        private readonly ExtraTab shippingExtraTab;

        private readonly ProductSticker newSticker;

        public string PaymentText { get; set; }
        public string ShippingText { get; set; }
        

        public Collection<Product> ProductsWithoutImages = new Collection<Product>();

        public WatchToProductMapper(
            IOpenCartDomain openCartDomain, 
            IPriceProvider priceProvider,
            ImageCatalog imageCatalog)
        {
            russian = openCartDomain.Languages.FindByName("Russian");
            var categories = openCartDomain.Categories.Include(x => x.Descriptions).Include(x => x.Categories).ToArray();
            handWatchCategory = categories.FindByName("Наручные часы", russian);

            var builder = new CategoryTreeBuilder();
            maleCategoryTree = builder.Build(categories.FindByName("Мужские часы", russian), russian);
            femaleCategoryTree = builder.Build(categories.FindByName("Женские часы", russian), russian);

            var statuses = openCartDomain.StockStatuses.ToArray();
            availableStatus = statuses.FindByName("В наличии");
            absentStatus = statuses.FindByName("Нет в наличии");

            attributes = new Dictionary<string, Attribute>();
            foreach (var attribute in openCartDomain.AttributeGroups
                .SelectMany(groups => groups.Attributes).Include(x => x.Descriptions))
            {
                var attributeName = attribute.Descriptions.Single(x => x.LanguageId == russian.Id).Name;
                attributes.Add(attributeName, attribute);
            }

            manufacturers = openCartDomain.Manufactorers.AsEnumerable().ToDictionary(x => x.Name.ToLowerInvariant(), x => x);

            weightUnit = openCartDomain.WeightUnits.Single(unit => unit.Value == 1);
            lenghtUnit = openCartDomain.LengthUnits.Single(unit => unit.Value == 1);

            defaultStore = openCartDomain.Stores.FindByName("justwatches");

            var extraTabs = openCartDomain.ExtraTabs.Include(x => x.Descriptions).ToArray();
            paymentExtraTab = extraTabs.Single(x => x.Descriptions.GetTitle(russian) == "Оплата");
            shippingExtraTab = extraTabs.Single(x => x.Descriptions.GetTitle(russian) == "Доставка");
            this.extraTabs = extraTabs.ToDictionary(x => x.Descriptions.GetTitle(russian), x => x);
            
            this.newSticker = openCartDomain.Stickers.Include(x => x.Descriptions)
                .AsEnumerable()
                .Single(x => x.Descriptions.GetTitle(russian) == "Новые");

            this.priceProvider = priceProvider;
            this.imageCatalog = imageCatalog;
        }

        public Product MapForward(Watch source)
        {
            var product = new Product();
            MapForward(product, source);
            return product;
        }

        public void MapForward(Product product, Watch watch)
        {
            product.Model = watch.Model;

            SetProductDefaultValues(product);
            SetProductManufacturer(product, watch);
            SetProductPrice(product, watch);
            SetProductDescription(product, watch);
            SetProductImages(product, watch);
            SetProductAttributes(product, watch);
            SetProductCategories(product, watch);
            SetProductStore(product);
            SetProductStatus(product);
            SetProductExtraTabs(product, watch);
            SetProductSticker(product);

            product.Layouts.Clear();
        }

        private void SetProductSticker(Product product)
        {
            product.Stickers = string.Concat("a:1:{i:3;s:1:\"", this.newSticker.Id, "\";}");  //string.Format("a:1:{i:3;s:1:\"{0}\";}", this.newSticker.Id);
        }

        private void SetProductStore(Product product)
        {
            if (product.Stores.All(x => x.Id != defaultStore.Id))
                product.Stores.Add(defaultStore);
        }

        private void SetProductManufacturer(Product product, Watch watch)
        {
            Manufacturer manufacturer = null;

            if (!manufacturers.TryGetValue(watch.Brand.ToLowerInvariant(), out manufacturer))
                throw new Exception($"В базе данных OpenCart отсутствует производитель '{watch.Brand}'.");

            product.ManufacturerId = manufacturer.Id;
        }

        private void SetProductDefaultValues(Product product)
        {
            product.SKU = string.Empty;
            product.UPC = string.Empty;
            product.EAN = string.Empty;
            product.JAN = string.Empty;
            product.ISBN = string.Empty;
            product.MPN = string.Empty;
            product.Location = string.Empty;
            product.Stickers = string.Empty;
            product.Shipping = true;
            product.Points = 0;
            product.Weight = 0;
            product.WeightUnitId = weightUnit.Id;
            product.Length = 0;
            product.Height = 0;
            product.Width = 0;
            product.LengthUnitId = lenghtUnit.Id;
            product.SortOrder = 0;
            product.Subtract = false;
            product.Minimum = 1;
        }

        private void SetProductPrice(Product product, Watch watch)
        {
            var productPrice = priceProvider.GetPrice(watch.Brand, watch.Model);
            if (productPrice != null)
            {
                product.Price = (decimal) productPrice.Value;
                product.StockStatusId = availableStatus.Id;
                product.Quantity = 5;
            }
            else
            {
                product.StockStatusId = absentStatus.Id;
                product.Quantity = 0;
            }
        }

        private void SetProductStatus(Product product)
        {
            product.Status = product.Price > 0 && product.Image != null;
        }

        #region Product images assing logic

        private void SetProductImages(Product product, Watch watch)
        {
            var images = imageCatalog.GetImages(watch.Brand, watch.Model);

            if (images != null && images.Length >= 1)
            {
                product.Image = images[0];
            }
            else
            {
                product.Image = null;
                ProductsWithoutImages.Add(product);
            }

            if (images != null && images.Length > 1)
            {
                int sortOrder = 1;

                foreach (var image in images.Skip(1))
                {
                    product.SetImage(image, sortOrder);
                    sortOrder++;
                }
            }
            else
            {
                foreach (var productImage in product.Images.ToArray())
                {
                    // TODO: Configure correct produc image dependency
                    product.Images.Remove(productImage);
                    productImage.ProductId = -1;
                    productImage.Product = null;
                }
            }
        }

        #endregion

        #region Product attributes assign logic

        private void SetProductAttributes(Product product, Watch watch)
        {
            SetProductAttribute(product, "Пол", watch.Gender.HasValue ? Formatting.FormatWatchGender(watch.Gender.Value) : null);
            SetProductAttribute(product, "Механизм", watch.Movement.HasValue ? Formatting.FormatWatchMovement(watch.Movement.Value) : null);
            SetProductAttribute(product, "Цифры", watch.DialMarkers != null ? watch.DialMarkers.ToLower() : null);
            SetProductAttribute(product, "Отображение времени", watch.DialType.HasValue ? Formatting.FormatWatchDialType(watch.DialType.Value) : null);
            SetProductAttribute(product, "Питание", watch.EnergySource != null ? watch.EnergySource.ToLower() : null);

            SetProductAttribute(product, "Водозащита", watch.WaterResistance);
            SetProductAttribute(product, "Материал корпуса", watch.CaseMaterial.HasValue ? Watch.FormatCaseMaterial(watch.CaseMaterial.Value).ToLower() : null);
            SetProductAttribute(product, "Материал браслета/ремешка", watch.StrapMaterial.HasValue ? Watch.FormatStrapMaterial(watch.StrapMaterial.Value).ToLower() : null);
            SetProductAttribute(product, "Стекло", watch.Crystal != null ? Watch.FormatWatchCrystal(watch.Crystal.Value).ToLower() : null);
            SetProductAttribute(product, "Габариты (мм)", watch.Size != null ? watch.Size.ToString() : null);
            SetProductAttribute(product, "Вес (г)", watch.Weight.HasValue ? watch.Weight.Value.ToString(CultureInfo.InvariantCulture) : null);

            SetProductAttribute(product, "Отображение даты", watch.DateDisplay != null ? watch.DateDisplay.ToString().ToLower() : null);
        }        

        private void SetProductAttribute(Product product, string attributeName, string attributeValue)
        {
            if (!attributes.ContainsKey(attributeName))
                throw new Exception(string.Format("Attribute '{0}' doesn't exist in open cart database", attributeName));

            var attribute = attributes[attributeName];

            var productAttribute = product.Attributes.FirstOrDefault(x => x.LanguageId == russian.Id && x.AttributeId == attribute.Id);

            if (string.IsNullOrEmpty(attributeValue) && productAttribute != null)
            {
                product.Attributes.Remove(productAttribute);
            }
            else if (!string.IsNullOrEmpty(attributeValue) && productAttribute != null)
            {
                productAttribute.Value = attributeValue;
            }
            else if (!string.IsNullOrEmpty(attributeValue) && productAttribute == null)
            {
                product.Attributes.Add(new ProductAttribute(attribute, russian, attributeValue));
            }
        }

        #endregion

        #region Product categories assign logic

        private void SetProductCategories(Product product, Watch watch)
        {
            product.AddToCategory(handWatchCategory, true);

            if (watch.Gender != null)
            {
                switch (watch.Gender)
                {
                    case WatchGender.Male:
                        AddProductToCategories(product, watch, maleCategoryTree);
                        RemoveProductFromCategories(product, femaleCategoryTree);
                        break;
                    case WatchGender.Female:
                        AddProductToCategories(product, watch, femaleCategoryTree);
                        RemoveProductFromCategories(product, maleCategoryTree);
                        break;
                }
            }
        }

        private void AddProductToCategories(Product product, Watch watch, CategoryTree categoryTree)
        {
            product.AddToCategory(categoryTree.GenderCategory);

            foreach (var brandCategory in categoryTree.BrandCategories)
            {
                if (watch.Brand == brandCategory.Key)
                    product.AddToCategory(brandCategory.Value);
                else
                    product.RemoveFromCategory(brandCategory.Value);
            }

            foreach (var seriesCategory in categoryTree.SeriesCategories)
            {
                if (watch.Series == seriesCategory.Key)
                    product.AddToCategory(seriesCategory.Value);
                else
                    product.RemoveFromCategory(seriesCategory.Value);
            }

            foreach (var countryCategory in categoryTree.CountryCategories)
            {
                if (watch.Country == countryCategory.Key)
                    product.AddToCategory(countryCategory.Value);
                else
                    product.RemoveFromCategory(countryCategory.Value);
            }

            foreach (var mechanismCategory in categoryTree.MechanismCategories)
            {
                if ((mechanismCategory.Key == "Электронные" && watch.Gender == WatchGender.Male && watch.Brand == "Suunto") ||
                    (mechanismCategory.Key == "Кварцевые" && watch.Movement == WatchMovement.Quartz) ||
                    (mechanismCategory.Key == "Механические" &&
                     (watch.Movement == WatchMovement.MechanicalManualWindUp ||
                      watch.Movement == WatchMovement.MechanicalAutomaticWindUp)))
                    product.AddToCategory(mechanismCategory.Value);
                else
                    product.RemoveFromCategory(mechanismCategory.Value);
            }

            foreach (var crystalCategory in categoryTree.CrystalCategories)
            {
                if (watch.Crystal == crystalCategory.Key)
                    product.AddToCategory(crystalCategory.Value);
                else
                    product.RemoveFromCategory(crystalCategory.Value);
            }

            foreach (var caseMaterialCategory in categoryTree.CaseMaterialCategories)
            {
                if (watch.CaseMaterial == caseMaterialCategory.Key)
                    product.AddToCategory(caseMaterialCategory.Value);
                else
                    product.RemoveFromCategory(caseMaterialCategory.Value);
            }

            foreach (var strapMaterialCategory in categoryTree.StrapMaterialCategories)
            {
                if (watch.StrapMaterial == strapMaterialCategory.Key)
                    product.AddToCategory(strapMaterialCategory.Value);
                else
                    product.RemoveFromCategory(strapMaterialCategory.Value);
            }

            if (watch.StrapMaterial == WatchStrapMaterial.StainlessSteel ||
                watch.StrapMaterial == WatchStrapMaterial.Titanium)
                product.AddToCategory(categoryTree.SpecialStrapCategory);
            else
                product.RemoveFromCategory(categoryTree.SpecialStrapCategory);
        }

        private void RemoveProductFromCategories(Product product, CategoryTree categoryTree)
        {
            foreach (var category in categoryTree)
            {
                product.RemoveFromCategory(category);
            }
        }

        #endregion

        #region Product description assign logic

        private void SetProductDescription(Product product, Watch watch)
        {
            var fullName = string.Format("{0} {1}", watch.Brand, watch.Model);
            var shortDescription = string.Empty;
            var fullDescription = string.Empty;

            if (watch.Brand == "Casio")
            {
                shortDescription = GetShortDescription(watch);
                shortDescription = shortDescription.Substring(0, Math.Min(255, shortDescription.Length));
                fullDescription = GetFullDescription(watch);
            }

            var productDescription = product.Descriptions.Localize(russian);
            if (productDescription == null)
            {
                productDescription = new ProductDescription();
                product.Descriptions.Add(productDescription);
            }

            productDescription.LanguageId = russian.Id;
            productDescription.Name = fullName;
            productDescription.MetaH1 = fullName;
            productDescription.Tag = string.Empty;
            productDescription.MetaTitle = GetProductTitle(watch);
            productDescription.MetaDescription = GetProductMetaDescription(watch);
            productDescription.MetaKeyword = GetProductMetaKeywords(watch);
            productDescription.Description = string.Empty; //fullDescription
        }

        private string GetFullDescription(Watch watch)
        {
            return string.Format(@"Самый популярный и один из самых надежных брендов наручных часов на мировом рынке - Casio!
Изделия мирового бренда отличаются своей оригинальностью исполнения дизайна, многофункциональностью и рядом других преимуществ.
Из множества представленных Casio серий каждый найдет для себя и своих близких именно то, что нужно!

{0}", GetShortDescription(watch));
        }

        private string GetShortDescription(Watch watch)
        {
            return string.Format(
               "Японские наручные часы {0} из серии {1}. Кварцевый механизм. Отображение времени на циферблате {2}. Ремешок выполнен из {3}. {4} корпус. Стекло на часах {5}. Водонепроницаемость {6}. Габариты часов: {7}.",
               watch.Brand,
               watch.Series,
               watch.DialType.HasValue ? Formatting.FormatWatchDialType(watch.DialType.Value) : string.Empty,
               watch.StrapMaterial.HasValue ? Formatting.FormatStrapMaterial(watch.StrapMaterial.Value) : string.Empty,
               watch.CaseMaterial.HasValue ? Formatting.FormatCaseMaterial(watch.CaseMaterial.Value) : string.Empty,
               watch.Crystal,
               watch.WaterResistance,
               watch.Size != null ? watch.Size.ToString() : string.Empty);
        }

        private string GetProductTitle(Watch watch)
        {
            var title = new StringBuilder("Купить ");

            if (watch.Gender == WatchGender.Male || watch.Gender == WatchGender.Female)
            {
                title.Append(Formatting.FormatWatchGender(watch.Gender.Value)).Append(" ");
            }

            title.AppendFormat("часы {0} {1} в Санкт-Петербурге с доставкой по России", watch.Brand, watch.Model);

            return title.ToString();  
        }

        private string GetProductMetaKeywords(Watch watch)
        {
            var keywords = new StringBuilder();

            keywords.Append("купить заказать ");

            if (!string.IsNullOrEmpty(watch.Country))
            {
                keywords.Append(watch.Country.ToLowerInvariant()).Append(" ");
            }

            if (!string.IsNullOrEmpty(watch.Brand))
            {
                keywords.Append(watch.Brand).Append(" ");
            }

            if (!string.IsNullOrEmpty(watch.Model))
            {
                keywords.Append(watch.Model).Append(" ");
            }

            keywords.Append("цена доставка москва спб цена недорого");

            return keywords.ToString();
        }

        private string GetProductMetaDescription(Watch watch)
        {
            var randomizer = new Randomizer(new Random(watch.Model.GetHashCode()));

            var description = new StringBuilder();

            description.Append(randomizer.GetRandom("Купить", "Заказать"))
                .Append(" ");

            if (!string.IsNullOrEmpty(watch.Country))
            {
                description
                    .Append(watch.Country.ToLowerInvariant())
                    .Append(" ");
            }

            description.Append("часы ").Append(watch.Brand).Append(" ")
                .Append(watch.Model).Append(" ")
                .AppendFormat("в {0} JUSTWATCHES, ", randomizer.GetRandom("магазине", "интернет-магазине"))
                .AppendFormat("у нас ✔ {0}; ", randomizer.GetRandom("Гарантия от производителя", "Оригинальные часы", "Большой выбор"))
                .AppendFormat("✔ {0}; ", randomizer.GetRandom("Доступные цены", "Низкие цены", "Хорошие цены"))
                .AppendFormat("✔ {0}; ", randomizer.GetRandom(
                    "Доставка по СПб, Москве и России", 
                    "Доставка по Санкт-Петербургу и России", 
                    "Доставка по Москве, Санкт-Петербургу и России"))
                .Append("✆ +7 (812) 988-58-37.");

            return description.ToString();
        }

        private class Randomizer
        {
            private readonly Random random;

            public Randomizer(Random random)
            {
                this.random = random;
            }

            public T GetRandom<T>(params T[] args)
            {
                var randomValue = this.random.Next(args.Length);

                if (randomValue >= 0 && randomValue < args.Length)
                    return args[randomValue];

                throw new ArgumentOutOfRangeException();
            }
        }

        #endregion

        #region Products extatabs logic

        private void SetProductExtraTabs(Product product, Watch watch)
        {
            if (string.IsNullOrEmpty(PaymentText))
                throw new Exception("Text for payment extratab is not provided");

            SetProductExtraTabText(product, paymentExtraTab, PaymentText);

            if (string.IsNullOrEmpty(ShippingText))
                throw new Exception("Text for shipping extratab is not provided");

            SetProductExtraTabText(product, shippingExtraTab, ShippingText);

            SetBrandExtraTabTextForProduct(product, watch);
        }

        private void SetBrandExtraTabTextForProduct(Product product, Watch watch)
        {
            Manufacturer manufacturer = null;
            if (!this.manufacturers.TryGetValue(watch.Brand.ToLowerInvariant(), out manufacturer))
                throw new Exception($"Could not get manufacturer {watch.Brand}");

            var name = manufacturer.Descriptions.GetName(russian);
            var description = manufacturer.Descriptions.Localize(russian).Description;

            if (!string.IsNullOrEmpty(description))
            {
                string extraTabName = $"О бренде {name}";
                ExtraTab extraTab = null;
                if (!extraTabs.TryGetValue(extraTabName, out extraTab))
                {
                    extraTab = new ExtraTab
                    {
                        SortOrder = shippingExtraTab.SortOrder + 1,
                        Status = true,
                        Descriptions = new Collection<ExtraTabDescription>()
                    };

                    extraTab.Descriptions.Add(new ExtraTabDescription(extraTabName, russian, extraTab));

                    extraTabs.Add(extraTabName, extraTab);
                }

                SetProductExtraTabText(product, extraTab, description);
            }
        }

        private void SetProductExtraTabText(Product product, ExtraTab extraTab, string text)
        {
            var productExtraTab = product.ExtraTabs.FirstOrDefault(
               x => x.ExtraTabId == extraTab.Id && x.LanguageId == russian.Id);

            if (productExtraTab != null)
            {
                productExtraTab.Text = text;
            }
            else
            {
                product.ExtraTabs.Add(new ProductExtraTab(extraTab, russian, text));
            }
        }

        #endregion 

        public Watch MapBack(Product target)
        {
            throw new NotImplementedException();
        }
    }
}
