﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenCart.Entities;
using JustWatch.WatchBook;

namespace JustWatch.OpenCartExport
{
    public class CategoryTreeBuilder
    {
        public CategoryTree Build(Category genderCategory, Language language)
        {
            var categoryTree = new CategoryTree();

            categoryTree.GenderCategory = genderCategory;
            categoryTree.BrandCategories = genderCategory.Categories.FindByName("Топ бренды", language).Categories.ToDictionary(x => x.Descriptions.GetName(language), x => x);
            categoryTree.SeriesCategories = categoryTree.BrandCategories.SelectMany(x => GetAllChildCategories(x.Value)).ToDictionary(x => x.Descriptions.GetName(language), x => x);
            categoryTree.CountryCategories = genderCategory.Categories.FindByName("Страна", language).Categories.ToDictionary(x => x.Descriptions.GetName(language), x => x);
            categoryTree.MechanismCategories = genderCategory.Categories.FindByName("Механизм", language).Categories.ToDictionary(x => x.Descriptions.GetName(language), x => x);
            categoryTree.CrystalCategories = genderCategory.Categories.FindByName("Стекло", language).Categories
                .ToDictionary(x => ParseWatchCrystal(x.Descriptions.GetName(language)), x => x);
            categoryTree.CaseMaterialCategories = genderCategory.Categories.FindByName("Корпус", language).Categories
                .ToDictionary(x => ParseCaseMaterial(x.Descriptions.GetName(language)), x => x);

            var categories = genderCategory.Categories.FindByName("Ремешок", language).Categories.ToArray();
            categoryTree.SpecialStrapCategory = categories.FindByName("Браслет", language);
            categoryTree.StrapMaterialCategories = categories.Where(x => x.Id != categoryTree.SpecialStrapCategory.Id)
                .ToDictionary(x => ParseStrapMaterial(x.Descriptions.GetName(language)), x => x);

            return categoryTree;
        }

        private static WatchCrystal ParseWatchCrystal(string categoryName)
        {
            switch (categoryName)
            {
                case "Минеральное":
                    return WatchCrystal.Mineral;
                case "Сапфировое":
                    return WatchCrystal.Sapphire;
                case "Пластиковое":
                    return WatchCrystal.Plastic;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал стекла: '{0}'", categoryName));
            }
        }

        private static WatchCaseMaterial ParseCaseMaterial(string caseMaterial)
        {
            switch (caseMaterial)
            {
                case "Пластиковые":
                    return WatchCaseMaterial.Plastic;
                case "Стальные":
                    return WatchCaseMaterial.StainlessSteel;
                case "Керамика":
                    return WatchCaseMaterial.Ceramics;
                case "Титан":
                case "Титановые":
                    return WatchCaseMaterial.Titanium;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал корпуса: '{0}'.", caseMaterial));
            }
        }

        private static WatchStrapMaterial ParseStrapMaterial(string strapMaterial)
        {
            switch (strapMaterial)
            {
                case "Пластик":
                    return WatchStrapMaterial.Plastic;
                case "Керамика":
                    return WatchStrapMaterial.Ceramics;
                case "Кожа":
                    return WatchStrapMaterial.Leather;
                case "Текстиль":
                    return WatchStrapMaterial.Textile;
                case "Каучук":
                    return WatchStrapMaterial.Rubber;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал ремешка: '{0}'.", strapMaterial));
            }
        }

        private static IEnumerable<Category> GetAllChildCategories(Category category)
        {
            return category.Categories.Concat(category.Categories.SelectMany(GetAllChildCategories));
        }
    }
}