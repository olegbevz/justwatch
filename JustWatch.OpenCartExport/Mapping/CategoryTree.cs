﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenCart.Entities;
using JustWatch.WatchBook;

namespace JustWatch.OpenCartExport
{
    public class CategoryTree : IEnumerable<Category>
    {
        public Category GenderCategory { get; set; }
        public Dictionary<string, Category> BrandCategories { get; set; }
        public Dictionary<string, Category> SeriesCategories { get; set; }
        public Dictionary<string, Category> CountryCategories { get; set; }
        public Dictionary<string, Category> MechanismCategories { get; set; }
        public Dictionary<WatchCrystal, Category> CrystalCategories { get; set; }
        public Dictionary<WatchCaseMaterial, Category> CaseMaterialCategories { get; set; }
        public Dictionary<WatchStrapMaterial, Category> StrapMaterialCategories { get; set; }
        public Category SpecialStrapCategory { get; set; }

        public IEnumerator<Category> GetEnumerator()
        {
            yield return GenderCategory;

            foreach (var category in BrandCategories.Values
                .Concat(CountryCategories.Values)
                .Concat(MechanismCategories.Values)
                .Concat(CrystalCategories.Values)
                .Concat(CaseMaterialCategories.Values)
                .Concat(StrapMaterialCategories.Values))
                yield return category;

            yield return SpecialStrapCategory;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}