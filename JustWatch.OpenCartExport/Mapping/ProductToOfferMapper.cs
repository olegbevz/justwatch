﻿using JustWatch.Common;
using JustWatch.EbayExport.YmlDocument;
using NickBuhro.Translit;
using OpenCart.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustWatch.OpenCartExport.Mapping
{
    public class ProductToOfferMapper : IMapper<Offer, Product>
    {
        private readonly Dictionary<string, string> usedOfferIds = new Dictionary<string, string>();
        private readonly Dictionary<int, string> attributes;

        public ProductToOfferMapper(IOpenCartDomain openCartDomain)
        {
            var russian = openCartDomain.Languages.FindByName("Russian");

            attributes = openCartDomain.AttributeGroups
                .Include("Attributes")
                .SelectMany(groups => groups.Attributes)
                .ToDictionary(
                    attribute => attribute.Id,
                    attribute => attribute.Descriptions.Single(description => description.LanguageId == russian.Id).Name);
        }

        public Offer MapForward(Product product)
        {
            var offer = new Offer();
            offer.Id = CreateOfferId(product.Model);
            offer.Available = true;
            offer.CategoryId = 14324;
            offer.CurrencyId = "RUR";

            offer.Type = "vendor.model";
            offer.Price = (double)product.Price;
            var keyword = product.Model.ReplaceUrlIncompatibleSymbols('-').ToLowerInvariant();

            offer.Url = $"https://justwatches.ru/watch/{keyword}";

            offer.TypePrefix = "Наручные часы";
            offer.Vendor = product.Manufacturer.Name;
            offer.VendorCode = product.Manufacturer.Name.ReplaceUrlIncompatibleSymbols().ToUpperInvariant();
            offer.Model = product.Model;
            offer.Description = string.Join(" ", offer.TypePrefix, offer.Vendor, offer.Model);
            offer.Pictures = new[] { $"https://justwatches.ru/image/{product.Image}" };
            offer.Pickup = true;
            //offer.Delivery = true;
            offer.SalesNotes = "Бесплатная доставка по Санкт-Петербургу, самовывоз";
            offer.ManufacturerWarranty = true;

            if (!string.IsNullOrEmpty(product.Manufacturer.Name))
                offer.Parameters.Add("Бренд", product.Manufacturer.Name);

            foreach (var attribute in product.Attributes)
            {
                string attributeName = null;
                if (!attributes.TryGetValue(attribute.AttributeId, out attributeName))
                    throw new Exception($"Could not find attribute with id {attribute.AttributeId}");

                offer.Parameters.Add(attributeName, attribute.Value);
            }

            if (offer.Price <= 0)
                throw new Exception($"Price for product { product.Model } could not be equal or less than zero.");

            return offer;
        }

        public Product MapBack(Offer target)
        {
            throw new NotImplementedException();
        }

        private string CreateOfferId(string model)
        {
            var offerId = Transliteration.CyrillicToLatin(
               model.ReplaceUrlIncompatibleSymbols().ToUpperInvariant(),
               NickBuhro.Translit.Language.Russian);

            if (offerId.Length > 20)
            {
                offerId = offerId.Substring(0, 20);
            }

            if (usedOfferIds.ContainsKey(offerId))
            {
                throw new Exception($"Offer id '{offerId}' has already been used.");
            }
            else
            {
                usedOfferIds.Add(offerId, model);
            }

            return offerId;
        }
    }
}
