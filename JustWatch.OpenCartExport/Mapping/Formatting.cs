﻿using JustWatch.WatchBook;
using System;

namespace JustWatch.OpenCartExport
{
    public static class Formatting
    {
        public static string FormatStrapMaterial(WatchStrapMaterial strapMaterial)
        {
            switch (strapMaterial)
            {
                case WatchStrapMaterial.Plastic:
                    return "пластика";
                case WatchStrapMaterial.StainlessSteel:
                    return "нержавеющей стали";
                case WatchStrapMaterial.Polyurethane:
                    return "полиуретана";
                case WatchStrapMaterial.Titanium:
                    return "титана";
                case WatchStrapMaterial.Polymer:
                    return "полимера";
                case WatchStrapMaterial.Aluminum:
                    return "алюминия";
                case WatchStrapMaterial.Brass:
                    return "латуни";
                case WatchStrapMaterial.Ceramics:
                    return "керамики";
                case WatchStrapMaterial.Copper:
                    return "меди";
                case WatchStrapMaterial.Composite:
                    return "композита";
                case WatchStrapMaterial.Leather:
                    return "кожи";
                //case WatchStrapMaterial.:
                //    return "каучука";
                case WatchStrapMaterial.Rubber:
                    return "резины";
                case WatchStrapMaterial.Silicone:
                    return "силикона";
                case WatchStrapMaterial.Textile:
                    return "текстиля";
                case WatchStrapMaterial.TitaniumCeramics:
                    return "титана и керамики";
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал браслета: '{0}'.", strapMaterial));
            }
        }

        public static string FormatCaseMaterial(WatchCaseMaterial caseMaterial)
        {
            switch (caseMaterial)
            {
                case WatchCaseMaterial.Plastic:
                    return "Пластиковый";
                case WatchCaseMaterial.StainlessSteel:
                    return "Стальной";
                case WatchCaseMaterial.Polyurethane:
                    return "Полиуретановый";
                case WatchCaseMaterial.Titanium:
                    return "Титановый";
                case WatchCaseMaterial.Polymer:
                    return "Полимерный";
                case WatchCaseMaterial.Aluminum:
                    return "Алюминиевый";
                case WatchCaseMaterial.Brass:
                    return "Латуниевый";
                case WatchCaseMaterial.Ceramics:
                    return "Керамический";
                case WatchCaseMaterial.Copper:
                    return "Медный";
                case WatchCaseMaterial.Composite:
                    return "Композитный";
                case WatchCaseMaterial.Gold:
                    return "Золотой";
                default:
                    throw new NotImplementedException();
            }
        }

        public static string FormatWatchDialType(WatchDialType watchDialType)
        {
            switch (watchDialType)
            {
                case WatchDialType.Analogue:
                    return "аналоговое";
                case WatchDialType.Digital:
                    return "цифровое";
                case WatchDialType.DigitalAndAnalogue:
                    return "комбинированное";
                default:
                    throw new NotImplementedException();
            }
        }

        public static string FormatWatchGender(WatchGender watchGender)
        {
            switch (watchGender)
            {
                case WatchGender.Male:
                    return "мужские";
                case WatchGender.Female:
                    return "женские";
                case WatchGender.Unisex:
                    return "унисекс";
                case WatchGender.Child:
                    return "детские";
                default:
                    throw new NotSupportedException("Unknown watch sex.");
            }
        }

        public static string FormatWatchMovement(WatchMovement movement)
        {
            switch (movement)
            {
                case WatchMovement.Quartz:
                    return "кварцевый";
                case WatchMovement.MechanicalAutomaticWindUp:
                case WatchMovement.MechanicalManualWindUp:
                    return "механический";
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
