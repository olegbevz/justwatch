﻿using System;
using System.IO;
using System.Linq;
using JustWatch.Common;
using JustWatch.WatchBook;

namespace JustWatch.SimpleMediaImport
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ConsoleUtils.WriteApplicationInfo();

                if (args.Length < 2)
                {
                    throw new Exception(
                        @"Following parameters should be passed to console: 
                        YandexMarketImporter.exe list.xlsx clocks.csv");
                }

                var currentDirectory = Directory.GetCurrentDirectory();
                var watchesExcelFilePath = Path.Combine(currentDirectory, args[0]);
                var yandexMarketCsvFilePath = Path.Combine(currentDirectory, args[1]);

                var mapper = new WatchMapper();
                var yandexMarketReader = new YandexMarketReader(yandexMarketCsvFilePath);
                var watches = yandexMarketReader.ReadProducts().Select(x => mapper.MapForward(x)).ToArray();

                var watchRepository = new WatchRepository(watchesExcelFilePath);
                watchRepository.AddWatches(watches);

                System.Console.WriteLine("Добавлено {0} записей.", watches.Length);
            }
            catch (Exception ex)
            {
                ConsoleUtils.WriteException(ex);
                Logger.LogException(ex);
            }
        }
    }
}
