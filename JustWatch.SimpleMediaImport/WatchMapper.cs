﻿using JustWatch.Common;
using JustWatch.WatchBook;
using System;
using System.Globalization;
using System.Linq;

namespace JustWatch.SimpleMediaImport
{
    public class WatchMapper : IMapper<Watch, YandexMarketProduct>
    {
        public Watch MapForward(YandexMarketProduct source)
        {
            var watch = new Watch();
            watch.Article = source.Name;
            var nameParts = source.Name.Split(' ');
            if (nameParts.Length < 2)
                throw new Exception(string.Format("Incorrect yandex market product name: {0}.", source.Name));
 
            watch.Brand = string.Join(" ", nameParts.Take(nameParts.Length - 1));
            watch.Model = nameParts[nameParts.Length - 1];
            watch.Pictures = source.Photos.OrderByDescending(x => x.Main).Select(x => x.File).ToArray();

            foreach (var feature in source.Features)
            {
                switch (feature.Group)
                {
                    case "Конструкция":
                        {
                            switch (feature.Name)
                            {
                                case "Вес":
                                    watch.Weight = ParseWeight(feature.Value);
                                    break;
                                case "Водонепроницаемые":
                                    watch.Watertight = ParseWatertight(feature.Value);
                                    break;
                                case "Габариты":
                                    watch.Size = ParseSize(feature.Value);
                                    break;
                                case "Габариты (ШхВхТ)":
                                    watch.Size = ParseDiameterHeightDepth(feature.Value);
                                    break;
                                case "Габариты (ШхВ)":
                                    watch.Size = ParseDiameterHeight(feature.Value);
                                    break;
                                case "Класс водонепроницаемости":
                                    watch.WaterResistance = feature.Value;
                                    break;
                                case "Материал браслета/ремешка":
                                    watch.StrapMaterial = ParseStrapMaterial(feature.Value);
                                    break;
                                case "Материал корпуса":
                                    watch.CaseMaterial = ParseCaseMaterial(feature.Value);
                                    break;
                                case "Стекло":
                                    watch.Crystal = ParseWatchCrystal(feature.Value);
                                    break;
                                case "Ширина ремешка":
                                case "Вставка":
                                case "Противоударные":
                                case "Прозрачный корпус":
                                    watch.AdditionalFunctions += string.Format("{0}: {1}; ", feature.Name, feature.Value);
                                    break;
                                default:
                                    throw CreateUnknownFeautureNameException(feature.Name);
                            }
                        }
                        break;
                    case "Конструкция и внешний вид":
                        switch (feature.Name)
                        {
                            case "Материал браслета/ремешка":
                                watch.StrapMaterial = ParseStrapMaterial(feature.Value);
                                break;
                            case "Вес":
                                watch.Weight = ParseWeight(feature.Value);
                                break;
                            case "Влагозащита":
                                watch.WaterResistance = feature.Value;
                                break;
                            case "Габариты (ШхВхТ)":
                                watch.Size = ParseDiameterHeightDepth(feature.Value);
                                break;
                            case "Тип стекла":
                                watch.Crystal = ParseWatchCrystal(feature.Value);
                                break;
                            case "Способ отображения времени":
                                watch.DialType = ParseWatchDialType(feature.Value);
                                break;
                            case "Защита от ударов":
                            case "Регулировка длины браслета/ремешка":
                            case "Цвета браслета/ремешка":
                            case "Цвета корпуса":
                                break;
                            default:
                                throw CreateUnknownFeautureNameException(feature.Name);
                        }
                        break;
                    case "Общие характеристики":
                        switch (feature.Name)
                        {
                            case "Источник энергии":
                                watch.EnergySource = feature.Value;
                                break;
                            case "Назначение":
                                watch.Gender = ParseSex(feature.Value);
                                break;
                            case "Способ отображения времени":
                                watch.DialType = ParseWatchDialType(feature.Value);
                                break;
                            case "Цифры":
                                watch.DialMarkers = feature.Value;
                                break;
                            case "Тип":
                                watch.Movement = ParseWatchMovement(feature.Value);
                                break;
                            case "Механизм":
                            case "Скелетон":
                                watch.AdditionalFunctions += string.Format("{0}: {1}; ", feature.Name, feature.Value);
                                break;
                            case "Поддержка платформ":
                            case "Установка сторонних приложений":
                            case "Уведомления с просмотром или ответом":
                                break;
                            default:
                                throw CreateUnknownFeautureNameException(feature.Name);
                        }
                        break;
                    case "Особенности":
                        switch (feature.Name)
                        {
                            case "Отображение даты":
                                watch.DateDisplay = string.IsNullOrEmpty(feature.Value) ? null : WatchDateDisplay.Parse(feature.Value);
                                break;
                            case "Дополнительные функции":
                                watch.AdditionalFunctions += feature.Value;
                                break;
                            case "Подсветка":
                            case "Точность хода":
                            case "Дополнительная информация":
                            case "Страна производства":
                            case "Хронограф":
                            case "Спорт-функции":
                            case "Хронометр":
                            case "Тахиметр":
                            case "Срок службы элемента питания":
                                break;
                            default:
                                throw CreateUnknownFeautureNameException(feature.Name);
                        }
                        break;
                    case "Связь":
                        switch (feature.Name)
                        {
                            case "GPS":
                            case "Интерфейсы":
                            case "Мобильный интернет":
                            case "Телефонные звонки":
                                break;
                            default:
                                throw CreateUnknownFeautureNameException(feature.Name);
                        }
                        break;
                    case "Питание":
                        switch (feature.Name)
                        {
                            case "Аккумулятор":
                            case "Время работы в активном режиме":
                            case "Дополнительная информация":
                            case "Время ожидания":
                                break;
                            default:
                                throw CreateUnknownFeautureNameException(feature.Name);
                        }
                        break;
                    case "Дополнительная функциональность":
                        switch (feature.Name)
                        {
                            case "Датчики":
                            case "Мониторинг":
                            case "Секундомер":
                            case "Таймер":
                                break;
                            default:
                                throw CreateUnknownFeautureNameException(feature.Name);
                        }
                        break;
                    case "Экран":
                        switch (feature.Name)
                        {
                            case "Наличие экрана":
                            case "Разрешение":
                            case "Тип":
                            case "Диагональ":
                                break;
                            default:
                                throw CreateUnknownFeautureNameException(feature.Name);
                        }
                        break;
                    case "Мультимедийные возможности":
                        switch (feature.Name)
                        {
                            case "Разъем для наушников":
                                break;
                            default:
                                throw CreateUnknownFeautureNameException(feature.Name);
                        }
                        break;
                    default:
                        throw new Exception(string.Format("Unknown feature group: {0}.", feature.Group));
                }
            }

            return watch;
        }

        public YandexMarketProduct MapBack(Watch target)
        {
            throw new NotImplementedException();
        }

        private WatchMovement? ParseWatchMovement(string watchMovement)
        {
            switch (watchMovement)
            {
                case "кварцевые ":
                case "кварцевые с автоподзаводом ":
                    return WatchMovement.Quartz;
                case "механические с автоподзаводом ":
                    return WatchMovement.MechanicalAutomaticWindUp;
                case "механические ":
                    return WatchMovement.MechanicalManualWindUp;
                case "умные часы ":
                    return null;
                default:
                    throw new NotImplementedException();
            }
        }

        private WatchGender ParseSex(string sex)
        {
            switch (sex)
            {
                case "унисекс ":
                    return WatchGender.Unisex;
                case "мужские ":
                    return WatchGender.Male;
                case "женские ":
                    return WatchGender.Female;
                default:
                    throw new NotImplementedException();
            }
        }

        private WatchCrystal ParseWatchCrystal(string glass)
        {
            throw new NotImplementedException();
        }

        private double ParseWeight(string weight)
        {
            return double.Parse(weight.Replace(" г", string.Empty), CultureInfo.InvariantCulture);
        }

        private bool ParseWatertight(string watertight)
        {
            switch (watertight)
            {
                case "да ":
                    return true;
                case "нет":
                    return false;
                default:
                    throw new NotImplementedException();
            }
        }

        private WatchSize ParseSize(string size)
        {
            var watchSize = new WatchSize();
            size = size.Replace(" мм", string.Empty);
            var sizeParts = size.Split('x');

            if (sizeParts.Length == 1)
            {
                watchSize.Diameter = double.Parse(sizeParts[0], CultureInfo.InvariantCulture);
            }
            else if (sizeParts.Length == 2)
            {
                watchSize.Diameter = double.Parse(sizeParts[0], CultureInfo.InvariantCulture);
                watchSize.Depth = double.Parse(sizeParts[1], CultureInfo.InvariantCulture);
            }
            else
            {
                throw new Exception("Incorrect size");
            }

            return watchSize;
        }

        private WatchSize ParseDiameterHeightDepth(string size)
        {
            size = size.Replace(" мм", string.Empty);
            var sizeParts = size.Split('x');

            if (sizeParts.Length != 3)
                throw new Exception("Incorrect size");

            var diameter = double.Parse(sizeParts[0], CultureInfo.InvariantCulture);
            var height = double.Parse(sizeParts[1], CultureInfo.InvariantCulture);
            var depth = double.Parse(sizeParts[2], CultureInfo.InvariantCulture);

            return new WatchSize(diameter, height, depth);
        }

        private WatchSize ParseDiameterHeight(string size)
        {
            size = size.Replace(" мм", string.Empty);
            var sizeParts = size.Split('x');

            if (sizeParts.Length != 2)
                throw new Exception("Incorrect size");

            var diameter = double.Parse(sizeParts[0], CultureInfo.InvariantCulture);
            var height = double.Parse(sizeParts[1], CultureInfo.InvariantCulture);

            return new WatchSize(diameter, height, null);
        }

        public static WatchDialType ParseWatchDialType(string watchDialType)
        {
            switch (watchDialType.RemoveRedundantWhiteSpaces())
            {
                case "Аналоговый":
                    return WatchDialType.Analogue;
                case "Цифровой":
                    return WatchDialType.Digital;
                case "Аналоговый и цифровой":
                    return WatchDialType.DigitalAndAnalogue;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить тип дисплея: \"{0}\".", watchDialType));
            }
        }

        private Exception CreateUnknownFeautureNameException(string featureName)
        {
            return new Exception(string.Format("Unknown feature name: {0}.", featureName));
        }

        private static WatchCaseMaterial ParseCaseMaterial(string caseMaterial)
        {
            switch (caseMaterial.RemoveRedundantWhiteSpaces())
            {
                case "Пластик":
                case "Покрытия":
                    return WatchCaseMaterial.Plastic;
                case "Нержавеющая сталь":
                    return WatchCaseMaterial.StainlessSteel;
                //case "Полиуретан":
                //    return WatchCaseMaterial.Polyurethane;
                case "Титан":
                    return WatchCaseMaterial.Titanium;
                //case "Полимер":
                //    return WatchCaseMaterial.Polymer;
                //case "Алюминий":
                //    return WatchCaseMaterial.Aluminum;
                //case "Латунь":
                //    return WatchCaseMaterial.Aluminum;
                case "Керамика":
                    return WatchCaseMaterial.Ceramics;
                //case "Медь":
                //    return WatchCaseMaterial.Copper;
                //case "Композит":
                case "Сплав":
                    return WatchCaseMaterial.Composite;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал корпуса: '{0}'.", caseMaterial));
            }
        }

        private static WatchStrapMaterial ParseStrapMaterial(string material)
        {
            throw new NotImplementedException();
        }
    }
}
