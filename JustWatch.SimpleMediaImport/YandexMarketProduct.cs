﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace JustWatch.SimpleMediaImport
{
    public class YandexMarketProduct
    {
        public YandexMarketProduct()
        {
            Photos = new Collection<ProductPhoto>();
            Features = new Collection<ProductFeature>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public Collection<ProductPhoto> Photos { get; private set; }
        public Collection<ProductFeature> Features { get; private set; }
    }

    public class ProductFeature
    {
        public ProductFeature()
        {
        }

        public ProductFeature(string group, string feature, string value)
        {
            Group = group;
            Name = feature;
            Value = value;
        }

        public string Group { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ProductPhoto
    {
        public ProductPhoto(string file, bool main)
        {
            File = file;
            Main = main;
        }

        public string File { get; set; }
        public bool Main { get; set; }
    }
}
