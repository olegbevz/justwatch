﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using JustWatch.Common;

namespace JustWatch.SimpleMediaImport
{
    public class CsvParser
    {
        private readonly string fileName;
        private const char separator = ',';

        public CsvParser(string fileName)
        {
            this.fileName = fileName;
        }

        public IEnumerable<string[]> ReadValues()
        {
            using (var fileStream = File.OpenRead(this.fileName))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    var currentLineBuilder = new StringBuilder();
                    bool waitingForClosingQuote = false;

                    while (!streamReader.EndOfStream)
                    {
                        var currentLine = streamReader.ReadLine();

                        if (currentLine.ContainsUnclosedQuote())
                        {
                            waitingForClosingQuote = !waitingForClosingQuote;
                            if (!waitingForClosingQuote)
                            {
                                currentLineBuilder.Append(currentLine);
                                currentLine = currentLineBuilder.ToString();
                                currentLineBuilder.Clear();
                            }
                        }

                        if (waitingForClosingQuote)
                            currentLineBuilder.Append(currentLine);

                        if (!waitingForClosingQuote)
                            yield return currentLine.SplitIfNotInQuotes(separator);
                    }
                }
            }
        }
    }
}
