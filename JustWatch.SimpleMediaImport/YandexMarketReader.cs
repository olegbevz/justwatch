﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace JustWatch.SimpleMediaImport
{
    public class YandexMarketReader
    {
        private readonly string fileName;

        public YandexMarketReader(string fileName)
        {
            this.fileName = fileName;
        }

        public IEnumerable<YandexMarketProduct> ReadProducts()
        {
            var csvParser = new CsvParser(fileName);
            bool captionRowWasReaden = false;
            MarketProductColumns productColumns = null;

            foreach (var csvValues in csvParser.ReadValues())
            {
                if (captionRowWasReaden)
                {
                    var marketProduct = ReadMarketProduct(productColumns, csvValues);
                    yield return marketProduct;
                }
                else
                {
                    productColumns = ReadCaptionColumns(csvValues);
                    captionRowWasReaden = true;
                }
            }
        }

        private YandexMarketProduct ReadMarketProduct(MarketProductColumns productColumns, string[] csvValues)
        {
            var marketProduct = new YandexMarketProduct();
            marketProduct.Name = csvValues[productColumns.NameColumn];
            marketProduct.Description = csvValues[productColumns.DescriptionColumn];
            marketProduct.Url = csvValues[productColumns.UrlColumn];

            foreach (var photoColumnTuple in productColumns.PhotoColumns)
            {
                var photoFile = csvValues[photoColumnTuple.Item1];
                if (string.IsNullOrEmpty(photoFile))
                    continue;

                var photoIsMain = csvValues[photoColumnTuple.Item2];
                if (string.IsNullOrEmpty(photoIsMain))
                    continue;

                marketProduct.Photos.Add(new ProductPhoto(photoFile, bool.Parse(photoIsMain)));
            }

            foreach (var featureColumnTuple in productColumns.FeatureColumns)
            {
                var featureGroup = csvValues[featureColumnTuple.Item1];
                if (string.IsNullOrEmpty(featureGroup))
                    continue;

                var featureName = csvValues[featureColumnTuple.Item2];
                if (string.IsNullOrEmpty(featureName))
                    continue;

                var featureValue = csvValues[featureColumnTuple.Item3];
                if (string.IsNullOrEmpty(featureValue))
                    continue;

                marketProduct.Features.Add(new ProductFeature(featureGroup, featureName, featureValue));
            }
            return marketProduct;
        }

        private MarketProductColumns ReadCaptionColumns(string[] csvValues)
        {
            var productColumns = new MarketProductColumns();

            var csvValuesList = csvValues.ToList();
            productColumns.NameColumn = csvValuesList.IndexOf("name");
            productColumns.DescriptionColumn = csvValuesList.IndexOf("description");
            productColumns.UrlColumn = csvValuesList.IndexOf("url");

            int photoIndex = 0, photoFileColumn = 0, photoIsMainColumn = 0;
            while ((photoFileColumn = csvValuesList.IndexOf(string.Format("photos/{0}/file", photoIndex))) >= 0 &&
                (photoIsMainColumn = csvValuesList.IndexOf(string.Format("photos/{0}/main", photoIndex))) >= 0)
            {
                productColumns.PhotoColumns.Add(new Tuple<int, int>(photoFileColumn, photoIsMainColumn));
                photoIndex++;
            }

            int featureIndex = 0, featureGroupColumn = 0, featureNameColumn = 0, featureValueColumn = 0;
            while ((featureGroupColumn = csvValuesList.IndexOf(string.Format("features/{0}/group", featureIndex))) >= 0 &&
                (featureNameColumn = csvValuesList.IndexOf(string.Format("features/{0}/feature", featureIndex))) >= 0 &&
                (featureValueColumn = csvValuesList.IndexOf(string.Format("features/{0}/value", featureIndex))) >= 0)
            {
                productColumns.FeatureColumns.Add(new Tuple<int, int, int>(featureGroupColumn, featureNameColumn, featureValueColumn));
                featureIndex++;
            }

            return productColumns;
        }

        private class MarketProductColumns
        {
            public MarketProductColumns()
            {
                PhotoColumns = new Collection<Tuple<int, int>>();
                FeatureColumns = new Collection<Tuple<int, int, int>>();
            }

            public int NameColumn { get; set; }
            public int DescriptionColumn { get; set; }
            public int UrlColumn { get; set; }
            public Collection<Tuple<int, int>> PhotoColumns { get; set; }
            public Collection<Tuple<int, int, int>> FeatureColumns { get; set; }
        }
    }
}
