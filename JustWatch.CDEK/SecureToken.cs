﻿using JustWatch.Common;
using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace JustWatch.CDEK
{
    class SecureToken
    {
        private static readonly Encoding encoding = Encoding.UTF8;

        private readonly DateTime date;
        private readonly Guid password;
        private readonly bool shortDateFormat;

        private const string LongDateFormat = "yyyy-MM-ddThh:mm:ss";
        private const string ShortDateFormat = "yyyy-MM-dd";

        public SecureToken(DateTime date, Guid password, bool shortDateFormat = false)
        {
            this.date = date;
            this.password = password;
            this.shortDateFormat = shortDateFormat;
        }

        public override string ToString()
        {
            using (var md5 = MD5.Create())
            {
                var dateString = date.ToString(shortDateFormat ? ShortDateFormat : LongDateFormat, CultureInfo.InvariantCulture);
                var hashString = string.Format("{0}&{1:n}", date.ToString(dateString, CultureInfo.InvariantCulture), password);

                var byteArray = md5.ComputeHash(encoding.GetBytes(hashString));

                return ByteUtils.BufferToHexString(byteArray);
            }
        }
    }
}
