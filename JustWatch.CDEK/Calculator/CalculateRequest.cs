﻿using System;

namespace JustWatch.CDEK.Calculator
{
    internal class CalculateRequest
    {
        public string Version { get; set; }
        public DateTime? DateExecute { get; set; }
        public string AuthLogin { get; set; }
        public string Secure { get; set; }
        public int? SenderCityId { get; set; }
        public string SenderCityPostCode { get; set; }
        public int? ReceiverCityId { get; set; }
        public string ReceiverCityPostCode { get; set; }
        public int? TariffId { get; set; }
        public Tariff[] TariffList { get; set; }
        public int? ModeId { get; set; }
        public Good[] Goods { get; set; }
    }
}
