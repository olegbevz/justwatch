﻿using Newtonsoft.Json;

namespace JustWatch.CDEK.Calculator
{
    internal class CalculateResponse
    {
        public DeliveryCondition Result { get; set; }

        [JsonProperty("error")]
        public Error[] Errors {get; set; }

        public class Error
        {
            public int Code { get; set; }
            public string Text { get; set; }
        }
    }
}
