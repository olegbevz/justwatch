﻿namespace JustWatch.CDEK.Calculator
{
    public class City
    {
        public City(int id)
        {
            Id = id;
        }

        public City(string postCode)
        {
            PostCode = postCode;
        }

        public int? Id { get; private set; }

        public string PostCode { get; private set; }
    }
}
