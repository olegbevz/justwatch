﻿using System;

namespace JustWatch.CDEK.Calculator
{
    public class CdekException : Exception
    {
        public CdekException(int code, string text)
        {
            Code = code;
            Text = text;
        }

        public int Code { get; set; }

        public string Text { get; set; }

        public override string Message
        {
            get
            {
                return $"Ошибка {Code}: {Text}";
            }
        }
    }
}
