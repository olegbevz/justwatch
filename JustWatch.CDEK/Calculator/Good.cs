﻿namespace JustWatch.CDEK.Calculator
{
    public class Good
    {
        public Good(double weight, double volume)
        {
            Weight = weight;
            Volume = volume;
        }

        public Good(double weight, double length, double width, double height)
        {
            Weight = weight;
            Length = length;
            Width = width;
            Height = height;
        }

        public double? Weight { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public double? Volume { get; set; }
    }
}
