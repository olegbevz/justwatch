﻿using System;

namespace JustWatch.CDEK.Calculator
{
    public class DeliveryCondition
    {
        public double Price { get; set; }
        public int DeliveryPeriodMin { get; set; }
        public int DeliveryPeriodMax { get; set; }
        public DateTime DeliveryDateMin { get; set; }
        public DateTime DeliveryDateMax { get; set; }
        public int TariffId { get; set; }
        public double CashOnDelivery { get; set; }
        public double? PriceByCurrency { get; set; }
        public string Currency { get; set; }
    }
}
