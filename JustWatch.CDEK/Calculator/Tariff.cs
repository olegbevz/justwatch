﻿namespace JustWatch.CDEK.Calculator
{
    public class Tariff
    {
        public Tariff(int id, int priority)
        {
            Id = id;
            Priority = priority;
        }

        public int Id { get; set; }
        public int Priority { get; set; }
    }
}
