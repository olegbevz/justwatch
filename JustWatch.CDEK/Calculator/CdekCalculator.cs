﻿using JustWatch.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace JustWatch.CDEK.Calculator
{
    public class CdekCalculator
    {
        private const string BaseUrl = "http://api.cdek.ru/calculator";
        private const string CalculatorPage = "calculate_price_by_json.php";
        private const string ContentType = "application/json";
        private const string DateFormat = "yyyy-MM-dd";

        private static readonly Encoding encoding = Encoding.UTF8;

        private readonly Guid? account;
        private readonly Guid? password;

        private readonly JsonSerializerSettings jsonSettings = CreateJsonSettings();

        public CdekCalculator()
        {
        }

        public CdekCalculator(Guid account, Guid password)
        {
            this.account = account;
            this.password = password;
        }

        public string Version { get; set; } = "1.0";

        public DeliveryCondition Calculate(DateTime? executeDate, City cityFrom, City cityTo, int[] tariffs, int? mode, params Good[] goods)
        {
            var calculateRequest = new CalculateRequest();

            calculateRequest.Version = Version;
            
            if (account != null && password != null && executeDate != null)
            {
                calculateRequest.AuthLogin = account.Value.ToString("n");
                var secureToken = new SecureToken(executeDate.Value, password.Value, true);
                calculateRequest.Secure = secureToken.ToString();
            }

            calculateRequest.DateExecute = executeDate;
            calculateRequest.SenderCityId = cityFrom.Id;
            calculateRequest.SenderCityPostCode = cityFrom.PostCode;
            calculateRequest.ReceiverCityId = cityTo.Id;
            calculateRequest.ReceiverCityPostCode = cityTo.PostCode;

            if (tariffs != null || tariffs.Length == 1)
            {
                calculateRequest.TariffId = tariffs[0];
            }
            else if (tariffs != null || tariffs.Length > 1)
            {
                calculateRequest.TariffList = tariffs
                    .Select((tariff, order) => new Tariff(tariff, order))
                    .ToArray();
            }

            calculateRequest.ModeId = mode;
            calculateRequest.Goods = goods;

            var httpRequest = HttpWebRequest.Create(HttpUrl.Concat(BaseUrl, CalculatorPage));
            httpRequest.Method = "POST";
            httpRequest.ContentType = ContentType;

            using (var requestStream = httpRequest.GetRequestStream())
            {
                var requestBody = JsonConvert.SerializeObject(calculateRequest, jsonSettings);

                var requestBodyBytes = encoding.GetBytes(requestBody);

                requestStream.Write(requestBodyBytes, 0, requestBodyBytes.Length);
            }

            var response = httpRequest.GetResponse();

            using (var responseStream = response.GetResponseStream())
            {
                using (var memoryStream = new MemoryStream())
                {
                    responseStream.CopyTo(memoryStream);

                    memoryStream.Seek(0, SeekOrigin.Begin);

                    var responseBody = encoding.GetString(memoryStream.ToArray());

                    var calculatorResponse = JsonConvert.DeserializeObject<CalculateResponse>(responseBody, jsonSettings);

                    if (calculatorResponse?.Errors?.Length > 0)
                    {
                        var error = calculatorResponse.Errors[0];

                        throw new CdekException(error.Code, error.Text);
                    }

                    return calculatorResponse.Result;
                }
            }
        }

        private static JsonSerializerSettings CreateJsonSettings()
        {
            return new JsonSerializerSettings()
            {
                DateFormatString = DateFormat,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };
        }
    }
}
