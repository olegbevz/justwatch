﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot("DeleteRequest")]
    public class DeleteRequest
    {
        public DeleteRequest()
        {
            Orders = new List<Order>();
        }
        
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "OrderCount")]
        public int OrderCount { get; set; }
        [XmlAttribute(AttributeName = "Account")]
        public string Account { get; set; }
        [XmlAttribute(AttributeName = "Secure")]
        public string Secure { get; set; }

        [XmlElement(ElementName = "Order")]
        public List<Order> Orders { get; set; }
        
        public class Order
        {
            public Order(string number)
            {
                Number = number;
            }

            public Order()
            {
            }

            [XmlAttribute(AttributeName = "Number")]
            public string Number { get; set; }
        }
    }
}