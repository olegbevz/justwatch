﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot(ElementName = "response")]
    public class DeliveryResponse
    {
        [XmlElement(ElementName = "Order")]
        public List<DeliveryOrderStatus> Orders { get; set; }       
    }

    [XmlRoot(ElementName = "Order")]
    public class DeliveryOrderStatus
    {
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "DispatchNumber")]
        public string DispatchNumber { get; set; }
        [XmlAttribute(AttributeName = "Msg")]
        public string Msg { get; set; }
    }
}
