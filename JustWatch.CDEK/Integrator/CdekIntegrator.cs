﻿using JustWatch.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    public class CdekIntegrator
    {
        private const string BaseUrl = "http://int.cdek.ru";
        private const string DateFormat = "yyyy-MM-ddThh:mm:ss";
        private const string PostVariable = "xml_request";

        private const string DeliveryPostsPage = "pvzlist.php";
        private const string StatusReportPage = "status_report_h.php";
        private const string InfoReportPage = "info_report.php";
        private const string OrdersPrintPage = "orders_print.php";
        private const string NewOrdersPage = "new_orders.php";
        private const string DeleteOrdersPage = "delete_orders.php";

        private readonly Guid account;
        private readonly Guid securePassword;

        private static readonly Encoding encoding = Encoding.UTF8;
        private static readonly Dictionary<Type, XmlSerializer> xmlSerializers = new Dictionary<Type, XmlSerializer>();

        private static TimeSpan deliveryRequestTimeout = TimeSpan.FromMinutes(15);

        public CdekIntegrator(Guid account, Guid securePassword)
        {
            this.account = account;
            this.securePassword = securePassword;
        }

        public IEnumerable<DeliveryPost> GetDeliveryPosts(int? cityId, string cityPostCode, string postType, string language)
        {
            if (cityId == null && cityPostCode == null)
                throw new ArgumentException("You should specify either city id or city post code.");

            var parameters = new Dictionary<string, string>();

            if (cityId.HasValue)
                parameters.Add("cityid", cityId.ToString());

            if (cityPostCode != null)
                parameters.Add("citypostcode", cityPostCode);

            if (postType != null)
                parameters.Add("type", postType);

            if (language != null)
                parameters.Add("lang", language);

            var response = SendGetRequest(DeliveryPostsPage, parameters);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.StatusCode.ToString());

            using (var responseStream = response.GetResponseStream())
            {
                var deliveryPosts  = GetXmlSerializer<DeliveryPostsReport>().Deserialize(responseStream) as DeliveryPostsReport;

                if (deliveryPosts != null)
                    return deliveryPosts.Pvz;

                throw new NullReferenceException();
            }
        }

        public StatusReport GetStatusReport(DateTime startDate, DateTime? endDate, bool? showHistory)
        {
            var secretToken = new SecureToken(startDate, securePassword).ToString();

            var parameters = new Dictionary<string, string>();

            parameters.Add("account", account.ToString("n"));
            parameters.Add("secure", secretToken);
            parameters.Add("datefirst", startDate.ToString(DateFormat, CultureInfo.InvariantCulture));
            if (endDate.HasValue)
                parameters.Add("datelast", endDate.Value.ToString(DateFormat, CultureInfo.InvariantCulture));
            if (showHistory.HasValue)
                parameters.Add("showhistory", showHistory.Value ? "1" : "0");

            var response = SendGetRequest(StatusReportPage, parameters);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.StatusCode.ToString());

            using (var responseStream = response.GetResponseStream())
            {
                return GetXmlSerializer<StatusReport>().Deserialize(responseStream) as StatusReport;
            }
        }

        public InfoReport GetInfoReport(DateTime? startDate, DateTime? endDate, IEnumerable<OrderReference> orders)
        {
            var currentTime = DateTime.Now;
            var secureToken = new SecureToken(currentTime, securePassword).ToString();

            var infoRequest = new InfoRequest(account.ToString("n"), currentTime.ToString(DateFormat, CultureInfo.InvariantCulture), secureToken);
            if (startDate != null || endDate != null)
            {
                infoRequest.ChangePeriod = new InfoRequest.RequestPeriod(
                    startDate != null ? startDate.Value.ToString(DateFormat, CultureInfo.InvariantCulture) : string.Empty,
                    endDate != null ? endDate.Value.ToString(DateFormat, CultureInfo.InvariantCulture) : string.Empty);
            }

            if (orders != null)
            {
                infoRequest.Orders.AddRange(orders);
            }

            var response = SendPostRequest(InfoReportPage, infoRequest);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.StatusCode.ToString());

            using (var responseStream = response.GetResponseStream())
            {
                return GetXmlSerializer<InfoReport>().Deserialize(responseStream) as InfoReport;
            }
        }

        public IEnumerable<DeliveryOrderStatus> SendDeliveryRequest(string actNumber, DateTime date, string currency, IEnumerable<DeliveryOrder> orders)
        {
            var deliveryRequest = new DeliveryRequest
            {
                Account = account.ToString("n", CultureInfo.InvariantCulture),
                Date = date.ToString(DateFormat, CultureInfo.InvariantCulture),
                Currency = currency,
                Number = actNumber,
                OrderCount = orders.Count(),
                Secure = new SecureToken(date, securePassword).ToString()
            };

            deliveryRequest.Orders.AddRange(orders);

            var response = SendPostRequest(NewOrdersPage, deliveryRequest, deliveryRequestTimeout);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.StatusCode.ToString());

            using (var responseStream = response.GetResponseStream())
            {
                var deliveryResponse = GetXmlSerializer<DeliveryResponse>().Deserialize(responseStream) as DeliveryResponse;

                return deliveryResponse?.Orders;
            }
        }

        public Stream PrintOrders(DateTime date, int copyCount, IEnumerable<OrderReference> orders)
        {
            var printRequest = new OrdersPrintRequest(
                date.ToString(DateFormat, CultureInfo.InvariantCulture), 
                account.ToString("n"), 
                new SecureToken(date, securePassword).ToString(), 
                orders.Count(), 
                copyCount);

            printRequest.Orders.AddRange(orders);

            var response = SendPostRequest(OrdersPrintPage, printRequest);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.StatusCode.ToString());

            return response.GetResponseStream();
        }

        public void DeleteOrders(string actNumber, DateTime date, string[] orderNumbers)
        {
            var deleteRequest = new DeleteRequest
            {
                Account = account.ToString("n", CultureInfo.InvariantCulture),
                Date = date.ToString(DateFormat, CultureInfo.InvariantCulture),
                Number = actNumber,
                OrderCount = orderNumbers.Length,
                Secure = new SecureToken(date, securePassword).ToString(),
            };

            deleteRequest.Orders.AddRange(orderNumbers.Select(number => new DeleteRequest.Order(number)));

            var response = SendPostRequest(DeleteOrdersPage, deleteRequest);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.StatusCode.ToString());
        }

        #region Secondary methods

        private HttpWebResponse SendPostRequest<T>(string requestPage, T requestOptions, TimeSpan? timeout = null)
        {
            var request = HttpWebRequest.Create(HttpUrl.Concat(BaseUrl, requestPage));
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            if (timeout != null)
                request.Timeout = (int)timeout.Value.TotalMilliseconds;

            var requestContent = string.Format("{0}={1}", PostVariable, Serialize(requestOptions));
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(encoding.GetBytes(requestContent), 0, encoding.GetByteCount(requestContent));
            }

            return request.GetResponse() as HttpWebResponse;
        }

        private HttpWebResponse SendGetRequest(string requestPage, Dictionary<string, string> queryParameters)
        {
            var httpRequest = HttpWebRequest.Create(HttpUrl.Concat(BaseUrl, requestPage, queryParameters));
            httpRequest.Method = "GET";
            return httpRequest.GetResponse() as HttpWebResponse;
        }

        private XmlSerializer GetXmlSerializer<T>()
        {
            var type = typeof(T);
            XmlSerializer xmlSerializer = null;

            if (!xmlSerializers.TryGetValue(type, out xmlSerializer))
            {
                xmlSerializers.Add(type, xmlSerializer = new XmlSerializer(type));
            }

            return xmlSerializer;
        }

        private string Serialize<T>(T @object)
        {
            using (var xmlStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(xmlStream, encoding))
                {
                    GetXmlSerializer<T>().Serialize(streamWriter, @object);

                    xmlStream.Seek(0, SeekOrigin.Begin);

                    using (var streamReader = new StreamReader(xmlStream))
                    {
                        return streamReader.ReadToEnd();
                    }
                }
            }
        }

        #endregion
    }
}
