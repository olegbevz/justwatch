﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot(ElementName = "OrdersPrint")]
    public class OrdersPrintRequest
    {
        public OrdersPrintRequest(string requestDate, string account, string secure, int orderCount, int copyCount)
        {
            Date = requestDate;
            Account = account;
            Secure = secure;
            OrderCount = orderCount;
            CopyCount = copyCount;

            Orders = new List<OrderReference>();
        }

        public OrdersPrintRequest()
        {
        }

        [XmlElement(ElementName = "Order")]
        public List<OrderReference> Orders { get; set; }
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "Account")]
        public string Account { get; set; }
        [XmlAttribute(AttributeName = "Secure")]
        public string Secure { get; set; }
        [XmlAttribute(AttributeName = "OrderCount")]
        public int OrderCount { get; set; }
        [XmlAttribute(AttributeName = "CopyCount")]
        public int CopyCount { get; set; }
    }
}
