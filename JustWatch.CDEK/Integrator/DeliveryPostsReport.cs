﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot(ElementName = "WeightLimit")]
    public class WeightLimit
    {
        [XmlAttribute(AttributeName = "WeightMin")]
        public string WeightMin { get; set; }
        [XmlAttribute(AttributeName = "WeightMax")]
        public string WeightMax { get; set; }
    }

    [XmlRoot(ElementName = "Pvz")]
    public class DeliveryPost
    {
        [XmlElement(ElementName = "WeightLimit")]
        public WeightLimit WeightLimit { get; set; }
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "CityCode")]
        public string CityCode { get; set; }
        [XmlAttribute(AttributeName = "City")]
        public string City { get; set; }
        [XmlAttribute(AttributeName = "WorkTime")]
        public string WorkTime { get; set; }
        [XmlAttribute(AttributeName = "Address")]
        public string Address { get; set; }
        [XmlAttribute(AttributeName = "Phone")]
        public string Phone { get; set; }
        [XmlAttribute(AttributeName = "Note")]
        public string Note { get; set; }
        [XmlAttribute(AttributeName = "coordX")]
        public string CoordX { get; set; }
        [XmlAttribute(AttributeName = "coordY")]
        public string CoordY { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "ownerCode")]
        public string OwnerCode { get; set; }
    }

    [XmlRoot(ElementName = "PvzList")]
    public class DeliveryPostsReport
    {
        [XmlElement(ElementName = "Pvz")]
        public List<DeliveryPost> Pvz { get; set; }
    }
}
