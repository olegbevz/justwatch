﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot(ElementName = "Status")]
    public class Status
    {
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "Description")]
        public string Description { get; set; }
        [XmlAttribute(AttributeName = "CityCode")]
        public string CityCode { get; set; }
        [XmlAttribute(AttributeName = "CityName")]
        public string CityName { get; set; }
    }

    [XmlRoot(ElementName = "Reason")]
    public class Reason
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "Description")]
        public string Description { get; set; }
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
    }

    [XmlRoot(ElementName = "DelayReason")]
    public class DelayReason
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "Description")]
        public string Description { get; set; }
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
    }

    [XmlRoot(ElementName = "Good")]
    public class Good
    {
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "DateDeliv")]
        public string DateDeliv { get; set; }
    }

    [XmlRoot(ElementName = "CallGood")]
    public class CallGood
    {
        [XmlElement(ElementName = "Good")]
        public List<Good> Good { get; set; }
        [XmlElement(ElementName = "Reason")]
        public Reason Reason { get; set; }
        [XmlElement(ElementName = "DelayReason")]
        public DelayReason DelayReason { get; set; }
        [XmlElement(ElementName = "Call")]
        public Call Call { get; set; }
    }

    [XmlRoot(ElementName = "Fail")]
    public class Fail
    {
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "ReasonCode")]
        public string ReasonCode { get; set; }
        [XmlAttribute(AttributeName = "ReasonDescription")]
        public string ReasonDescription { get; set; }
    }

    [XmlRoot(ElementName = "CallFail")]
    public class CallFail
    {
        [XmlElement(ElementName = "Fail")]
        public List<Fail> Fail { get; set; }
    }

    [XmlRoot(ElementName = "Delay")]
    public class Delay
    {
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "DateNext")]
        public string DateNext { get; set; }
    }

    [XmlRoot(ElementName = "CallDelay")]
    public class CallDelay
    {
        [XmlElement(ElementName = "Delay")]
        public List<Delay> Delay { get; set; }
    }

    [XmlRoot(ElementName = "Call")]
    public class Call
    {
        [XmlElement(ElementName = "CallGood")]
        public CallGood CallGood { get; set; }
        [XmlElement(ElementName = "CallFail")]
        public CallFail CallFail { get; set; }
        [XmlElement(ElementName = "CallDelay")]
        public CallDelay CallDelay { get; set; }
    }

    [XmlRoot(ElementName = "Order")]
    public class Order
    {
        [XmlElement(ElementName = "Status")]
        public Status Status { get; set; }
        [XmlElement(ElementName = "Reason")]
        public Reason Reason { get; set; }
        [XmlElement(ElementName = "DelayReason")]
        public DelayReason DelayReason { get; set; }
        [XmlElement(ElementName = "Call")]
        public Call Call { get; set; }
        [XmlAttribute(AttributeName = "ActNumber")]
        public string ActNumber { get; set; }
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "DispatchNumber")]
        public int DispatchNumber { get; set; }
        [XmlAttribute(AttributeName = "DeliveryDate")]
        public string DeliveryDate { get; set; }
        [XmlAttribute(AttributeName = "RecipientName")]
        public string RecipientName { get; set; }
    }

    [XmlRoot(ElementName = "StatusReport")]
    public class StatusReport
    {
        [XmlElement(ElementName = "Order")]
        public List<Order> Orders { get; set; }
        [XmlAttribute(AttributeName = "DateFirst")]
        public string DateFirst { get; set; }
        [XmlAttribute(AttributeName = "DateLast")]
        public string DateLast { get; set; }
    }
}
