﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot(ElementName = "DeliveryRequest")]
    public class DeliveryRequest
    {       
        public DeliveryRequest()
        {
            Orders = new List<DeliveryOrder>();
        }

        [XmlAttribute(AttributeName = "Account")]
        public string Account { get; set; }
        [XmlAttribute(AttributeName = "Currency")]
        public string Currency { get; set; }
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "OrderCount")]
        public int OrderCount { get; set; }
        [XmlAttribute(AttributeName = "Secure")]
        public string Secure { get; set; }

        [XmlElement(ElementName = "Order")]
        public List<DeliveryOrder> Orders { get; set; }
    }

    [XmlRoot(ElementName = "address")]
    public class DeliveryAddress
    {
        [XmlAttribute(AttributeName = "flat")]
        public string Flat { get; set; }
        [XmlAttribute(AttributeName = "house")]
        public string House { get; set; }
        [XmlAttribute(AttributeName = "street")]
        public string Street { get; set; }
    }

    [XmlRoot(ElementName = "item")]
    public class DeliveryItem
    {
        [XmlAttribute(AttributeName = "amount")]
        public int Amount { get; set; }
        [XmlAttribute(AttributeName = "comment")]
        public string Comment { get; set; }
        [XmlAttribute(AttributeName = "commentEx")]
        public string CommentEx { get; set; }
        [XmlAttribute(AttributeName = "cost")]
        public double Cost { get; set; }
        [XmlAttribute(AttributeName = "costEx")]
        public double CostEx { get; set; }
        [XmlAttribute(AttributeName = "link")]
        public string Link { get; set; }
        [XmlAttribute(AttributeName = "payment")]
        public double Payment { get; set; }
        [XmlAttribute(AttributeName = "paymentEx")]
        public double PaymentEx { get; set; }
        [XmlAttribute(AttributeName = "wareKey")]
        public string WareKey { get; set; }
        [XmlAttribute(AttributeName = "weight")]
        public int Weight { get; set; }
        [XmlAttribute(AttributeName = "weightBrutto")]
        public int WeightBrutto { get; set; }
    }

    [XmlRoot(ElementName = "package")]
    public class DeliveryPackage
    {
        [XmlElement(ElementName = "item")]
        public List<DeliveryItem> Items { get; set; }
        [XmlAttribute(AttributeName = "barCode")]
        public string BarCode { get; set; }
        [XmlAttribute(AttributeName = "number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "weight")]
        public int Weight { get; set; }
    }

    [XmlRoot(ElementName = "order")]
    public class DeliveryOrder
    {
        [XmlElement(ElementName = "address")]
        public DeliveryAddress Address { get; set; }
        [XmlElement(ElementName = "package")]
        public DeliveryPackage Package { get; set; }
        [XmlAttribute(AttributeName = "DateInvoice")]
        public string DateInvoice { get; set; }
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "phone")]
        public string Phone { get; set; }
        [XmlAttribute(AttributeName = "RecCityCode")]
        public string RecCityCode { get; set; }
        [XmlAttribute("RecCityPostCode")]
        public string RecCityPostCode { get; set; }
        [XmlAttribute(AttributeName = "recipientEmail")]
        public string RecipientEmail { get; set; }
        [XmlAttribute(AttributeName = "recipientName")]
        public string RecipientName { get; set; }
        [XmlAttribute(AttributeName = "sellerAddress")]
        public string SellerAddress { get; set; }
        [XmlAttribute(AttributeName = "sellerName")]
        public string SellerName { get; set; }
        [XmlAttribute(AttributeName = "SendCityCode")]
        public string SendCityCode { get; set; }
        [XmlAttribute("SendCityPostCode")]
        public string SendCityPostCode { get; set; }
        [XmlAttribute(AttributeName = "ShipperAddress")]
        public string ShipperAddress { get; set; }
        [XmlAttribute(AttributeName = "ShipperName")]
        public string ShipperName { get; set; }
        [XmlAttribute(AttributeName = "tariffTypeCode")]
        public int TariffTypeCode { get; set; }
        [XmlAttribute("Comment")]
        public string Comment { get; set; }
    }
}
