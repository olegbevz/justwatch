﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot(ElementName = "InfoRequest")]
    public class InfoRequest
    {
        public InfoRequest()
        {
        }

        public InfoRequest(string account, string date, string secure)
        {
            Account = account;
            Secure = secure;
            Date = date;

            Orders = new List<OrderReference>();
        }

        [XmlElement(ElementName = "ChangePeriod")]
        public RequestPeriod ChangePeriod { get; set; }
        [XmlElement(ElementName = "Order")]
        public List<OrderReference> Orders { get; set; }
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "Account")]
        public string Account { get; set; }
        [XmlAttribute(AttributeName = "Secure")]
        public string Secure { get; set; }

        [XmlRoot(ElementName = "ChangePeriod")]
        public class RequestPeriod
        {
            public RequestPeriod(string startDate, string endDate)
            {
                DateBeg = startDate;
                DateEnd = endDate;
            }

            public RequestPeriod()
            {
            }

            [XmlAttribute(AttributeName = "DateBeg")]
            public string DateBeg { get; set; }
            [XmlAttribute(AttributeName = "DateEnd")]
            public string DateEnd { get; set; }
        }
    }
}
