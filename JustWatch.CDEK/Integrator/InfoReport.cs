﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot(ElementName = "InfoReport")]
    public class InfoReport
    {
        [XmlElement(ElementName = "Order")]
        public List<Order> Orders { get; set; }
        
        public class City
        {
            [XmlAttribute(AttributeName = "Code")]
            public string Code { get; set; }
            [XmlAttribute(AttributeName = "PostCode")]
            public string PostCode { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
        }

        [XmlRoot(ElementName = "AddedService")]
        public class AddedService
        {
            [XmlAttribute(AttributeName = "ServiceCode")]
            public string ServiceCode { get; set; }
            [XmlAttribute(AttributeName = "Sum")]
            public string Sum { get; set; }
        }

        [XmlRoot(ElementName = "Order")]
        public class Order
        {
            [XmlElement(ElementName = "SendCity")]
            public City SendCity { get; set; }
            [XmlElement(ElementName = "RecCity")]
            public List<City> RecCity { get; set; }
            [XmlElement(ElementName = "AddedService")]
            public List<AddedService> AddedService { get; set; }
            [XmlAttribute(AttributeName = "Number")]
            public string Number { get; set; }
            [XmlAttribute(AttributeName = "Date")]
            public string Date { get; set; }
            [XmlAttribute(AttributeName = "DispatchNumber")]
            public string DispatchNumber { get; set; }
            [XmlAttribute(AttributeName = "TariffTypeCode")]
            public string TariffTypeCode { get; set; }
            [XmlAttribute(AttributeName = "Weight")]
            public string Weight { get; set; }
            [XmlAttribute(AttributeName = "DeliverySum")]
            public string DeliverySum { get; set; }
            [XmlAttribute(AttributeName = "DateLastChange")]
            public string DateLastChange { get; set; }
            [XmlAttribute(AttributeName = "CashOnDeliv")]
            public string CashOnDeliv { get; set; }
            [XmlAttribute(AttributeName = "CashOnDelivFact")]
            public string CashOnDelivFact { get; set; }
            [XmlAttribute(AttributeName = "deliveryMode")]
            public string DeliveryMode { get; set; }
            [XmlAttribute(AttributeName = "deliveryVariant")]
            public string DeliveryVariant { get; set; }
            [XmlAttribute(AttributeName = "pvzCode")]
            public string PvzCode { get; set; }
        }
    }
}
