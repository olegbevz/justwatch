﻿using System;
using System.Globalization;
using System.Xml.Serialization;

namespace JustWatch.CDEK
{
    [XmlRoot(ElementName = "Order")]
    public class OrderReference
    {
        public OrderReference(int dispatchNumber)
        {
            DispatchNumber = dispatchNumber;
        }

        public OrderReference(DateTime date, string number)
        {
            Date = date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            Number = number;
        }

        public OrderReference()
        {
        }

        [XmlAttribute(AttributeName = "DispatchNumber")]
        public int DispatchNumber { get; set; }
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "Date")]
        public string Date { get; set; }
    }
}
