﻿using JustWatch.Common;

namespace JustWatch.EbayExport.BrandPictures
{
    public class BrandPictureRepository : LinqToExcelRepository<BrandPicture>
    {
        public BrandPictureRepository(string fileName) : base(fileName)
        {
        }
    }
}
