﻿using LinqToExcel.Attributes;

namespace JustWatch.EbayExport.BrandPictures
{
    public class BrandPicture
    {
        [ExcelColumn("$brand$")]
        public string Brand { get; set; }

        [ExcelColumn("$picture brand$")]
        public string Url { get; set; }

        [ExcelColumn("$width$")]
        public int Width { get; set; }

        [ExcelColumn("$height$")]
        public int Height { get; set; }
    }
}
