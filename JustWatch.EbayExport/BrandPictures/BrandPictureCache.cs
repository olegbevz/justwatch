﻿using JustWatch.Common;
using System;
using System.Collections.Generic;

namespace JustWatch.EbayExport.BrandPictures
{
    public class BrandPictureCache : CacheBase<BrandPicture>
    {
        public BrandPictureCache(IEnumerable<BrandPicture> brandPictures) : base(brandPictures)
        {
        }

        public BrandPicture GetBrandPicture(string brand)
        {
            BrandPicture brandPicture = GetByKey(brand.Simplify());
            
            if (brandPicture == null)
                throw new Exception(string.Format("Для бренда '{0}' не задано изображение.", brand));

            return brandPicture;
        }

        protected override string CreateKey(BrandPicture brandPicture)
        {
            if (string.IsNullOrEmpty(brandPicture.Brand))
                throw new Exception("Не задан бренд часов.");

            return brandPicture.Brand.Simplify();
        }

        protected override void OnDuplicateKeyCreated(string key, BrandPicture brandPicture)
        {
            throw new Exception(string.Format("Для бренда '{0}' уже было задано изображение.", brandPicture.Brand));
        }
    }
}
