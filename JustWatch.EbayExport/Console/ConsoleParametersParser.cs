﻿namespace JustWatch.EbayExport.Console
{
    public class ConsoleParametersParser
    {
        private const string StockParameter = "-stock";
        private const string MaxPriceParameter = "-maxprice";

        public ConsoleParameters Parse(string[] args)
        {
            var importParameters = new ConsoleParameters();

            for (var i = 0; i < args.Length - 1; i+=2)
            {
                Parse(importParameters, args[i], args[i + 1]);
            }

            return importParameters;
        }

        private void Parse(ConsoleParameters parameters, string paramName, string paramValue)
        {
            switch (paramName)
            {
                case StockParameter:
                    parameters.Stock = int.Parse(paramValue);
                    break;
                case MaxPriceParameter:
                    parameters.MaxPrice = int.Parse(paramValue);
                    break;
            }
        }
    }
}
