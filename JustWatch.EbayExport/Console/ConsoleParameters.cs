﻿namespace JustWatch.EbayExport.Console
{
    public class ConsoleParameters
    {
        public int? Stock { get; set; }
        public int? MaxPrice { get; set; }
    }
}
