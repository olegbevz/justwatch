﻿using JustWatch.EbayExport.BrandPictures;
using JustWatch.Common;
using JustWatch.WatchBook;
using JustWatch.EbayExport.YmlDocument;
using System;
using System.Globalization;

namespace JustWatch.EbayExport.YmlImporter
{
    public class OfferMapper : IMapper<Offer, Watch>
    {
        private readonly BrandPictureCache brandPictures;

        public OfferMapper(BrandPictureCache brandPictures)
        {
            this.brandPictures = brandPictures;
        }

        public int? Stock { get; set; }

        public Offer MapForward(Watch watch)
        {
            var offer = new Offer();

            offer.Id = watch.Article;
            offer.Price = watch.Price;
            offer.Pictures = watch.Pictures;
            offer.Name = watch.Name;
            offer.Available = true;
            offer.CategoryId = 14324;
            offer.CurrencyId = "RUR";

            offer.Stock = Stock;

            if (!string.IsNullOrEmpty(watch.Brand))
                offer.Parameters.Add("Бренд", watch.Brand);

            //if (!string.IsNullOrEmpty(watch.AdditionalFunctions))
            //    Offer.Parameters.Add("Дополнительные функции", watch.AdditionalFunctions);

            if (watch.Gender.HasValue)
            {
                offer.Parameters.Add("Пол", Watch.FormatWatchGender(watch.Gender.Value));
            }

            if (watch.Movement.HasValue)
            {
                switch (watch.Movement)
                {
                    case WatchMovement.Quartz:
                        offer.Parameters.Add("Часовой механизм", "Кварцевые (аккумулятор)");
                        break;
                    case WatchMovement.MechanicalAutomaticWindUp:
                        offer.Parameters.Add("Часовой механизм", "Механические (автоматические)");
                        break;
                    case WatchMovement.MechanicalManualWindUp:
                        offer.Parameters.Add("Часовой механизм", "Механические (ручной завод)");
                        break;
                    default:
                        throw new NotSupportedException("Unknown watch movement.");
                }
            }

            if (!string.IsNullOrEmpty(watch.DialMarkers))
                offer.Parameters.Add("Цифры", watch.DialMarkers);

            if (watch.Watertight.HasValue)
            {
                if (watch.Watertight.Value)
                {
                    if (!string.IsNullOrEmpty(watch.WaterResistance))
                        offer.Parameters.Add("Показатель водонепроницаемости", watch.WaterResistance);
                    else
                        offer.Parameters.Add("Водонепроницаемые", "да");
                }
                else
                {
                    offer.Parameters.Add("Водонепроницаемые", "нет");
                }
            }
            else
            {
                offer.Parameters.Add("Водонепроницаемые", "нет данных");
            }

            if (watch.CaseMaterial.HasValue)
                offer.Parameters.Add("Материал корпуса", Watch.FormatCaseMaterial(watch.CaseMaterial.Value));

            if (watch.StrapMaterial.HasValue)
                offer.Parameters.Add("Материал браслета", Watch.FormatStrapMaterial(watch.StrapMaterial.Value));

            if (watch.Crystal.HasValue)
                offer.Parameters.Add("Стекло", Watch.FormatWatchCrystal(watch.Crystal.Value));

            if (watch.DialType.HasValue)
                offer.Parameters.Add("Дисплей", Watch.FormatWatchDialType(watch.DialType.Value));

            if (!string.IsNullOrEmpty(watch.EnergySource))
                offer.Parameters.Add("Источник энергии", watch.EnergySource);

            if (watch.Size != null)
                offer.Parameters.Add("Размер, мм", watch.Size.ToString());

            if (watch.Weight.HasValue)
                offer.Parameters.Add("Вес", watch.Weight.Value.ToString(CultureInfo.InvariantCulture));

            if (watch.DateDisplay != null)
                offer.Parameters.Add("Отображение даты", watch.DateDisplay.ToString());

            if (!string.IsNullOrEmpty(watch.Guarantee))
                offer.Parameters.Add("Гарантия", watch.Guarantee);

            return offer;
        }

        public Watch MapBack(Offer target)
        {
            throw new NotImplementedException();
        }
    }
}
