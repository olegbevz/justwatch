﻿using JustWatch.EbayExport.BrandFormulas;
using JustWatch.EbayExport.BrandPictures;
using JustWatch.Common;
using JustWatch.EbayExport.CustomRules;
using JustWatch.EbayExport.Description;
using JustWatch.WatchBook;
using JustWatch.EbayExport.PriceList;
using JustWatch.EbayExport.YmlDocument;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using JustWatch.Prices;
using JustWatch.Yml;

namespace JustWatch.EbayExport.YmlImporter
{
    public class YmlImporter
    {
        private readonly PriceCalculator priceCalculator;
        private readonly BrandPictureCache brandPictures;
        private readonly CustomRuleCache customRuleCache;
        private readonly DescriptionProvider descriptionProvider;
        private readonly PriceListMapper priceListMapper;

        public YmlImporter(
            string description,
            IEnumerable<BrandFormula> brandFormulas,
            BrandPictureCache brandPictures, 
            CustomRuleCache customRuleCache)
        {
            this.priceCalculator = new PriceCalculator(brandFormulas);
            this.brandPictures = brandPictures;
            this.customRuleCache = customRuleCache;
            this.descriptionProvider = new DescriptionProvider(description);
            this.priceListMapper = new PriceListMapper();

            ProductsWithoutName = new Collection<string>();
            ProductsWithoutPicture = new Collection<string>();
            ProductsWithHigherPrice = new Collection<string>();
            BrandsWithoutCalculatedPrice = new Collection<string>();
        }

        public int? Stock { get; set; }
        public int? MaxPrice { get; set; }
        public int ImportedOffers { get; private set; }
        public Collection<string> ProductsWithoutName { get; private set; }
        public Collection<string> ProductsWithoutPicture { get; private set; }
        public Collection<string> ProductsWithHigherPrice { get; private set; }
        public Collection<string> BrandsWithoutCalculatedPrice { get; private set; }

        public void Import(
            IWatchProvider watchProvider,
            string priceListTemplate,
            string priceListFileName,
            Stream documentStream)
        {
            ImportedOffers = 0;
            ProductsWithoutName.Clear();
            ProductsWithoutPicture.Clear();
            ProductsWithHigherPrice.Clear();
            BrandsWithoutCalculatedPrice.Clear();

            var OfferMapper = new OfferMapper(brandPictures);
            OfferMapper.Stock = Stock;

            using (var priceListWriter = new PriceListWriter(priceListTemplate, priceListFileName))
            using (var ymlWriter = new YmlDocumentWriter(documentStream))
            {
                var shop = new Shop("justwatch.ru", "justwatch.ru", "http://www.ebay.com/usr/justwatch.ru");
                shop.Currencies = new[] { new Currency("RUR", 1) };
                shop.Categories = new[] { new Category(14324, "г. Санкт-Петербург. Телефон для связи: +7-999-219-4135") };

                ymlWriter.WriteHeader(shop);

                foreach (var watch in watchProvider.ImportWatches())
                {
                    // Check watch properties
                    if (!CheckWatch(watch))
                        continue;

                    // Calculate watch price
                    if (!CalculatePrice(watch))
                        continue;

                    // Get custom rule if exists
                    var customRule = customRuleCache.GetCustomRule(watch.Model);

                    // Apply custom rule to the watch properties
                    if (customRule != null)
                        ApplyCustomRule(watch, customRule);

                    // Add record to the price list
                    priceListWriter.WriteRecord(priceListMapper.MapForward(watch));

                    // If watch price exceeds the limit then ignore that watch
                    if (!CheckWatchPrice(watch))
                        continue;

                    // Read custom description if exisis
                    string customDescription = GetCustomDescription(customRule);

                    // Map watch model to YML record
                    var Offer = OfferMapper.MapForward(watch);

                    // Set offer record description
                    Offer.Description = descriptionProvider.GetDescription(CreateDescriptionTags(watch, customDescription));

                    // Apply custom rule properties if exists
                    if (customRule != null)
                        ApplyCustomRule(Offer, customRule);

                    // Write record to YML
                    ymlWriter.WriteOffer(Offer);

                    ImportedOffers++;
                }

                ymlWriter.WriteFooter();
            }
        }

        private string GetCustomDescription(CustomRule customRule)
        {
            string customDescription = null;
            if (customRule != null && !string.IsNullOrEmpty(customRule.DescriptionFile))
            {
                var descriptionPath = PathUtils.CombineWithCurrentDirectory(AppSettings.DescriptionsDirectory, customRule.DescriptionFile);
                customDescription = File.ReadAllText(descriptionPath, Encodings.RussianEncoding);
            }

            return customDescription;
        }

        private bool CalculatePrice(Watch watch)
        {
            var calculatedPrice = priceCalculator.CalculatePrice(watch.Brand, watch.Price);

            if (calculatedPrice.HasValue)
            {
                watch.Price = Math.Floor(calculatedPrice.Value);
                return true;
            }
            else
            {
                if (!BrandsWithoutCalculatedPrice.Contains(watch.Brand))
                    BrandsWithoutCalculatedPrice.Add(watch.Brand);
                return false;
            }    
        }

        private bool CheckWatch(Watch watch)
        {
            if (string.IsNullOrEmpty(watch.Article))
            {
                return false;
            }

            if (string.IsNullOrEmpty(watch.Name))
            {
                ProductsWithoutName.Add(watch.Article);
                return false;
            }

            if (watch.Pictures.Length == 0 ||
                watch.Pictures.All(picture => string.IsNullOrEmpty(picture)))
            {
                ProductsWithoutPicture.Add(watch.Name);
                return false;
            }

            return true;
        }

        public bool CheckWatchPrice(Watch watch)
        {
            if (MaxPrice.HasValue && watch.Price > MaxPrice.Value)
            {
                ProductsWithHigherPrice.Add(watch.Name);
                return false;
            }

            return true;
        }

        private void ApplyCustomRule(Watch watch, CustomRule customRule)
        {
            if (customRule.Price.HasValue)
                watch.Price = customRule.Price.Value;

            if (customRule.Discount.HasValue)
                watch.Price = watch.Price * (1 - customRule.Discount.Value / 100);
        }

        private void ApplyCustomRule(Offer Offer, CustomRule customRule)
        {
            if (customRule.Stock.HasValue)
                Offer.Stock = customRule.Stock.Value;
        }

        private Dictionary<string, string> CreateDescriptionTags(Watch watch, string customDescription)
        {
            var tags = new Dictionary<string, string>();
            tags.Add("name", watch.Name);

            if (watch.Gender.HasValue)
            {
                switch (watch.Gender)
                {
                    case WatchGender.Male:
                        tags.Add("Пол", "Мужские");
                        break;
                    case WatchGender.Female:
                        tags.Add("Пол", "Женские");
                        break;
                    case WatchGender.Unisex:
                        tags.Add("Пол", "Унисекс");
                        break;
                    default:
                        throw new NotSupportedException("Unknown watch sex.");
                }
            }
            else
            {
                tags.Add("Пол", null);
            }
            
            tags.Add("Способ отображения времени", watch.DialType.HasValue ? Watch.FormatWatchDialType(watch.DialType.Value) : null);
            tags.Add("Цифры", watch.DialMarkers);

            if (watch.Movement.HasValue)
            {
                switch (watch.Movement)
                {
                    case WatchMovement.Quartz:
                        tags.Add("Тип механизма", "Кварцевые (аккумулятор)");
                        break;
                    case WatchMovement.MechanicalAutomaticWindUp:
                        tags.Add("Тип механизма", "Механические (автоматические)");
                        break;
                    case WatchMovement.MechanicalManualWindUp:
                        tags.Add("Тип механизма", "Механические (ручной завод)");
                        break;
                    default:
                        throw new NotSupportedException("Unknown watch mechanism.");
                }
            }
            else
            {
                tags.Add("Тип механизма", null);
            }

            tags.Add("Источник энергии", watch.EnergySource);
            tags.Add("Водонепроницаемые", watch.WaterResistance);
            tags.Add("Материал корпуса", watch.CaseMaterial.HasValue ? Watch.FormatCaseMaterial(watch.CaseMaterial.Value) : null);
            tags.Add("Материал браслета", watch.StrapMaterial.HasValue ? Watch.FormatStrapMaterial(watch.StrapMaterial.Value) : null);
            tags.Add("Стекло", watch.Crystal.HasValue ? Watch.FormatWatchCrystal(watch.Crystal.Value) : null);
            tags.Add("Отображение даты", watch.DateDisplay?.ToString());
            tags.Add("Дополнительные функции", watch.AdditionalFunctions);
            tags.Add("Размер", watch.Size?.ToString());
            tags.Add("Вес", watch.Weight?.ToString(CultureInfo.InvariantCulture));
            tags.Add("Гарантия", watch.Guarantee);

            var descriptionPicture = watch.DescriptionPicture;
            if (string.IsNullOrEmpty(descriptionPicture))
                descriptionPicture = watch.Pictures.FirstOrDefault();

            tags.Add("picture", descriptionPicture);

            var brandPicture = brandPictures.GetBrandPicture(watch.Brand);
            if (brandPicture != null)
            {
                tags.Add("brand picture", brandPicture.Url);
                tags.Add("width", brandPicture.Width.ToString());
                tags.Add("height", brandPicture.Height.ToString());
            }

            foreach (var tag in tags.ToArray())
            {
                if (string.IsNullOrEmpty(tag.Value))
                {
                    tags[tag.Key] = "нет данных";
                }
            }

            tags.Add("add", customDescription);

            return tags;
        }
    }
}
