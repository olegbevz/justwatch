﻿using JustWatch.Common;
using JustWatch.WatchBook;
using JustWatch.EbayExport.PriceList;
using System;
using System.Globalization;

namespace JustWatch.EbayExport.YmlImporter
{
    public class PriceListMapper : IMapper<PriceListRecord, Watch>
    {
        public PriceListRecord MapForward(Watch watch)
        {
            var priceListRecord = new PriceListRecord
            {
                Brand = watch.Brand,
                Model = watch.Model,
                Price = watch.Price.ToString(CultureInfo.InvariantCulture )
            };

            if (watch.Gender.HasValue)
            {
                priceListRecord.Sex = Watch.FormatWatchGender(watch.Gender.Value);
            }

            if (watch.Movement.HasValue)
            {
                switch (watch.Movement)
                {
                    case WatchMovement.Quartz:
                        priceListRecord.WatchMechanism = "Кварцевые (аккумулятор)";
                        break;
                    case WatchMovement.MechanicalAutomaticWindUp:
                        priceListRecord.WatchMechanism = "Механические (автоматические)";
                        break;
                    case WatchMovement.MechanicalManualWindUp:
                        priceListRecord.WatchMechanism = "Механические (ручной завод)";
                        break;
                    default:
                        throw new NotSupportedException("Unknown watch mechanism.");
                }
            }

            return priceListRecord;
        }

        public Watch MapBack(PriceListRecord target)
        {
            throw new NotImplementedException();
        }
    }
}
