﻿using System.Collections.Generic;
using JustWatch.Common;
using System;

namespace JustWatch.EbayExport.CustomRules
{
    public class CustomRuleCache : CacheBase<CustomRule>
    {
        public CustomRuleCache(IEnumerable<CustomRule> customRules) : base(customRules)
        {
        }

        public CustomRule GetCustomRule(string model)
        {
            return GetByKey(model.Simplify());
        }

        protected override string CreateKey(CustomRule customRule)
        {
            if (string.IsNullOrEmpty(customRule.WatchModel))
                throw new Exception("Не задана модель часов.");

            return customRule.WatchModel.Simplify();
        }

        protected override void OnDuplicateKeyCreated(string key, CustomRule customRule)
        {
            throw new Exception(string.Format("Для часов модели '{0}' уже были заданы параметры", customRule.WatchModel));
        }
    }
}
