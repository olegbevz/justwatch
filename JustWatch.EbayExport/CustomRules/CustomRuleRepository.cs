﻿using JustWatch.Common;

namespace JustWatch.EbayExport.CustomRules
{
    public class CustomRuleRepository : LinqToExcelRepository<CustomRule>
    {
        public CustomRuleRepository(string fileName) : base(fileName)
        {
        }
    }
}
