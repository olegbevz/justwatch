﻿using LinqToExcel.Attributes;
using System.IO;

namespace JustWatch.EbayExport.CustomRules
{
    public class CustomRule
    {
        [ExcelColumn("модель")]
        public string WatchModel { get; set; }

        [ExcelColumn("сток")]
        public int? Stock { get; set; }

        [ExcelColumn("цена")]
        public double? Price { get; set; }

        [ExcelColumn("скидка")]
        public double? Discount { get; set; }

        [ExcelColumn("файл")]
        public string DescriptionFile { get; set; }
    }
}
