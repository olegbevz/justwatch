﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace JustWatch.EbayExport.Description
{
    public class DescriptionProvider
    {
        private readonly string descriptionTemplate;
        private static readonly Regex tagRegex = new Regex(@"\$\w+\$");

        public DescriptionProvider(string description)
        {
            this.descriptionTemplate = description;
        }

        public string GetDescription(Dictionary<string, string> tags)
        {
            var description = descriptionTemplate;

            foreach (var tag in tags)
            {
                description = description.Replace(string.Format("${0}$", tag.Key), tag.Value);
            }

            var tagMatch = tagRegex.Match(description);
            if (tagMatch != null && tagMatch.Success)
                throw new Exception($"Product description contains unreplaced tag '{tagMatch.Value}'");

            return description;
        }
    }
}
