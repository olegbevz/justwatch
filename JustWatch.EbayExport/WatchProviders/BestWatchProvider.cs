﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using JustWatch.BestWatch;
using JustWatch.Common;
using JustWatch.WatchBook;

namespace JustWatch.EbayExport
{
    public class BestWatchProvider : IWatchProvider
    {
        private readonly ProductsDocumentReader productsReader;
        private readonly WatchCache watchCache;

        public BestWatchProvider(ProductsDocumentReader productsReader, WatchCache watchCache)
        {
            this.productsReader = productsReader;
            this.watchCache = watchCache;

            ProductsWithoutDetails = new Collection<string>();
        }

        public Collection<string> ProductsWithoutDetails { get; private set; }

        public IEnumerable<Watch> ImportWatches()
        {
            return productsReader.ReadProducts().Where(product => CheckProduct(product)).Select(product => CreateWatch(product)).Select(watch => MergeWatch(watch));
        }

        private bool CheckProduct(Product product)
        {
            if (product.Type != "Часы")
                return false;

            if (product.Mechanism == "Термометр")
                return false;

            return true;
        }

        private Watch CreateWatch(Product product)
        {
            var watch = new Watch();
            watch.Article = product.Articul;
            watch.Model = product.Articul;
            watch.Price = product.Price;
            watch.Pictures = new string[] { product.Image };
            watch.Name = string.Format("Оригинальные наручные часы {0} {1} {2}", product.Brand.ToFirstLetterUppercase(), product.Articul, product.Sex);
            watch.Brand = product.Brand.ToFirstLetterUppercase();
            if (!string.IsNullOrEmpty(product.Sex))
                watch.Gender = ParseWatchGender(product.Sex);
            if (!string.IsNullOrEmpty(product.Mechanism))
                watch.Movement = ParseWatchMechanism(product.Mechanism);
            watch.Watertight = !string.IsNullOrEmpty(product.Waterproof);
            watch.WaterResistance = product.Waterproof;
            if (!string.IsNullOrEmpty(product.Material))
                watch.CaseMaterial = ParseCaseMaterial(product.Material);
            if (!string.IsNullOrEmpty(product.Glass))
                watch.Crystal = ParseWatchCrystal(product.Glass);
            watch.Size = new WatchSize(product.Width, null, product.Depth);
            return watch;
        }

        private Watch MergeWatch(Watch watch)
        {
            var watchFromRepository = watchCache.GetWatch(watch.Brand, watch.Model);
            if (watchFromRepository == null)
            {
                ProductsWithoutDetails.Add(string.Format("{0} {1}", watch.Brand, watch.Model));
                return watch;
            }

            watchFromRepository.Article = watch.Article;

            return watchFromRepository.Merge(watch);
        }

        private WatchGender ParseWatchGender(string watchGender)
        {
            switch (watchGender)
            {
                case "Мужские":
                    return WatchGender.Male;
                case "Женские":
                    return WatchGender.Female;
                case "Унисекс":
                    return WatchGender.Unisex;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить пол для часов: \"{0}\".", watchGender));
            }
        }

        private WatchMovement ParseWatchMechanism(string mechanism)
        {
            switch (mechanism)
            {
                case "Электронный":
                case "Электронные часы":
                case "Кварцевый":
                case "Кварцевый хронограф":
                case "Кварцевые часы":
                    return WatchMovement.Quartz;
                case "Механические часы с автоподзаводом":
                case "Механический хронограф с автоподзаводом":
                case "Механический хронометр с автоподзаводом":
                    return WatchMovement.MechanicalAutomaticWindUp;
                case "Механические часы с ручным заводом":
                    return WatchMovement.MechanicalManualWindUp;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить тип механизма часов: \"{0}\".", mechanism));
            }
        }

        private static WatchCrystal ParseWatchCrystal(string glass)
        {
            switch (glass)
            {
                case "Минеральное стекло":
                    return WatchCrystal.Mineral;
                case "Сапфировое стекло":
                    return WatchCrystal.Sapphire;
                case "Пластиковое стекло":
                    return WatchCrystal.Plastic;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал стекла: '{0}'.", glass));
            }
        }

        private static WatchCaseMaterial ParseCaseMaterial(string caseMaterial)
        {
            switch (caseMaterial)
            {
                case "Пластик":
                case "Покрытия":
                    return WatchCaseMaterial.Plastic;
                case "Нержавеющая сталь":
                    return WatchCaseMaterial.StainlessSteel;
                //case "Полиуретан":
                //    return WatchCaseMaterial.Polyurethane;
                case "Титан":
                    return WatchCaseMaterial.Titanium;
                //case "Полимер":
                //    return WatchCaseMaterial.Polymer;
                //case "Алюминий":
                //    return WatchCaseMaterial.Aluminum;
                //case "Латунь":
                //    return WatchCaseMaterial.Aluminum;
                case "Керамика":
                    return WatchCaseMaterial.Ceramics;
                //case "Медь":
                //    return WatchCaseMaterial.Copper;
                //case "Композит":
                case "Сплав":
                    return WatchCaseMaterial.Composite;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал корпуса: '{0}'.", caseMaterial));
            }
        }
    }
}
