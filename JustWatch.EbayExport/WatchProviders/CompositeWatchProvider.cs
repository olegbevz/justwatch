﻿using System.Collections.Generic;
using System.Linq;
using JustWatch.WatchBook;
using JustWatch.Common;

namespace JustWatch.EbayExport
{
    public class CompositeWatchProvider : IWatchProvider
    {
        private IEnumerable<IWatchProvider> watchProviders;

        public CompositeWatchProvider(params IWatchProvider[] watchProviders)
        {
            this.watchProviders = watchProviders;
        }

        public IEnumerable<Watch> ImportWatches()
        {
            var cache = new HashSet<string>(); 

            foreach (var watch in watchProviders.SelectMany(provider => provider.ImportWatches()))
            {
                var key = watch.Brand.Simplify() + watch.Model.Simplify();

                if (cache.Contains(key))
                    continue;

                cache.Add(key);

                yield return watch;
            }
        }
    }
}
