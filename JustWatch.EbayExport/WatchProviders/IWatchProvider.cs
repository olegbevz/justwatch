﻿using System.Collections.Generic;
using JustWatch.WatchBook;

namespace JustWatch.EbayExport
{
    public interface IWatchProvider
    {
        IEnumerable<Watch> ImportWatches();
    }
}
