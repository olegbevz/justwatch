﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using JustWatch.Delta;
using JustWatch.WatchBook;

namespace JustWatch.EbayExport
{
    public class DeltaWatchProvider : IWatchProvider
    {
        private readonly PricesReader pricesReader;
        private readonly WatchCache watchCache;

        public DeltaWatchProvider(PricesReader pricesReader, WatchCache watchCache)
        {
            this.pricesReader = pricesReader;
            this.watchCache = watchCache;

            ProductsWithoutDetails = new Collection<string>();
        }

        public Collection<string> ProductsWithoutDetails { get; private set; }

        public IEnumerable<Watch> ImportWatches()
        {
            foreach (var priceRecord in pricesReader.ReadPrices())
            {
                var watch = watchCache.GetWatch(priceRecord.Brand, priceRecord.Model);
                if (watch == null)
                {
                    ProductsWithoutDetails.Add(string.Format("{0} {1}", priceRecord.Brand, priceRecord.Model));
                    continue;
                }

                watch.Price = priceRecord.Price;

                yield return watch;
            }
        }
    }
}
