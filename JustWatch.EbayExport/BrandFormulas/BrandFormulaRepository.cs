﻿using JustWatch.Common;
using JustWatch.Prices;

namespace JustWatch.EbayExport.BrandFormulas
{
    public class BrandFormulaRepository : LinqToExcelRepository<BrandFormula>
    {
        public BrandFormulaRepository(string fileName) : base(fileName)
        {
            this.excelQueryFactory.AddMapping<BrandFormula>(x => x.Brand, "Name");
            this.excelQueryFactory.AddMapping<BrandFormula>(x => x.Formula, "Price");
        }
    }
}
