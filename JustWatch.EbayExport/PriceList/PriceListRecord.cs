﻿namespace JustWatch.EbayExport.PriceList
{
    public class PriceListRecord
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Sex { get; set; }
        public string WatchMechanism { get; set; }
        public string Price { get; set; }
    }
}
