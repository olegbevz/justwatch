﻿using OfficeOpenXml;
using System;
using System.IO;

namespace JustWatch.EbayExport.PriceList
{
    public class PriceListWriter : IDisposable
    {
        private readonly ExcelPackage templateExcelPackage;
        private readonly string outputFileName;
        private readonly ExcelWorksheet worksheet;

        private int currentRow;

        public PriceListWriter(string templateFileName, string outputFileName)
        {
            this.templateExcelPackage =  new ExcelPackage(new FileInfo(templateFileName));
            this.worksheet = templateExcelPackage.Workbook.Worksheets[1];
            this.outputFileName = outputFileName;

            this.currentRow = 2;
        }

        public void WriteRecord(PriceListRecord record)
        {
            for (int i = 1; i <= 5; i++)
            {
                worksheet.Cells[currentRow, i].StyleName = "JustWatch";
            }

            worksheet.Cells[currentRow, 1].Value = record.Brand;
            worksheet.Cells[currentRow, 2].Value = record.Model;
            worksheet.Cells[currentRow, 3].Value = record.Sex;
            worksheet.Cells[currentRow, 4].Value = record.WatchMechanism;
            worksheet.Cells[currentRow, 5].Value = record.Price;

            currentRow++;
        }

        void IDisposable.Dispose()
        {
            templateExcelPackage.SaveAs(new FileInfo(outputFileName));
        }
    }
}
