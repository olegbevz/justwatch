﻿﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
 using JustWatch.BestWatch;
 using JustWatch.Delta;

namespace JustWatch.EbayExport
{
    using BrandFormulas;
    using BrandPictures;
    using Console;
    using CustomRules;
    using Common;
    using WatchBook;
    using Yml;
    using System.Net;
    using FluentFTP;
    public class Program
    {  
        static void Main(string[] args)
        {
            try
            {
                ConsoleUtils.WriteApplicationInfo();

                var parametersParser = new ConsoleParametersParser();
                var parameters = parametersParser.Parse(args);

                var ymlImporter = CreateYmlImporter();

                ymlImporter.MaxPrice = parameters.MaxPrice;
                if (parameters.Stock.HasValue)
                    ymlImporter.Stock = parameters.Stock.Value;

                var watchCache = CreateWatchCache();
                var deltaWatchProvider = CreateDeltaWatchProvider(watchCache);
                var bestWatchProvider = CreateBestWatchProvider(watchCache);

                var ymlFileName = PathUtils.CombineWithCurrentDirectory(AppSettings.YmlFileName);
                using (var memoryStream = new FileStream(ymlFileName, FileMode.Create, FileAccess.Write))
                {
                    System.Console.WriteLine("Импортируем yml документ...");

                    ymlImporter.Import(
                        new CompositeWatchProvider(deltaWatchProvider, bestWatchProvider),
                        GetPriceListTemplateFileName(),
                        GetPriceListFileName(),
                        memoryStream);

                    System.Console.WriteLine("Документ создан.");
                }

                using (var fileStream = File.OpenRead(ymlFileName))
                {
                    ValidateYmlSchema(fileStream);
                }

                System.Console.WriteLine("Выгружаем документ на ftp сервер... ");
                UploadFileToFtpServer(ymlFileName);
                System.Console.WriteLine($"Документ {AppSettings.FtpDirectory}/{AppSettings.YmlFileName} успешно выгружен на ftp сервер {AppSettings.FtpHost}");

                var productsWithoutDetails = deltaWatchProvider
                    .ProductsWithoutDetails
                    .Concat(bestWatchProvider.ProductsWithoutDetails)
                    .ToArray();

                DisplayImportStatus(
                    ymlImporter.ImportedOffers,
                    ymlImporter.ProductsWithoutName.Count,
                    productsWithoutDetails.Length,
                    ymlImporter.ProductsWithoutPicture.Count,
                    ymlImporter.ProductsWithHigherPrice.Count,
                    ymlImporter.BrandsWithoutCalculatedPrice.Count);

                SaveImportStatus(
                   ymlImporter.ProductsWithoutName,
                   productsWithoutDetails,
                   ymlImporter.ProductsWithoutPicture,
                   ymlImporter.BrandsWithoutCalculatedPrice);

                ConsoleUtils.AskToPressAnyKey();
            }
            catch (Exception ex)
            {
                ConsoleUtils.WriteException(ex);
                Logger.LogException(ex);
                ConsoleUtils.AskToPressAnyKey();
            }
        }

        private static string GetYmlFileName()
        {
            return PathUtils.CombineWithCurrentDirectory(AppSettings.OutputDirectory, AppSettings.YmlFileName);
        }

        private static string GetPriceListTemplateFileName()
        {
            return PathUtils.CombineWithCurrentDirectory(AppSettings.PriceListTemplate);
        }

        private static string GetPriceListFileName()
        {
            return PathUtils.CombineWithCurrentDirectory(string.Format("Justwatchrus_Price_{0}.xlsx", DateTime.Now.ToString("ddMMyy")));
        }

        private static WatchCache CreateWatchCache()
        {
            var filePath = PathUtils.CombineWithCurrentDirectory(AppSettings.WatchBook);

            try
            {
                var watchRepository = new WatchRepository(filePath);
                return new WatchCache(watchRepository.GetWatches());
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Ошибка при чтении данных из файла '{0}'.", filePath), ex);
            }
        }

        private static DeltaWatchProvider CreateDeltaWatchProvider(WatchCache watchCache)
        {
            var pricesFolder = PathUtils.CombineWithCurrentDirectory(AppSettings.DeltaPrices);

            var deltaWatchProvider = new DeltaWatchProvider(
                new PricesReader(pricesFolder),
                watchCache);

            return deltaWatchProvider;
        }

        private static BestWatchProvider CreateBestWatchProvider(WatchCache watchCache)
        {
            var productsDocumentPath = PathUtils.CombineWithCurrentDirectory(AppSettings.BestWatchProducts);

            return new BestWatchProvider(
                new ProductsDocumentReader(productsDocumentPath),
                watchCache);
        }

        private static YmlImporter.YmlImporter CreateYmlImporter()
        {
            var brandFormulasFilePath = PathUtils.CombineWithCurrentDirectory(AppSettings.Pricing);
            var brandFormulaRepository = new BrandFormulaRepository(brandFormulasFilePath);
            var brandFormulas = brandFormulaRepository.GetAll();

            var brandPictureCache = CreateBrandPictureCache();
            var customRulesCache = CreateCustomRuleCache();

            var companyDescription = GetCompanyDescription();

            return new YmlImporter.YmlImporter(
                companyDescription, 
                brandFormulas, 
                brandPictureCache, 
                customRulesCache);
        }

        private static BrandPictureCache CreateBrandPictureCache()
        {
            var brandPicturesFilePath = PathUtils.CombineWithCurrentDirectory(AppSettings.BrandPictures);
            try
            {
                var brandPictureRepository = new BrandPictureRepository(brandPicturesFilePath);
                return new BrandPictureCache(brandPictureRepository.GetAll());
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Ошибка при чтении данных из файла '{0}'.", brandPicturesFilePath), ex);
            }
        }

        private static CustomRuleCache CreateCustomRuleCache()
        {
            var customRulesFilePath = PathUtils.CombineWithCurrentDirectory(AppSettings.CustomRules);

            try
            {
                var customRulesRepository = new CustomRuleRepository(customRulesFilePath);
                return new CustomRuleCache(customRulesRepository.GetAll());
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Ошибка при чтении данных из файла '{0}'.", customRulesFilePath), ex);
            }
        }

        private static string GetCompanyDescription()
        {
            var descriptionFilePath = PathUtils.CombineWithCurrentDirectory(AppSettings.Description);
            return File.ReadAllText(descriptionFilePath, Encodings.RussianEncoding);
        }

        private static void ValidateYmlSchema(Stream documentStream)
        {
            System.Console.WriteLine("Проверка документа на соответсвие схеме dtd...");

            var schemaValidator = new DtdSchemaValidator();
            var validationResult = schemaValidator.Validate(documentStream);
            if (validationResult.Errors.Count > 0)
            {
                var messageBuilder = new StringBuilder();
                messageBuilder.AppendLine("Схема yml документа некорректна.");
                foreach (var error in validationResult.Errors)
                    messageBuilder.AppendLine(error);
                throw new Exception(messageBuilder.ToString());
            }
            else
            {
                System.Console.WriteLine("Схема yml документа корректна.");
            }
        }

        private static void SaveImportStatus(
            IEnumerable<string> productsWithoutName,
            IEnumerable<string> productsWithoutDetails,
            IEnumerable<string> productsWithoutPicture,
            IEnumerable<string> productsWithoutCalculatedPrice)
        {
            var productsWithoutNamePath = PathUtils.CombineWithCurrentDirectory(AppSettings.ProductsWithoutName);
            File.WriteAllLines(productsWithoutNamePath, productsWithoutName);

            var productsWithoutDetailsPath = PathUtils.CombineWithCurrentDirectory(AppSettings.ProductsWithoutDetails);
            File.WriteAllLines(productsWithoutDetailsPath, productsWithoutDetails);

            var productsWithoutPicturePath = PathUtils.CombineWithCurrentDirectory(AppSettings.ProductsWithoutPicture);
            File.WriteAllLines(productsWithoutPicturePath, productsWithoutPicture);

            var productsWithoutCalculatedPricePath = PathUtils.CombineWithCurrentDirectory(AppSettings.BrandsWithoutFormula);
            File.WriteAllLines(productsWithoutCalculatedPricePath, productsWithoutCalculatedPrice);
        }

        private static void DisplayImportStatus(
            int importedOffers, 
            int productsWithoutName, 
            int productsWithoutDetails, 
            int productsWithoutPicture, 
            int productsWithHigherPrice, 
            int productsWithoutCalculatedPrice)
        {
            System.Console.WriteLine("Импортировано товаров: {0}", importedOffers);
            System.Console.WriteLine("Товаров без имени: {0}", productsWithoutName);
            System.Console.WriteLine("Товаров без описания: {0}", productsWithoutDetails);
            System.Console.WriteLine("Товаров без картинки: {0}", productsWithoutPicture);
            System.Console.WriteLine("Товаров с ценой выше максимума: {0}", productsWithHigherPrice);
            System.Console.WriteLine("Товаров без рассчитанной цены: {0}", productsWithoutCalculatedPrice);
        }

        private static void UploadFileToFtpServer(string fileName)
        {
            using (var fileStream = File.OpenRead(fileName))
            {
                UploadStreamToFtpServer(fileStream);
            }
        }

        private static void UploadStreamToFtpServer(Stream stream)
        {
            var credentials = new NetworkCredential(AppSettings.FtpUser, AppSettings.FtpPassword);

            using (var ftpClient = new FtpClient(AppSettings.FtpHost, credentials))
            {
                ftpClient.Connect();

                ftpClient.Upload(
                    stream,
                    Path.Combine(AppSettings.FtpDirectory, AppSettings.YmlFileName),
                    FtpExists.Overwrite);
            }
        }
    }
}
