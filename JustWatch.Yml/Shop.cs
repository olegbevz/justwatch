﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JustWatch.Yml
{
    public class Shop
    {
        public Shop()
        {
        }

        public Shop(string name, string company, string url)
        {
            Name = name;
            Company = company;
            Url = url;
        }

        public string Name { get; set; }
        public string Company { get; set; }
        public string Url { get; set; }
        public IEnumerable<Currency> Currencies { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}
