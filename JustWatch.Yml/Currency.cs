﻿namespace JustWatch.Yml
{
    public class Currency
    {
        public Currency(string id, double rate)
        {
            Id = id;
            Rate = rate;
        }

        public string Id { get; set; }
        public double Rate { get; set; }
    }
}
