﻿using JustWatch.Yml;
using System;
using System.Globalization;
using System.IO;
using System.Xml;

namespace JustWatch.EbayExport.YmlDocument
{
    public class YmlDocumentWriter : IDisposable
    {
        private readonly XmlWriter writer;

        public YmlDocumentWriter(Stream output)
        {
             var xmlWriterSettings = new XmlWriterSettings()
              {
                Indent = true,
                IndentChars = "\t"
              };

            writer = XmlWriter.Create(output, xmlWriterSettings);
        }

        public YmlDocumentWriter(string fileName) 
            : this(File.OpenWrite(fileName))
        {
        }

        public void WriteHeader(Shop shop)
        {
            writer.WriteDocType("yml_catalog", null, "shops.dtd", null);
            writer.WriteStartElement("yml_catalog");
            writer.WriteAttributeString("date", DateTime.Now.ToString("yyyy-MM-dd HH-mm"));

            writer.WriteStartElement("shop");

                writer.WriteElementString("name", shop.Name);
                writer.WriteElementString("company", shop.Company);
                writer.WriteElementString("url", shop.Url);

                if (shop.Currencies != null)
                {
                    writer.WriteStartElement("currencies");
                    foreach (var currency in shop.Currencies)
                    {
                        writer.WriteStartElement("currency");
                        writer.WriteAttributeString("id", currency.Id);
                        writer.WriteAttributeString("rate", currency.Rate.ToString(CultureInfo.InvariantCulture));
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }

                if (shop.Categories != null)
                {
                    writer.WriteStartElement("categories");
                    foreach (var category in shop.Categories)
                    {
                        writer.WriteStartElement("category");
                        writer.WriteAttributeString("id", category.Id.ToString());
                        writer.WriteString(category.Name);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
                writer.WriteStartElement("offers");

            writer.Flush();
        }

        public void WriteOffer(Offer offerRecord)
        {
            writer.WriteStartElement("offer");
                writer.WriteAttributeString("id", offerRecord.Id);
                writer.WriteAttributeString("available", offerRecord.Available.ToString().ToLowerInvariant());

                if (!string.IsNullOrEmpty(offerRecord.Type))
                    writer.WriteAttributeString("type", offerRecord.Type);

                if (!string.IsNullOrEmpty(offerRecord.Url))
                    writer.WriteElementString("url", offerRecord.Url);

                writer.WriteElementString("price", ((int)offerRecord.Price).ToString(CultureInfo.InvariantCulture));
                writer.WriteElementString("currencyId", offerRecord.CurrencyId);
                writer.WriteElementString("categoryId", offerRecord.CategoryId.ToString());

                if (offerRecord.Pictures != null)
                {
                    foreach (var picture in offerRecord.Pictures)
                    {
                        writer.WriteElementString("picture", picture);
                    }
                }

                if (offerRecord.Pickup != null)
                    writer.WriteElementString("pickup", offerRecord.Pickup.Value.ToString().ToLowerInvariant());

                if (offerRecord.Delivery != null)
                    writer.WriteElementString("delivery", offerRecord.Delivery.Value.ToString().ToLowerInvariant());

                if (!string.IsNullOrEmpty(offerRecord.Name))
                    writer.WriteElementString("name", offerRecord.Name);

                if (!string.IsNullOrEmpty(offerRecord.TypePrefix))
                    writer.WriteElementString("typePrefix", offerRecord.TypePrefix);

                if (!string.IsNullOrEmpty(offerRecord.Vendor))
                    writer.WriteElementString("vendor", offerRecord.Vendor);

                if (!string.IsNullOrEmpty(offerRecord.VendorCode))
                    writer.WriteElementString("vendorCode", offerRecord.VendorCode);

                if (!string.IsNullOrEmpty(offerRecord.Model))
                    writer.WriteElementString("model", offerRecord.Model);

                if (!string.IsNullOrEmpty(offerRecord.Description))
                {
                    var description = $"<![CDATA[{offerRecord.Description}]]>";
                    writer.WriteStartElement("description");
                    writer.WriteRaw(description);
                    writer.WriteEndElement();
                }

                if (!string.IsNullOrEmpty(offerRecord.SalesNotes))
                {
                    writer.WriteElementString("sales_notes", offerRecord.SalesNotes);
                }

                if (offerRecord.ManufacturerWarranty != null)
                {
                    writer.WriteElementString(
                        "manufacturer_warranty", 
                        offerRecord.ManufacturerWarranty.ToString().ToLowerInvariant());
                }

                foreach (var parameter in offerRecord.Parameters)
                {
                    writer.WriteStartElement("param");
                    writer.WriteAttributeString("name", parameter.Key);
                    writer.WriteString(parameter.Value);
                    writer.WriteEndElement();
                }

                if (offerRecord.Stock != null)
                {
                    writer.WriteStartElement("stock");
                    writer.WriteString(offerRecord.Stock.Value.ToString(CultureInfo.InvariantCulture));
                    writer.WriteEndElement();
                }

            writer.WriteEndElement();
        }

        public void WriteFooter()
        {
                    writer.WriteEndElement();
                writer.WriteEndElement();
            writer.WriteEndElement();

            writer.Flush();
        }

        public void Dispose()
        {
            writer.Close();
        }
    }
}
