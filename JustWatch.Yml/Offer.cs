﻿using System.Collections.Generic;

namespace JustWatch.EbayExport.YmlDocument
{
    public class Offer
    {
        public Offer()
        {
            Parameters = new Dictionary<string, string>();
        }

        public string Id { get; set; }
        public string Type { get; set; }
        public bool Available { get; set; }

        public string Url { get; set; }
        public double Price { get; set; }
        public string CurrencyId { get; set; }
        public int CategoryId { get; set; }
        public string[] Pictures { get; set; }

        public string TypePrefix { get; set; }
        public string Vendor { get; set; }
        public string Model { get; set; }
        public string VendorCode { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public bool? Pickup { get; set; }
        public bool? Delivery { get; set; }
        public string SalesNotes { get; set; }
        public bool? ManufacturerWarranty { get; set; }

        public Dictionary<string, string> Parameters { get; private set; }

        public int? Stock { get; set; }
    }
}
