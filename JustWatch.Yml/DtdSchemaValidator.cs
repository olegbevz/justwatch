﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace JustWatch.Yml
{
    public class DtdSchemaValidator
    {
        private readonly SchemaValidationResult validationResult = new SchemaValidationResult();

        public SchemaValidationResult Validate(string fileName)
        {
            using (var inputStream = File.OpenRead(fileName))
            {
                return Validate(inputStream);
            }
        }

        public SchemaValidationResult Validate(Stream xmlStream)
        {
            validationResult.Warnings.Clear();
            validationResult.Errors.Clear();

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.DTD;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationEventHandler);

            XmlReader xmlReader = XmlReader.Create(xmlStream, settings);

            while (xmlReader.Read()) { }
            xmlReader.Close();

            return validationResult;
        }

        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                validationResult.Warnings.Add(e.Message);
            }
            else if (e.Severity == XmlSeverityType.Error)
            {
                validationResult.Errors.Add(e.Message);
            }
        }
    }

    public class SchemaValidationResult
    {
        public SchemaValidationResult()
        {
            Warnings = new Collection<string>();
            Errors = new Collection<string>();
        }

        public ICollection<string> Warnings { get; private set; }
        public ICollection<string> Errors { get; private set; }
    }
}
