﻿using System;
using System.IO;

namespace JustWatch.Common
{
    public static class Logger
    {
        private static string ErrorFile = "error.txt";

        public static void LogException(Exception ex)
        {
            var errorFilePath = PathUtils.CombineWithCurrentDirectory(ErrorFile);
            using (var fileStream = File.OpenWrite(errorFilePath))
            {
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    LogException(streamWriter, ex);
                }
            }
        }

        public static void LogException(StreamWriter streamWriter, Exception ex)
        {
            streamWriter.WriteLine(ex.Message);
            streamWriter.WriteLine(ex.StackTrace);

            if (ex.InnerException != null)
            {
                streamWriter.WriteLine("Inner exception:");
                LogException(streamWriter, ex.InnerException);
            }
        }
    }
}
