﻿using System;
using System.Reflection;
using System.Linq;

namespace JustWatch.Common
{
    public static class ConsoleUtils
    {
        public static void WriteException(Exception ex)
        {
            Console.WriteLine(ex.Message);

            if (ex.InnerException != null)
                WriteException(ex.InnerException);
        }        

        public static void AskToPressAnyKey()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public static bool Ask(string question)
        {
            Console.WriteLine("{0} (y/n)", question);

            while (true)
            {
                var key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.Y:
                        Console.WriteLine();
                        return true;
                    case ConsoleKey.N:
                        Console.WriteLine();
                        return false;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("Please enter 'y' or 'n' to answer the question...");
                        continue;
                }
            }
        }

        public static void WriteApplicationInfo()
        {
            var currentAssembly = Assembly.GetCallingAssembly();
            if (currentAssembly == null)
                throw new Exception("Could not get calling assembly.");

            var assemblyName = currentAssembly.GetName();

            Console.OutputEncoding = System.Text.Encoding.Unicode; ;
            Console.WriteLine("{0} version {1}", assemblyName.Name, assemblyName.Version);

            var companyAttribute = currentAssembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false)
                .OfType<AssemblyCompanyAttribute>()
                .FirstOrDefault();
            var copyrightAttribute = currentAssembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false)
                .OfType<AssemblyCopyrightAttribute>()
                .FirstOrDefault();

            if (companyAttribute != null && copyrightAttribute != null)
                Console.WriteLine("{0} {1}", copyrightAttribute.Copyright, companyAttribute.Company);
            Console.WriteLine();
        }
    }
}