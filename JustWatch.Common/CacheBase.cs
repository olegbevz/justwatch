﻿using System;
using System.Collections.Generic;

namespace JustWatch.Common
{
    public abstract class CacheBase<T> where T : class
    {
        private readonly IEnumerable<T> elements;
        private Dictionary<string, T> dictionary;

        protected CacheBase(IEnumerable<T> elements)
        {
            this.elements = elements;
        }

        protected abstract string CreateKey(T element);

        protected T GetByKey(string key)
        {
            if (dictionary == null)
                Init();

            T targetElement = null;

            dictionary.TryGetValue(key, out targetElement);

            return targetElement;
        }

        public void Init()
        {
            if (dictionary != null)
                throw new Exception("Cache was already initialized");

            dictionary = new Dictionary<string, T>();

            foreach (var element in elements)
            {
                var elementKey = CreateKey(element);

                if (dictionary.ContainsKey(elementKey))
                {
                    OnDuplicateKeyCreated(elementKey, element);
                    continue;
                }

                dictionary.Add(elementKey, element);
            }
        }

        protected virtual void OnDuplicateKeyCreated(string key, T element)
        {
            throw new Exception(string.Format("Duplicate element in the cache: {0}", key));
        }
    }
}
