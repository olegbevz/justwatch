﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace JustWatch.Common
{
    public static class StringExtensions
    {
        public static string Simplify(this string inputString)
        {
            return inputString.Replace("_", string.Empty).Replace(" ", string.Empty).ToUpper();
        }

        public static string ToFirstLetterUppercase(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public static string RemoveRedundantWhiteSpaces(this string input)
        {
            if (input == null)
                throw new Exception("Can't remove redundant white spaces for null.");

            var output = new StringBuilder();

            char? previousChar = null;
            int charIndex = -1;

            foreach (var @char in input)
            {
                charIndex++;

                if (char.IsWhiteSpace(@char))
                {
                    if (!previousChar.HasValue)
                        continue;

                    if (char.IsWhiteSpace(previousChar.Value))
                        continue;

                    if (charIndex + 1 == input.Length)
                        continue;

                    if (char.IsWhiteSpace(input[charIndex + 1]))
                        continue;
                }

                output.Append(@char);
                previousChar = @char;
            }

            var outputString = output.ToString();

            return outputString;
        }

        public static bool ContainsUnclosedQuote(this string input)
        {
            var quoteIsOpen = false;

            foreach (var @char in input)
            {
                if (@char == '"')
                    quoteIsOpen = !quoteIsOpen;
            }

            return quoteIsOpen;
        }

        public static string[] SplitIfNotInQuotes(this string input, char separator)
        {
            var quoteIsOpen = false;
            var stringParts = new Collection<string>();
            var currentStringPart = new StringBuilder();
            
            foreach (var @char in input)
            {
                if (@char == '"')
                {
                    quoteIsOpen = !quoteIsOpen;
                }
                else if (@char == separator && !quoteIsOpen)
                {
                    stringParts.Add(currentStringPart.ToString());
                    currentStringPart.Clear();
                }
                else
                {
                    currentStringPart.Append(@char);
                }
            }

            return stringParts.ToArray();
        }

        public static string ReplaceUrlIncompatibleSymbols(this string input, char? replacement = null)
        {
            var stringBuilder = new StringBuilder();

            char? previousChar = null;
            int charIndex = -1;

            foreach (var @char in input)
            {
                charIndex++;

                if (!char.IsDigit(@char) && !char.IsLetter(@char))
                {
                    if (!previousChar.HasValue)
                        continue;

                    if (!char.IsDigit(previousChar.Value) && !char.IsLetter(previousChar.Value))
                        continue;

                    if (charIndex + 1 == input.Length)
                        continue;

                    if (!char.IsDigit(input[charIndex + 1]) && !char.IsLetter(input[charIndex + 1]))
                        continue;

                    if (replacement != null)
                        stringBuilder.Append(replacement.Value);
                }
                else
                {
                    stringBuilder.Append(@char);
                }

                previousChar = @char;

            }

            return stringBuilder.ToString();
        }
    }
}
