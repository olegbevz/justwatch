﻿using System.IO;

namespace JustWatch.Common
{
    public static class PathUtils
    {
        public static string CombineWithCurrentDirectory(params string[] paths)
        {
            var currentPath = Path.Combine(paths); 

            var currentDirectory = Directory.GetCurrentDirectory();

            return Path.Combine(currentDirectory, currentPath);
        }
    }
}
