﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JustWatch.Common
{
    public static class HttpUrl
    {
        public static string Concat(
            string baseUrl, 
            string requestUrl, 
            IEnumerable<KeyValuePair<string, string>> queryStringParameters = null)
        {
            var urlBuilder = new StringBuilder();

            urlBuilder.Append(baseUrl);
            
            if (!baseUrl.EndsWith("/") && !requestUrl.StartsWith("/"))
                urlBuilder.Append("/");

            if (baseUrl.EndsWith("/") && requestUrl.StartsWith("/"))
                requestUrl = requestUrl.Substring(0, requestUrl.Length - 1);

            urlBuilder.Append(requestUrl);

            if (queryStringParameters != null && queryStringParameters.Any())
            {
                urlBuilder.Append("?");
                urlBuilder.Append(string.Join("&", queryStringParameters.Select(x => string.Format("{0}={1}", x.Key, x.Value))));
            }

            return urlBuilder.ToString();
        }
    }
}
