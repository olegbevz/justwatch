﻿using System.Text;

namespace JustWatch.Common
{
    public static class Encodings
    {
        static Encodings()
        {
            RussianEncoding = Encoding.GetEncoding(1251);
        }

        public static Encoding RussianEncoding { get; private set; } 
    }
}
