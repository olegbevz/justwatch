﻿using System.Text;

namespace JustWatch.Common
{
    public static class ByteUtils
    {
        public static string BufferToHexString(byte[] buffer)
        {
            var builder = new StringBuilder();
            for (int i = 0; i < buffer.Length; i++)
                builder.Append(buffer[i].ToString("x2")); /* hex format */
            return builder.ToString();
        }
    }
}
