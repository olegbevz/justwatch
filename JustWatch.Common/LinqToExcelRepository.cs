﻿using System.Collections.Generic;
using LinqToExcel;
using LinqToExcel.Query;

namespace JustWatch.Common
{
    public abstract class LinqToExcelRepository<T>
    {
        protected readonly ExcelQueryFactory excelQueryFactory;
        private readonly int worksheetIndex = 0;
        private readonly string worksheetName;

        protected LinqToExcelRepository(string fileName, int worksheetIndex) : this(fileName)
        {
            this.worksheetIndex = worksheetIndex;
        }

        protected LinqToExcelRepository(string fileName, string worksheetName) : this(fileName)
        {
            this.worksheetName = worksheetName;
        }

        protected LinqToExcelRepository(string fileName)
        {
            excelQueryFactory = new ExcelQueryFactory(fileName);
            excelQueryFactory.ReadOnly = true;
            excelQueryFactory.StrictMapping = StrictMappingType.ClassStrict;
            excelQueryFactory.TrimSpaces = TrimSpacesType.Both;
        }

        public IEnumerable<T> GetAll()
        {
            if (worksheetName != null)
                return excelQueryFactory.Worksheet<T>(worksheetName);

            return excelQueryFactory.Worksheet<T>(worksheetIndex);
        }
    }
}
