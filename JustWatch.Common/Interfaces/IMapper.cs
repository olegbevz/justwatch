﻿
namespace JustWatch.Common
{
    public interface IMapper<TTargetType, TSourceType>
    {
        TTargetType MapForward(TSourceType source);
        TSourceType MapBack(TTargetType target);
    }
}
