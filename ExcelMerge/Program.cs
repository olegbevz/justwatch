﻿using JustWatch.ExcelMerge.Console;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;

namespace JustWatch.ExcelMerge
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var parametersParser = new ConsoleParametersParser();
                var parameters = parametersParser.Parse(args);

                var currentDirectory = Directory.GetCurrentDirectory();
                var firstExcelFile = Path.Combine(currentDirectory, parameters.FirstExcel);
                var secondExcelFile = Path.Combine(currentDirectory, parameters.SecondExcel);

                var valuesToMerge = GetValuesToMerge(firstExcelFile, parameters.FirstExcelJoinColumn, parameters.FirstExcelMergeColumn);

                MergeValues(secondExcelFile, parameters.SecondExcelJoinColumn, parameters.SecondExcelMergeColumn, valuesToMerge);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private static Dictionary<string, string> GetValuesToMerge(string excelFilePath, string joinColumn, string mergeColumn)
        {
            using (var fileStream = File.OpenRead(excelFilePath))
            {
                using (var excelPackage = new ExcelPackage(fileStream))
                {
                    if (excelPackage.Workbook.Worksheets.Count == 0)
                        throw new Exception("There are no worksheets in excel document.");

                    var worksheet = excelPackage.Workbook.Worksheets[1];

                    var values = new Dictionary<string, string>();

                    System.Console.WriteLine(string.Format("Считывание значений ячеек в документе {0}", Path.GetFileName(excelFilePath)));

                    var rowCount = GetWorksheetRowsCount(worksheet);

                    for (int rowIndex = 1; rowIndex <= rowCount; rowIndex++)
                    {
                        var joinValueCell = worksheet.Cells[joinColumn + rowIndex].Value;
                        if (joinValueCell == null) 
                            continue;

                        var joinValue = joinValueCell.ToString();
                        if (string.IsNullOrEmpty(joinValue))
                            continue;

                        var mergeValueCell = worksheet.Cells[mergeColumn + rowIndex].Value;
                        if (mergeValueCell == null)
                            continue;

                        var mergeValue = mergeValueCell.ToString();
                        if (string.IsNullOrEmpty(mergeValue))
                            continue;

                        if (!values.ContainsKey(joinValue))
                            values.Add(joinValue, mergeValue);
                    }

                    return values;
                }
            }
        }

        private static void MergeValues(string excelFilePath, string joinColumn, string mergeColumn, Dictionary<string, string> values)
        {
            using (var excelPackage = new ExcelPackage(new FileInfo(excelFilePath)))
            {
                if (excelPackage.Workbook.Worksheets.Count == 0)
                    throw new Exception("There are no worksheets in excel document.");

                var worksheet = excelPackage.Workbook.Worksheets[1];

                System.Console.WriteLine(string.Format("Обновление значений ячеек в документе {0}", Path.GetFileName(excelFilePath)));

                var rowCount = GetWorksheetRowsCount(worksheet);
                var changedCellsCount = 0;

                for (int rowIndex = 1; rowIndex <= rowCount; rowIndex++)
                {
                    var joinValueCell = worksheet.Cells[joinColumn + rowIndex].Value;
                    if (joinValueCell == null)
                        continue;

                    var joinValue = joinValueCell.ToString();
                    if (string.IsNullOrEmpty(joinValue))
                        continue;

                    string currentValue = null;
                    if (!values.TryGetValue(joinValue, out currentValue))
                        continue;

                    worksheet.Cells[mergeColumn + rowIndex].Value = currentValue;

                    changedCellsCount++;
                }

                System.Console.WriteLine(string.Format("Обновлено {0} ячеек из {1}", changedCellsCount, rowCount));
                System.Console.WriteLine(string.Format("Сохранение документа {0}", Path.GetFileName(excelFilePath)));

                excelPackage.Save();
            }
        }

        private static int GetWorksheetRowsCount(ExcelWorksheet worksheet)
        {
            var cellRange = worksheet.Cells.Value as object[,];
            if (cellRange == null)
                throw new Exception("Can't get rows count");

            return cellRange.GetLength(0);
        }
    }
}
