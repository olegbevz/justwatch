﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JustWatch.ExcelMerge.Console
{
    public class ConsoleParameters
    {
        public string FirstExcel { get; set; }
        public string FirstExcelJoinColumn { get; set; }
        public string FirstExcelMergeColumn { get; set; }

        public string SecondExcel { get; set; }
        public string SecondExcelJoinColumn { get; set; }
        public string SecondExcelMergeColumn { get; set; }
    }
}
