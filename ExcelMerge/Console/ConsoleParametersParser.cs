﻿using System;

namespace JustWatch.ExcelMerge.Console
{
    public class ConsoleParametersParser
    {
        private const string JoinColumnPrefix = "-jc:";
        private const string MergeColumnPrefix = "-mc:";

        public ConsoleParameters Parse(string[] args)
        {
            var parameters = new ConsoleParameters();

            if (args.Length != 6)
                throw new Exception(
@"Following parameters should be passed to console: 
ExcelMerge.exe file1.xlsx file2.xlsx -jc:B -mc:C -jc:E -mc:F");

            parameters.FirstExcel = args[0];
            parameters.SecondExcel = args[1];

            for (int i = 2; i < 6; i++)
            {
                var argument = args[i];

                if (argument.StartsWith(JoinColumnPrefix))
                {
                    argument = argument.Replace(JoinColumnPrefix, string.Empty);
                    if (parameters.FirstExcelJoinColumn == null)
                        parameters.FirstExcelJoinColumn = argument;
                    else
                        parameters.SecondExcelJoinColumn = argument;
                }
                else if (argument.StartsWith(MergeColumnPrefix))
                {
                    argument = argument.Replace(MergeColumnPrefix, string.Empty);
                    if (parameters.FirstExcelMergeColumn == null)
                        parameters.FirstExcelMergeColumn = argument;
                    else
                        parameters.SecondExcelMergeColumn = argument;
                }
            }
            
            return parameters;
        }
    }
}
