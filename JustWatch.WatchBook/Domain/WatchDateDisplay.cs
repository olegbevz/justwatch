﻿using System.Collections.ObjectModel;
using JustWatch.Common;
using System;

namespace JustWatch.WatchBook
{
    public class WatchDateDisplay
    {
        public bool DayOfWeek { get; set; }

        public bool DayOfMonth { get; set; }
        
        public bool Month { get; set; }

        public bool Year { get; set; }

        public static WatchDateDisplay Parse(string input)
        {
            var dateDisplay = new WatchDateDisplay();

            foreach (var parts in input.Split(','))
            {
                switch (parts.RemoveRedundantWhiteSpaces().ToLower())
                {
                    case "число":
                        dateDisplay.DayOfMonth = true;
                        break;
                    case "день недели":
                        dateDisplay.DayOfWeek = true;
                        break;
                    case "месяц":
                        dateDisplay.Month = true;
                        break;
                    case "год":
                        dateDisplay.Year = true;
                        break;
                    case "отсутствует":
                        break;
                    default:
                        throw new Exception(string.Format("Не удалось определить отображение даты: '{0}'", input));
                }
            }

            return dateDisplay;
        }

        public override string ToString()
        {
            var dateDisplay = new Collection<string>();

            if (DayOfMonth)
                dateDisplay.Add("Число");

            if (DayOfWeek)
                dateDisplay.Add("день недели");

            if (Month)
                dateDisplay.Add("месяц");

            if (Year)
                dateDisplay.Add("год");

            return string.Join(", ", dateDisplay);
        }
    }
}
