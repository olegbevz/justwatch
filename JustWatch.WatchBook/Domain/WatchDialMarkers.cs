﻿namespace JustWatch.WatchBook
{
    public enum WatchDialMarkers
    {
        Roman,
        Arabian,
        RomanArabian,
        Absent
    }
}
