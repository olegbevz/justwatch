﻿namespace JustWatch.WatchBook
{
    public enum WatchMovement
    {
        MechanicalAutomaticWindUp,
        MechanicalManualWindUp,
        Quartz
    }
}
