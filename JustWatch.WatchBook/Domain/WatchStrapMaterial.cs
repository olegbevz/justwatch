﻿namespace JustWatch.WatchBook
{
    public enum WatchStrapMaterial
    {
        /// <summary>
        /// Пластик
        /// </summary>
        Plastic,

        /// <summary>
        /// Нержавеющая сталь
        /// </summary>
        StainlessSteel,

        /// <summary>
        /// Полиуретан
        /// </summary>
        Polyurethane,

        /// <summary>
        /// Титан
        /// </summary>
        Titanium,

        /// <summary>
        /// Полимер
        /// </summary>
        Polymer,

        /// <summary>
        /// Алюминий
        /// </summary>
        Aluminum,

        /// <summary>
        /// Латунь
        /// </summary>
        Brass,

        /// <summary>
        /// Керамика
        /// </summary>
        Ceramics,

        /// <summary>
        /// Медь
        /// </summary>
        Copper,

        /// <summary>
        /// Композит
        /// </summary>
        Composite,

        /// <summary>
        /// Кожа
        /// </summary>
        Leather,

        /// <summary>
        /// Резина
        /// </summary>
        Rubber,

        /// <summary>
        /// Каучук
        /// </summary>
        Caoutchouc,

        /// <summary>
        /// Силикон
        /// </summary>
        Silicone,

        /// <summary>
        /// Текстиль
        /// </summary>
        Textile,

        /// <summary>
        /// Титан и керамика
        /// </summary>
        TitaniumCeramics,

        /// <summary>
        /// Тканевый
        /// </summary>
        Tissue
    }
}