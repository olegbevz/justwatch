﻿using System;

namespace JustWatch.WatchBook
{
    public class Watch
    {
        /// <summary>
        /// Артикуль товара
        /// </summary>
        public string Article { get; set; }

        /// <summary>
        /// Наименование часов
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Модель часов
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Бренд часов
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// Пол часов
        /// </summary>
        public WatchGender? Gender { get; set; }

        /// <summary>
        /// Массив url для картинок
        /// </summary>
        public string[] Pictures { get; set; }
        
        /// <summary>
        /// Url картики, который вставляется в описание часов
        /// </summary>
        public string DescriptionPicture { get; set; }

        /// <summary>
        /// Часовой механизм
        /// </summary>
        public WatchMovement? Movement { get; set; }

        /// <summary>
        /// Тип цифр
        /// </summary>
        public string DialMarkers { get; set; }

        /// <summary>
        /// Водонепроницаемые часы
        /// </summary>
        public bool? Watertight { get; set; } 

        /// <summary>
        /// Водонепроницаемость часов, атм или WT*
        /// </summary>
        public string WaterResistance { get; set; }

        /// <summary>
        /// Материал корпуса
        /// </summary>
        public WatchCaseMaterial? CaseMaterial { get; set; }

        /// <summary>
        /// Материал ремешка
        /// </summary>
        public WatchStrapMaterial? StrapMaterial { get; set; }

        /// <summary>
        /// Остекление
        /// </summary>
        public WatchCrystal? Crystal { get; set; }

        /// <summary>
        /// Тип дисплея
        /// </summary>
        public WatchDialType? DialType { get; set; }

        /// <summary>
        /// Источник энергии
        /// </summary>
        public string EnergySource { get; set; }

        /// <summary>
        /// Способ отображения времени
        /// </summary>
        public WatchDateDisplay DateDisplay { get; set; }

        /// <summary>
        /// Дополнительные функции
        /// </summary>
        public string AdditionalFunctions { get; set; }

        /// <summary>
        /// Гарантия товара
        /// </summary>
        public string Guarantee { get; set; }

        /// <summary>
        /// Размеры часов
        /// </summary>
        public WatchSize Size { get; set; }

        /// <summary>
        /// Вес часов
        /// </summary>
        public double? Weight { get; set; }

        /// <summary>
        /// Серия часов
        /// </summary>
        public string Series { get; set; }

        /// <summary>
        /// Страна производителя
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Цена часов, руб.
        /// </summary>
        public double Price { get; set; }

        public Watch Merge(Watch otherWatch)
        { 
            return new Watch
            {
                Article = string.IsNullOrEmpty(Article) ? otherWatch.Article : Article,
                Model = string.IsNullOrEmpty(Model) ? otherWatch.Model : Model,
                Name = string.IsNullOrEmpty(Name) ? otherWatch.Name : Name,
                Brand = string.IsNullOrEmpty(Brand) ? otherWatch.Brand : Brand,
                Gender = Gender ?? otherWatch.Gender,
                Pictures = (Pictures == null || Pictures.Length == 0) ? otherWatch.Pictures : Pictures,
                Movement = Movement ?? otherWatch.Movement,
                DialMarkers = string.IsNullOrEmpty(DialMarkers) ? otherWatch.DialMarkers : DialMarkers,
                Watertight = Watertight ?? otherWatch.Watertight,
                WaterResistance = string.IsNullOrEmpty(WaterResistance) ? otherWatch.WaterResistance : WaterResistance,
                CaseMaterial = CaseMaterial ?? otherWatch.CaseMaterial,
                StrapMaterial = StrapMaterial ?? otherWatch.StrapMaterial,
                Crystal = Crystal ?? otherWatch.Crystal,
                DialType = DialType ?? otherWatch.DialType,
                EnergySource = string.IsNullOrEmpty(EnergySource) ? otherWatch.EnergySource : EnergySource,
                DateDisplay = DateDisplay ?? otherWatch.DateDisplay,
                AdditionalFunctions = string.IsNullOrEmpty(AdditionalFunctions) ? otherWatch.AdditionalFunctions : AdditionalFunctions,
                Guarantee = string.IsNullOrEmpty(Guarantee) ? otherWatch.Guarantee : Guarantee,
                Size = Size ?? otherWatch.Size,
                Weight = Weight ?? otherWatch.Weight,
                Price = Price > 0.0 ? Price : otherWatch.Price
            };
        }    

        public static string FormatCaseMaterial(WatchCaseMaterial caseMaterial)
        {
            switch (caseMaterial)
            {
                case WatchCaseMaterial.Plastic:
                    return "Пластик";
                case WatchCaseMaterial.StainlessSteel:
                    return "Нержавеющая сталь";
                case WatchCaseMaterial.Polyurethane:
                    return "Полиуретан";
                case WatchCaseMaterial.Titanium:
                    return "Титан";
                case WatchCaseMaterial.Polymer:
                    return "Полимер";
                case WatchCaseMaterial.Aluminum:
                    return "Алюминий";
                case WatchCaseMaterial.Brass:
                    return "Латунь";
                case WatchCaseMaterial.Ceramics:
                    return "Керамика";
                case WatchCaseMaterial.Copper:
                    return "Медь";
                case WatchCaseMaterial.Composite:
                    return "Композит";
                case WatchCaseMaterial.Gold:
                    return "Золото";
                default:
                    throw new NotImplementedException();
            }
        }

        public static string FormatStrapMaterial(WatchStrapMaterial strapMaterial)
        {
            switch (strapMaterial)
            {
                case WatchStrapMaterial.Plastic:
                    return "Пластик";
                case WatchStrapMaterial.StainlessSteel:
                    return "Нержавеющая сталь";
                case WatchStrapMaterial.Polyurethane:
                    return "Полиуретан";
                case WatchStrapMaterial.Titanium:
                    return "Титан";
                case WatchStrapMaterial.Polymer:
                    return "Полимер";
                case WatchStrapMaterial.Aluminum:
                    return "Алюминий";
                case WatchStrapMaterial.Brass:
                    return "Латунь";
                case WatchStrapMaterial.Ceramics:
                    return "Керамика";
                case WatchStrapMaterial.Copper:
                    return "Медь";
                case WatchStrapMaterial.Composite:
                    return "Композит";
                case WatchStrapMaterial.Leather:
                    return "Искусственная кожа";
                case WatchStrapMaterial.Rubber:
                    return "Силикон/резина";
                case WatchStrapMaterial.Caoutchouc:
                    return "Каучук";
                case WatchStrapMaterial.Silicone:
                    return "Силикон";
                case WatchStrapMaterial.Textile:
                    return "Текстиль";
                case WatchStrapMaterial.TitaniumCeramics:
                    return "Титан/керамика";
                case WatchStrapMaterial.Tissue:
                    return "Ткань";
                default:
                    throw new NotImplementedException();
            }
        }

        public static string FormatWatchGender(WatchGender watchGender)
        {
            switch (watchGender)
            {
                case WatchGender.Male:
                    return "Для мужчин";
                case WatchGender.Female:
                    return "Для женщин";
                case WatchGender.Unisex:
                    return "Унисекс";
                case WatchGender.Child:
                    return "Детские";
                default:
                    throw new NotSupportedException("Unknown watch sex.");
            }
        }

        public static string FormatWatchDialType(WatchDialType watchDialType)
        {
            switch (watchDialType)
            {
                case WatchDialType.Analogue:
                    return "Аналоговый";
                case WatchDialType.Digital:
                    return "Цифровой";
                case WatchDialType.DigitalAndAnalogue:
                    return "Аналоговый и цифровой";
                default:
                    throw new NotImplementedException();
            }
        }

        public static string FormatWatchMovement(WatchMovement watchMovement)
        {
            switch (watchMovement)
            {
                case WatchMovement.Quartz:
                    return "Кварцевые (аккумулятор)";
                case WatchMovement.MechanicalAutomaticWindUp:
                    return "Механические (автоматические)";
                case WatchMovement.MechanicalManualWindUp:
                    return "Механические (ручной завод)";
                default:
                    throw new NotImplementedException();
            }
        }

        public static string FormatWatchCrystal(WatchCrystal crystal)
        {
            switch (crystal)
            {
                case WatchCrystal.Plastic:
                    return "Пластиковое";
                case WatchCrystal.Mineral:
                    return "Минеральное";
                case WatchCrystal.Sapphire:
                    return "Сапфировое";
                case WatchCrystal.MineralSapphire:
                    return "Минерал/Сапфир";
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
