﻿namespace JustWatch.WatchBook
{
    public enum WatchDialType
    {
        Digital,
        Analogue,
        DigitalAndAnalogue
    }
}
