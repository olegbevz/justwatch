﻿namespace JustWatch.WatchBook
{
    public enum WatchGender
    {
        Male,
        Female,
        Unisex,
        Child
    }
}
