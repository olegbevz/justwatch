﻿namespace JustWatch.WatchBook
{
    /// <summary>
    /// Материал которбки часов
    /// </summary>
    public enum WatchCaseMaterial
    {
        /// <summary>
        /// Пластик
        /// </summary>
        Plastic,

        /// <summary>
        /// Нержавеющая сталь
        /// </summary>
        StainlessSteel,

        /// <summary>
        /// Полиуретан
        /// </summary>
        Polyurethane,

        /// <summary>
        /// Титан
        /// </summary>
        Titanium,

        /// <summary>
        /// Полимер
        /// </summary>
        Polymer,

        /// <summary>
        /// Алюминий
        /// </summary>
        Aluminum,

        /// <summary>
        /// Латунь
        /// </summary>
        Brass,

        /// <summary>
        /// Керамика
        /// </summary>
        Ceramics,

        /// <summary>
        /// Медь
        /// </summary>
        Copper,

        /// <summary>
        /// Композит
        /// </summary>
        Composite,

        /// <summary>
        /// Золото
        /// </summary>
        Gold
    }
}
