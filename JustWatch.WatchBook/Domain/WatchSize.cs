﻿using System.Globalization;
using System.Linq;

namespace JustWatch.WatchBook
{
    public class WatchSize
    {
        public WatchSize()
        {
        }

        public WatchSize(double? diameter, double? height, double? depth)
        {
            Diameter = diameter;
            Height = height;
            Depth = depth;
        }

        /// <summary>
        /// Диаметр часов, мм
        /// </summary>
        public double? Diameter { get; set; }

        /// <summary>
        /// Расстояние от ушек сверху до ушек снизу, мм
        /// </summary>
        public double? Height { get; set; }

        /// <summary>
        /// Толщина часов, мм
        /// </summary>
        public double? Depth { get; set; }

        public override string ToString()
        {
            var sizeParts = new double?[] { Diameter, Height, Depth };

            return string.Join("x", sizeParts
                    .Where(x => x.HasValue)
                    .Select(x => x.Value)
                    .Select(x => x.ToString(CultureInfo.InvariantCulture)));
        }
    }
}
