﻿namespace JustWatch.WatchBook
{
    /// <summary>
    /// Материал стекла
    /// </summary>
    public enum WatchCrystal
    {
        /// <summary>
        /// Пластик
        /// </summary>
        Plastic,

        /// <summary>
        /// Минерал
        /// </summary>
        Mineral,

        /// <summary>
        /// Сапфир
        /// </summary>
        Sapphire,

        /// <summary>
        /// Минерал сапфир
        /// </summary>
        MineralSapphire
    }
}
