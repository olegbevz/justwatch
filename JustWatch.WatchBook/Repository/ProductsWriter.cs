﻿using OfficeOpenXml;
using System;
using System.IO;

namespace JustWatch.WatchBook
{
    internal class ProductsWriter : IDisposable
    {
        private readonly ExcelPackage excelPackage;
        private readonly ExcelWorksheet worksheet;

        private int currentLine = 0;

        public ProductsWriter(string fileName)
        {
            excelPackage = new ExcelPackage(new FileInfo(fileName));

            if (excelPackage.Workbook.Worksheets.Count == 0)
                throw new Exception("There are no worksheets in excel document.");

            worksheet = excelPackage.Workbook.Worksheets[1];

            var cellRange = worksheet.Cells.Value as object[,];
            if (cellRange == null)
                throw new Exception("Can't get rows count");

            currentLine = cellRange.GetLength(0) + 1;
        }

        public void WriteProduct(ProductRecord productRecord)
        {
            worksheet.Cells["A" + currentLine].Value = productRecord.Article;
            worksheet.Cells["B" + currentLine].Value = productRecord.Model;
            worksheet.Cells["D" + currentLine].Value = productRecord.Brand;
            worksheet.Cells["E" + currentLine].Value = productRecord.Sex;
            worksheet.Cells["F" + currentLine].Value = productRecord.MechanismType;
            worksheet.Cells["G" + currentLine].Value = productRecord.Digits;
            worksheet.Cells["H" + currentLine].Value = productRecord.Watertight;
            worksheet.Cells["I" + currentLine].Value = productRecord.WR;
            worksheet.Cells["J" + currentLine].Value = productRecord.CaseMaterial;
            worksheet.Cells["K" + currentLine].Value = productRecord.StrapMaterial;
            worksheet.Cells["L" + currentLine].Value = productRecord.Glass;
            worksheet.Cells["M" + currentLine].Value = productRecord.Display;
            worksheet.Cells["N" + currentLine].Value = productRecord.EnergySource;
            worksheet.Cells["O" + currentLine].Value = productRecord.Size;
            worksheet.Cells["P" + currentLine].Value = productRecord.Weight;
            worksheet.Cells["Q" + currentLine].Value = productRecord.DateDisplay;
            worksheet.Cells["R" + currentLine].Value = productRecord.Pictures;
            worksheet.Cells["S" + currentLine].Value = productRecord.Name;
            worksheet.Cells["U" + currentLine].Value = productRecord.AdditionalFunctions;
            worksheet.Cells["W" + currentLine].Value = productRecord.Warranty;

            currentLine++;
        }

        public void Flush()
        {
            excelPackage.Save();
        }

        public void Dispose()
        {
            excelPackage.Dispose();
        }
    }
}
