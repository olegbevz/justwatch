﻿using System;
using System.Linq;
using System.Globalization;
using JustWatch.Common;

namespace JustWatch.WatchBook
{
    internal class WatchMapper : IMapper<Watch, ProductRecord>
    {
        public Watch MapForward(ProductRecord productRecord)
        {
            var watch = new Watch
            {
                Article = productRecord.Article == null ? null : productRecord.Article.RemoveRedundantWhiteSpaces(),
                Name = productRecord.Name == null ? null : productRecord.Name.RemoveRedundantWhiteSpaces(),
                Model = productRecord.Model == null ? null : productRecord.Model.Replace(productRecord.Brand, string.Empty).RemoveRedundantWhiteSpaces(),
                Pictures = string.IsNullOrEmpty(productRecord.Pictures) ? new string[0] : productRecord.Pictures.Split(';'),
                Brand = productRecord.Brand == null ? null : productRecord.Brand.RemoveRedundantWhiteSpaces(),
                AdditionalFunctions = productRecord.AdditionalFunctions,
                Gender = string.IsNullOrEmpty(productRecord.Sex) ? new WatchGender?() : ParseWatchGender(productRecord.Sex.RemoveRedundantWhiteSpaces()),
                DialMarkers = productRecord.Digits,
                WaterResistance = productRecord.WR,
                StrapMaterial = string.IsNullOrEmpty(productRecord.StrapMaterial) || productRecord.StrapMaterial.Contains("не указан") ? 
                    new WatchStrapMaterial?() : 
                    ParseStrapMaterial(productRecord.StrapMaterial.RemoveRedundantWhiteSpaces()),
                CaseMaterial = string.IsNullOrEmpty(productRecord.CaseMaterial) || string.IsNullOrEmpty(productRecord.CaseMaterial.RemoveRedundantWhiteSpaces()) ?
                    new WatchCaseMaterial?() : 
                    ParseCaseMaterial(productRecord.CaseMaterial.ToFirstLetterUppercase().RemoveRedundantWhiteSpaces()),
                Crystal = string.IsNullOrEmpty(productRecord.Glass) ? new WatchCrystal?() : ParseWatchCrystal(productRecord.Glass.RemoveRedundantWhiteSpaces()),
                DialType = string.IsNullOrEmpty(productRecord.Display) ? new WatchDialType?() : ParseWatchDialType(productRecord.Display.RemoveRedundantWhiteSpaces()),
                EnergySource = productRecord.EnergySource,
                DateDisplay = productRecord.DateDisplay != null ? WatchDateDisplay.Parse(productRecord.DateDisplay.RemoveRedundantWhiteSpaces()) : null,
                Guarantee = productRecord.Warranty,
                Series = productRecord.Series == null ? null : productRecord.Series.RemoveRedundantWhiteSpaces(),
                DescriptionPicture = productRecord.DescriptionPicture,
                Country = productRecord.Country
            };

            if (!string.IsNullOrEmpty(productRecord.Weight))
            {
                double weight;
                if (double.TryParse(productRecord.Weight.RemoveRedundantWhiteSpaces(), NumberStyles.Float, CultureInfo.InvariantCulture, out weight))
                    watch.Weight = weight;
                else
                    throw new Exception(string.Format("Не удалось распознать вес часов: \"{0}\".", productRecord.Weight));
            }

            if (!string.IsNullOrEmpty(productRecord.Watertight))
            {
                if (productRecord.Watertight == "Есть" || productRecord.Watertight == "есть")
                    watch.Watertight = true;
                else
                    throw new Exception(string.Format("Не удалось определить являются ли часы водонепроницаемыми: \"{0}\".", productRecord.Watertight));
            }
            
            if (!string.IsNullOrEmpty(productRecord.Size))
                watch.Size = ParseSize(productRecord.Size.RemoveRedundantWhiteSpaces());

            if (!string.IsNullOrEmpty(productRecord.MechanismType))
                watch.Movement = ParseWatchMovement(productRecord.MechanismType.RemoveRedundantWhiteSpaces());

            return watch;
        }     

        public ProductRecord MapBack(Watch target)
        {
            return new ProductRecord
            {
                Article = target.Article,
                Name = target.Name,
                Model = target.Model,
                Pictures = string.Join(";", target.Pictures),
                Brand = target.Brand,
                AdditionalFunctions = target.AdditionalFunctions,
                Sex = target.Gender.HasValue ? Watch.FormatWatchGender(target.Gender.Value) : string.Empty,
                Digits = target.DialMarkers,
                WR = target.WaterResistance,
                StrapMaterial =  target.StrapMaterial.HasValue ? Watch.FormatStrapMaterial(target.StrapMaterial.Value) : string.Empty,
                CaseMaterial = target.CaseMaterial.HasValue ? Watch.FormatCaseMaterial(target.CaseMaterial.Value) : string.Empty,
                Glass = target.Crystal.HasValue ? Watch.FormatWatchCrystal(target.Crystal.Value) : string.Empty,
                Display = target.DialType.HasValue ? Watch.FormatWatchDialType(target.DialType.Value) : string.Empty,
                EnergySource = target.EnergySource,
                DateDisplay = target.DateDisplay != null ? target.DateDisplay.ToString() : string.Empty,
                Warranty = target.Guarantee,
                Weight = target.Weight.HasValue ? target.Weight.Value.ToString(CultureInfo.InvariantCulture) : string.Empty,
                Watertight = target.Watertight.HasValue && target.Watertight.Value ? "Есть" : string.Empty,
                MechanismType = target.Movement.HasValue ? Watch.FormatWatchMovement(target.Movement.Value) : string.Empty,
                Size = target.Size != null ? target.Size.ToString() : string.Empty,
                Country = target.Country
            };
        }

        public WatchSize ParseSize(string size)
        {
            var sizeParts = size.Split('x', 'х');
            var sizeValues = new double?[3];

            for (int i = 0; i < sizeParts.Length; i++)
            {
                double sizeValue;
                if (double.TryParse(sizeParts[i], NumberStyles.Float, CultureInfo.InvariantCulture, out sizeValue))
                    sizeValues[i] = sizeValue;
                else
                    throw new Exception(string.Format("Не удалось распознать размер часов: \"{0}\".", size));
            }

            return new WatchSize(sizeValues[0], sizeValues[1], sizeValues[2]);
        }

        private WatchGender ParseWatchGender(string watchGender)
        {
            switch (watchGender)
            {
                case "Для мужчин":
                    return WatchGender.Male;
                case "Для женщин":
                    return WatchGender.Female;
                case "Унисекс":
                    return WatchGender.Unisex;
                case "Детские":
                    return WatchGender.Child;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить пол для часов: \"{0}\".", watchGender));
            }
        }

        private WatchMovement ParseWatchMovement(string watchMovement)
        {
            switch (watchMovement)
            {
                case "Электронные":
                case "Кварцевые (аккумулятор)":
                    return WatchMovement.Quartz;
                case "Механические (автоматические)":
                case "Механика с автоподзаводом":
                case "Механика":
                    return WatchMovement.MechanicalAutomaticWindUp; 
                case "Механические (ручной завод)":
                    return WatchMovement.MechanicalManualWindUp;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить тип механизма часов: \"{0}\".", watchMovement));
            }
        }

        private WatchDialType ParseWatchDialType(string watchDialType)
        {
            switch (watchDialType)
            {
                case "Аналоговый":
                    return WatchDialType.Analogue;
                case "Цифровой":
                    return WatchDialType.Digital;
                case "Аналоговый и цифровой":
                    return WatchDialType.DigitalAndAnalogue;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить тип дисплея: \"{0}\".", watchDialType));
            }
        }

        private WatchCaseMaterial ParseCaseMaterial(string caseMaterial)
        {
            switch (caseMaterial)
            {
                case "Пластик":
                    return WatchCaseMaterial.Plastic;
                case "Нержавеющая сталь":
                    return WatchCaseMaterial.StainlessSteel;
                case "Нерж. cталь/керамика":
                    return WatchCaseMaterial.Ceramics;
                case "Полиуретан":
                    return WatchCaseMaterial.Polyurethane;
                case "Титан":
                    return WatchCaseMaterial.Titanium;
                case "Полимер":
                    return WatchCaseMaterial.Polymer;
                case "Алюминий":
                    return WatchCaseMaterial.Aluminum;
                case "Латунь":
                    return WatchCaseMaterial.Brass;
                case "Керамика":
                    return WatchCaseMaterial.Ceramics;
                case "Медь":
                    return WatchCaseMaterial.Copper;
                case "Композит":
                    return WatchCaseMaterial.Composite;
                case "Золото":
                    return WatchCaseMaterial.Gold;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал корпуса: '{0}'.", caseMaterial));
            }
        }

        private WatchStrapMaterial ParseStrapMaterial(string strapMaterial)
        {
            switch (strapMaterial.ToFirstLetterUppercase())
            {
                case "Пластик":
                    return WatchStrapMaterial.Plastic;
                case "Нержавеющая сталь":
                    return WatchStrapMaterial.StainlessSteel;
                case "Нерж. cталь/керамика":
                    return WatchStrapMaterial.Ceramics;
                case "Полиуретан":
                    return WatchStrapMaterial.Polyurethane;
                case "Титан":
                    return WatchStrapMaterial.Titanium;
                case "Полимер":
                    return WatchStrapMaterial.Polymer;
                case "Алюминий":
                    return WatchStrapMaterial.Aluminum;
                case "Латунь":
                    return WatchStrapMaterial.Brass;
                case "Керамика":
                    return WatchStrapMaterial.Ceramics;
                case "Медь":
                    return WatchStrapMaterial.Copper;
                case "Композит":
                    return WatchStrapMaterial.Composite;
                case "Силикон":
                    return WatchStrapMaterial.Silicone;
                case "Кожа":
                    return WatchStrapMaterial.Leather;
                case "Каучук":
                    return WatchStrapMaterial.Caoutchouc;
                case "Резина":
                    return WatchStrapMaterial.Rubber;
                case "Текстиль":
                    return WatchStrapMaterial.Textile;
                case "Тканевый":
                    return WatchStrapMaterial.Tissue;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал ремешка: '{0}'.", strapMaterial));
            }
        }

        private WatchCrystal ParseWatchCrystal(string glass)
        {
            switch (glass.ToLower())
            {
                case "пластик":
                case "пластиковое":
                    return WatchCrystal.Plastic;
                case "минеральное":
                case "минеральное стекло":
                    return WatchCrystal.Mineral;
                case "сапфировое":
                    return WatchCrystal.Sapphire;
                case "минеральное/сапфировое":
                    return WatchCrystal.MineralSapphire;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить материал стекла: '{0}'.", glass));
            }
        }
    }
}
