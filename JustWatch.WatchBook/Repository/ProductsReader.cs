﻿using Excel;
using System;
using System.Collections.Generic;
using System.IO;

namespace JustWatch.WatchBook
{
    internal class ProductsReader
    {
        private static readonly string NoDataMarker1 = "Нет данных";
        private static readonly string NoDataMarker2 = "#Н/Д";
        private static readonly string NoDataMarker3 = "#N/A";

        private readonly string fileName;

        public ProductsReader(string fileName)
        {
            this.fileName = fileName;
        }

        public IEnumerable<ProductRecord> ReadProductRecords()
        {
            using (var fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {  
                using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                {
                    if (!excelReader.Read())
                    {
                        throw new Exception("Products table is empty.");
                    }

                    while(excelReader.Read())
                    {
                        if (string.IsNullOrEmpty(excelReader.GetString(0)) &&
                            string.IsNullOrEmpty(excelReader.GetString(1)) &&
                            string.IsNullOrEmpty(excelReader.GetString(3)))
                            continue;

                        var productRecord = new ProductRecord
                        {
                            Article = ReadCellValue(excelReader, 0),
                            Model = ReadCellValue(excelReader, 1),
                            Brand = ReadCellValue(excelReader, 3),
                            Sex = ReadCellValue(excelReader, 4),
                            MechanismType = ReadCellValue(excelReader, 5),
                            Digits = ReadCellValue(excelReader, 6),
                            Watertight = ReadCellValue(excelReader, 7),
                            WR = ReadCellValue(excelReader, 8),
                            CaseMaterial = ReadCellValue(excelReader, 9),
                            StrapMaterial = ReadCellValue(excelReader, 10),
                            Glass = ReadCellValue(excelReader, 11),
                            Display = ReadCellValue(excelReader, 12),
                            EnergySource = ReadCellValue(excelReader, 13),
                            Size = ReadCellValue(excelReader, 14),
                            Weight = ReadCellValue(excelReader, 15),
                            DateDisplay = ReadCellValue(excelReader, 16),
                            Pictures = ReadCellValue(excelReader, 17),
                            Name = ReadCellValue(excelReader, 18),
                            AdditionalFunctions = ReadCellValue(excelReader, 20),
                            Warranty = ReadCellValue(excelReader, 22),
                            Series = ReadCellValue(excelReader, 24),
                            DescriptionPicture = ReadCellValue(excelReader, 27),
                            Country = ReadCellValue(excelReader, 29)
                        };

                        yield return productRecord;
                    }
                }
            }
        }

        private string ReadCellValue(IExcelDataReader excelReader, int column)
        {
            var value = excelReader.GetString(column);
            if (string.IsNullOrEmpty(value))
                return null;

            if (value == NoDataMarker1)
                return null;

            if (value == NoDataMarker2)
                return null;

            if (value == NoDataMarker3)
                return null;

            return value;
        }
    }
}
