﻿using System.Collections.Generic;
using System;

namespace JustWatch.WatchBook
{
    public class WatchRepository
    {
        private readonly string filePath;
        private readonly WatchMapper watchMapper = new WatchMapper();

        public WatchRepository(string filePath)
        {
            this.filePath = filePath;
        }

        public IEnumerable<Watch> GetWatches()
        {
            var productsDocumentReader = new ProductsReader(filePath);

            foreach (var productRecord in productsDocumentReader.ReadProductRecords())
            {
                Watch watch = null;

                try
                {
                    watch = watchMapper.MapForward(productRecord);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Не удалось считать запись о модели: {0}", productRecord.Model), ex);
                }

                yield return watch;
            }
        }

        public void AddWatches(IEnumerable<Watch> watches)
        {
            using (var productsWriter = new ProductsWriter(filePath))
            {
                foreach (var watch in watches)
                {
                    var productRecord = watchMapper.MapBack(watch);
                    productsWriter.WriteProduct(productRecord);
                }

                productsWriter.Flush();
            }
        }
    }
}
