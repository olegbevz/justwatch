namespace JustWatch.WatchBook
{
    internal class ProductRecord
    {
        public string Article { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public string Pictures { get; set; }
        public string Brand { get; set; }
        public string Sex { get; set; }
        public string MechanismType { get; set; }
        public string Digits { get; set; }
        public string Watertight { get; set; }
        public string WR { get; set; }
        public string CaseMaterial { get; set; }
        public string StrapMaterial { get; set; }
        public string Glass { get; set; }
        public string Display { get; set; }
        public string EnergySource { get; set; }
        public string Size { get; set; }
        public string Weight { get; set; }
        public string DateDisplay { get; set; }
        public string AdditionalFunctions { get; set; }	
        public string Warranty { get; set; }
        public string Series { get; internal set; }
        public string DescriptionPicture { get; set; }
        public string Country { get; set; }
    }
}