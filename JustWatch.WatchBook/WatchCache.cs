﻿using JustWatch.Common;
using System;
using System.Collections.Generic;

namespace JustWatch.WatchBook
{
    public class WatchCache : CacheBase<Watch>
    {
        public WatchCache(IEnumerable<Watch> watches) : base(watches)
        {
        }

        public Watch GetWatch(string brand, string model)
        {
            return this.GetByKey(CreateKey(brand, model));
        }

        private string CreateKey(string brand, string model)
        {
            if (string.IsNullOrEmpty(brand))
                throw new Exception("Не задано название бренда.");

            if (string.IsNullOrEmpty(model))
                throw new Exception("Не задано название модели часов.");

            return brand.Simplify() + model.Simplify();
        }

        protected override string CreateKey(Watch watch)
        {
            return CreateKey(watch.Brand, watch.Model);
        }

        protected override void OnDuplicateKeyCreated(string key, Watch watch)
        {
            throw new Exception(string.Format("Повторяющаяся модель часов: '{0} {1}'.", watch.Brand, watch.Model));
        }
    }
}
