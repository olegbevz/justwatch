﻿namespace JustWatch.Domain
{
    public enum WatchSex
    {
        Male,
        Female,
        Unisex,
        Child
    }
}
