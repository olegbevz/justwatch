﻿namespace JustWatch.Domain
{
    public class Watch
    {
        /// <summary>
        /// Артикуль товара
        /// </summary>
        public string Article { get; set; }

        /// <summary>
        /// Наименование часов
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Модель часов
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Бренд часов
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// Пол часов
        /// </summary>
        public WatchSex? Sex { get; set; }

        /// <summary>
        /// Массив url для картинок
        /// </summary>
        public string[] Pictures { get; set; }    
        
        /// <summary>
        /// Url картики, который вставляется в описание часов
        /// </summary>
        public string DescriptionPicture { get; set; }

        /// <summary>
        /// Часовой механизм
        /// </summary>
        public WatchMechanism? Mechanism { get; set; }

        /// <summary>
        /// Тип цифр
        /// </summary>
        public string Digits { get; set; }

        /// <summary>
        /// Водонепроницаемые часы
        /// </summary>
        public bool? Watertight { get; set; } 

        /// <summary>
        /// Водонепроницаемость часов, атм или WT*
        /// </summary>
        public string Waterproof { get; set; }

        /// <summary>
        /// Материал корпуса
        /// </summary>
        public string BoxMaterial { get; set; }

        /// <summary>
        /// Материал ремешка
        /// </summary>
        public string StrapMaterial { get; set; }

        /// <summary>
        /// Остекление
        /// </summary>
        public string Glass { get; set; }

        /// <summary>
        /// Тип дисплея
        /// </summary>
        public string Display { get; set; }

        /// <summary>
        /// Источник энергии
        /// </summary>
        public string EnergySource { get; set; }

        /// <summary>
        /// Способ отображения времени
        /// </summary>
        public string DateDisplay { get; set; }

        /// <summary>
        /// Дополнительные функции
        /// </summary>
        public string AdditionalFunctions { get; set; }

        /// <summary>
        /// Гарантия товара
        /// </summary>
        public string Warranty { get; set; }

        /// <summary>
        /// Толщина часов, мм
        /// </summary>
        public double? Depth { get; set; }
                
        /// <summary>
        /// Диаметр часов, мм
        /// </summary>
        public double? Diameter { get; set; }

        /// <summary>
        /// Расстояние от ушек сверху до ушек снизу, мм
        /// </summary>
        public double? Height { get; set; }

        /// <summary>
        /// Вес часов
        /// </summary>
        public double? Weight { get; set; }

        /// <summary>
        /// Цена часов, руб.
        /// </summary>
        public double Price { get; set; }

        public Watch Merge(Watch otherWatch)
        { 
            return new Watch
            {
                Article = string.IsNullOrEmpty(Article) ? otherWatch.Article : Article,
                Model = string.IsNullOrEmpty(Model) ? otherWatch.Model : Model,
                Name = string.IsNullOrEmpty(Name) ? otherWatch.Name : Name,
                Brand = string.IsNullOrEmpty(Brand) ? otherWatch.Brand : Brand,
                Sex = Sex.HasValue ? Sex : otherWatch.Sex,
                Pictures = (Pictures == null || Pictures.Length == 0) ? otherWatch.Pictures : Pictures,
                Mechanism = Mechanism.HasValue ? Mechanism : otherWatch.Mechanism,
                Digits = string.IsNullOrEmpty(Digits) ? otherWatch.Digits : Digits,
                Watertight = Watertight.HasValue ? Watertight : otherWatch.Watertight,
                Waterproof = string.IsNullOrEmpty(Waterproof) ? otherWatch.Waterproof : Waterproof,
                BoxMaterial = string.IsNullOrEmpty(BoxMaterial) ? otherWatch.BoxMaterial : BoxMaterial,
                StrapMaterial = string.IsNullOrEmpty(StrapMaterial) ? otherWatch.StrapMaterial : StrapMaterial,
                Glass = string.IsNullOrEmpty(Glass) ? otherWatch.Glass : Glass,
                Display = string.IsNullOrEmpty(Display) ? otherWatch.Display : Display,
                EnergySource = string.IsNullOrEmpty(EnergySource) ? otherWatch.EnergySource : EnergySource,
                DateDisplay = string.IsNullOrEmpty(DateDisplay) ? otherWatch.DateDisplay : DateDisplay,
                AdditionalFunctions = string.IsNullOrEmpty(AdditionalFunctions) ? otherWatch.AdditionalFunctions : AdditionalFunctions,
                Warranty = string.IsNullOrEmpty(Warranty) ? otherWatch.Warranty : Warranty,
                Depth = Depth.HasValue ? Depth : otherWatch.Depth,
                Diameter = Diameter.HasValue ? Diameter : otherWatch.Diameter,
                Height = Height.HasValue ? Height : otherWatch.Height,
                Weight = Weight.HasValue ? Weight : otherWatch.Weight,
                Price = Price > 0.0 ? Price : otherWatch.Price
            };
        }
    }
}
