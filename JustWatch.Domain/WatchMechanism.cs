﻿namespace JustWatch.Domain
{
    public enum WatchMechanism
    {
        MechanicalAutomaticWindUp,
        MechanicalManualWindUp,
        Quartz
    }
}
