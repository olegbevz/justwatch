﻿namespace JustWatch.Trimpo
{
    public static class ShippingService
    {
        public const string OvernightDelivery = "RU_OvernightDelivery";
        public const string StandardDelivery = "RU_StandardDelivery";
        public const string EconomyDelivery = "RU_EconomyDelivery";
    }
}