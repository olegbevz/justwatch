﻿using System.Collections;
using System.Collections.Generic;

namespace JustWatch.Trimpo
{
    public class OrderCollection : IEnumerable<Order>
    {
        private readonly IEnumerable<Order> orders;

        public OrderCollection(IEnumerable<Order> orders, int totalAmount)
        {
            this.orders = orders;
            TotalAmount = totalAmount;
        }

        public int TotalAmount { get; private set; }
        public IEnumerator<Order> GetEnumerator()
        {
            return orders.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}