﻿using Newtonsoft.Json;

namespace JustWatch.Trimpo
{
    public class OrderAddress
    {
        [JsonProperty("name")]
        public string Recipient { get; set; }

        [JsonProperty("street1")]
        public string Street1 { get; set; }

        [JsonProperty("street2")]
        public string Street2 { get; set; }

        [JsonProperty("city_name")]
        public string City { get; set; }

        [JsonProperty("state_or_province")]
        public string State { get; set; }

        [JsonProperty("country_name")]
        public string Country { get; set; }

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }
    }
}