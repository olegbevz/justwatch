﻿using Newtonsoft.Json;

namespace JustWatch.Trimpo
{
    public class TrimpoResponse
    {
        [JsonProperty("error_code")]
        public int ErrorCode { get; set; }

        [JsonProperty("error_message")]
        public string ErrorMessage { get; set; }
    }
}