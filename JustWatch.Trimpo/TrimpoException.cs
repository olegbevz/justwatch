﻿using System;

namespace JustWatch.Trimpo
{
    public class TrimpoException : Exception
    {
        public int ErrorCode { get; private set; }

        public TrimpoException(int errorCode, string errorMessage) : base(errorMessage)
        {
            ErrorCode = errorCode;
        }
    }
}