﻿namespace JustWatch.Trimpo
{
    public enum OrderPaymentStatus
    {
        Complete,
        Pending,
        Incomplete
    }
}