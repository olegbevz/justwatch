﻿namespace JustWatch.Trimpo
{
    public enum OrderStatus
    {
        Active,
        Completed,
        Cancelled
    }
}