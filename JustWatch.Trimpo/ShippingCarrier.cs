﻿namespace JustWatch.Trimpo
{
    public static class ShippingCarrier
    {
        public const string RussianPost = "russianpost";
        public const string RuExpeditedMoscowOnly = "ruexpeditedmoscowonly";
        public const string EMS = "ems";
        public const string ShopDeliveryService = "shopdeliveryservice";
    }
}