﻿using System;

namespace JustWatch.Trimpo
{
    public class OrdersRequestOptions
    {
        /// <summary>
        /// order_id: <Order ID in sourse system, for example ebay>, - optional field
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// merchant_id: <Shop ID in trimpo>, - optional field//number
        /// </summary>
        public int? MerchantId { get; set; }

        /// <summary>
        /// ebay_profile_id: <Profile ID in trimpo>, - optional field//number
        /// </summary>
        public int? EbayProfileId { get; set; }

        /// <summary>
        /// created_date_from: <date and time of order creation in source system - yyyy-MM-dd HH:mm:ss>, - optional field//date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// created_date_to: <date and time of order creation in source system - yyyy-MM-dd HH:mm:ss>, - optional field//date
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// start_number: <number of lines that have to be skipped - by default - 0>, - optional field//number
        /// </summary>
        public int? StartNumber { get; set; }

        /// <summary>
        /// count_items: <number of lines in answer - by default - 10>, - optional field//number
        /// </summary>
        public int? ItemsCount { get; set; }

        /// <summary>
        /// sort_by: <field for sort - order_id|merchant_id|ebay_profile_id|created_date>, - optional field//line
        /// </summary>
        public string SortBy { get; set; }

        /// <summary>
        /// sort_order: <ASC|DESC> - optional field
        /// </summary>
        public string SortOrder { get; set; }
    }
}
