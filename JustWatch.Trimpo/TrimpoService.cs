﻿using JustWatch.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace JustWatch.Trimpo
{
    public class TrimpoService
    {
        private readonly Guid applicationId;
        private readonly Guid applicationSecret;
        private readonly string tradeSystem;

        private const string BaseUrl = "https://api.trimpo.org";
        private const string DateFormat = "ddd, dd MMM yyyy hh:mm:ss";
        private const string OrderList = "/api/public/order/list";
        private const string OrderShipping = "/api/public/order/shipping";
        private const string OrderPaymentApproval = "/api/public/order/paid";
        private const int ApiVersion = 1;

        public TrimpoService(string tradeSystem, Guid applicationId, Guid applicationSecret)
        {
            this.tradeSystem = tradeSystem;
            this.applicationId = applicationId;
            this.applicationSecret = applicationSecret;
        }

        public OrderCollection GetOrders(OrdersRequestOptions options)
        {
            var queryStringParameters = GetQueryStringParameters(options);
            
            var response = SendGetRequest<OrdersResponse>(OrderList, queryStringParameters);

            return new OrderCollection(response.Orders, response.Amount);
        }

        public void ProvideOrderShippingDetails(
            string orderId, 
            string trackingNumber, 
            DateTime shippedTime,
            string shippingCarrier)
        {
            if (string.IsNullOrEmpty(orderId))
                throw new ArgumentNullException("orderId");

            var parameters = new Dictionary<string, string>();
            parameters.Add("source", tradeSystem);
            parameters.Add("order_id", orderId);

            if (trackingNumber != null)
                parameters.Add("tracking_number", trackingNumber);

            var shippingDate = shippedTime.ToString("yyyy-MM-ddThh:mm:sszzz", CultureInfo.InvariantCulture);
            shippingDate = shippingDate.Substring(0, shippingDate.Length - 3) +
                shippingDate.Substring(shippingDate.Length - 2, 2);
            parameters.Add("shipped_time", shippingDate);

            if (shippingCarrier != null)
                parameters.Add("shipping_carrier_used", shippingCarrier);

            SendPostRequest(OrderShipping, parameters);
        }

        public void ApproveOrderPayment(string orderId, bool paymentApproval)
        {
            if (string.IsNullOrEmpty(orderId))
                throw new ArgumentNullException("orderId");

            var parameters = new Dictionary<string, string>
            {
                {"source", tradeSystem},
                {"order_id", orderId},
                {"paid", paymentApproval ? "TRUE" : "FALSE" }
            };

            SendPostRequest(OrderPaymentApproval, parameters);
        }

        private Dictionary<string, string> GetQueryStringParameters(OrdersRequestOptions options)
        {
            var queryStringParameters = new Dictionary<string, string>();

            queryStringParameters.Add("source", tradeSystem);

            if (options.OrderId != null)
                queryStringParameters.Add("order_id", options.OrderId);

            if (options.MerchantId != null)
                queryStringParameters.Add("merchant_id", options.MerchantId.ToString());

            if (options.EbayProfileId != null)
                queryStringParameters.Add("ebay_profile_id", options.EbayProfileId.ToString());

            if (options.StartDate != null)
                queryStringParameters.Add("created_date_from", options.StartDate.Value.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture));

            if (options.EndDate != null)
                queryStringParameters.Add("created_date_to", options.EndDate.Value.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture));

            if (options.StartNumber != null)
                queryStringParameters.Add("start_number", options.StartNumber.ToString());

            if (options.ItemsCount != null)
                queryStringParameters.Add("count_items", options.ItemsCount.ToString());

            if (options.SortBy != null)
                queryStringParameters.Add("sort_by", options.SortBy);

            if (options.SortOrder != null)
                queryStringParameters.Add("sort_order", options.SortOrder);

            return queryStringParameters;
        }

        private T SendGetRequest<T>(string requestUrl, Dictionary<string, string> parameters) where T : TrimpoResponse
        {
            var url = HttpUrl.Concat(BaseUrl, requestUrl, parameters);

            var httpRequest = HttpWebRequest.Create(url);

            httpRequest.Method = "GET";

            SetTrimpoHttpHeaders(httpRequest);

            var response = httpRequest.GetResponse() as HttpWebResponse;

            using (var responseStream = response.GetResponseStream())
            {
                using (var stringReader = new StreamReader(responseStream))
                {
                    var content = stringReader.ReadToEnd();
                    var orderResponse = JsonConvert.DeserializeObject<T>(content);

                    if (orderResponse.ErrorCode != 0)
                        throw new TrimpoException(orderResponse.ErrorCode, orderResponse.ErrorMessage);

                    return orderResponse;
                }
            }
        }

        private void SendPostRequest(string requestUrl, Dictionary<string, string> parameters)
        {
            var url = HttpUrl.Concat(BaseUrl, requestUrl);

            var httpRequest = HttpWebRequest.Create(url);

            httpRequest.Method = "POST";
            
            var requestContent = JsonConvert.SerializeObject(parameters);
            var byteArray = Encoding.UTF8.GetBytes(requestContent);
            using (var requestStream = httpRequest.GetRequestStream())
            {
                requestStream.Write(byteArray, 0, byteArray.Length);
            }

            SetTrimpoHttpHeaders(httpRequest, byteArray);

            var response = httpRequest.GetResponse() as HttpWebResponse;

            using (var responseStream = response.GetResponseStream())
            {
                using (var stringReader = new StreamReader(responseStream))
                {
                    var content = stringReader.ReadToEnd();
                    var orderResponse = JsonConvert.DeserializeObject<TrimpoResponse>(content);

                    if (orderResponse.ErrorCode != 0)
                        throw new TrimpoException(orderResponse.ErrorCode, orderResponse.ErrorMessage);
                }
            }
        }

        private void SetTrimpoHttpHeaders(WebRequest httpRequest, byte[] requestContent = null)
        {
            httpRequest.Headers.Add("X-MP30-Version", ApiVersion.ToString());
            httpRequest.Headers.Add("X-MP30-Date", DateTime.Now.ToString(DateFormat, CultureInfo.InvariantCulture));
            httpRequest.Headers.Add("X-MP30-Application-Id", applicationId.ToString());
            httpRequest.ContentType = "application/json";

            var token = string.Format(
                "{0}:{1}:{2}", 
                httpRequest.Method, 
                httpRequest.Headers["X-MP30-Date"], 
                httpRequest.RequestUri.AbsolutePath);

            if (requestContent != null)
                token += string.Format(":{0}", ComputeMd5Hash(requestContent));
            
            httpRequest.Headers.Add("X-MP30-Token", ComputeSha1Hash(token, applicationSecret));
        }

        private string ComputeMd5Hash(byte[] buffer)
        {
            using (var md5 = MD5.Create())
            {
                return ByteUtils.BufferToHexString(md5.ComputeHash(buffer));
            }
        }

        private string ComputeSha1Hash(string inputString, Guid secret)
        {
            var encoding = Encoding.UTF8;

            using (var hashAlgorythm = new HMACSHA1(encoding.GetBytes(secret.ToString("n"))))
            {
                var hash = hashAlgorythm.ComputeHash(encoding.GetBytes(inputString));

                return ByteUtils.BufferToHexString(hash);
            }
        }        
    }
}
