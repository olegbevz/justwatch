﻿using Newtonsoft.Json;

namespace JustWatch.Trimpo
{
    public class OrderItem
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("sku")]
        public string SKU { get; set; }

        [JsonProperty("item_id")]
        public string ItemId { get; set; }

        [JsonProperty("item_name")]
        public string ItemName { get; set; }

        [JsonProperty("offer_id")]
        public string OfferId { get; set; }

        [JsonProperty("transaction_id")]
        public string TransactionId { get; set; }
    }
}