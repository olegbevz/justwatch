﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace JustWatch.Trimpo
{
    public class OrdersResponse : TrimpoResponse
    {
        [JsonProperty("arrays")]
        public IList<Order> Orders { get; set; }

        [JsonProperty("amount")]
        public int Amount { get; set; }
    }
}
