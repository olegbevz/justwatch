﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace JustWatch.Trimpo
{
    public class Order
    {
        [JsonProperty("source")]
        public string System { get; set; }

        [JsonProperty("address")]
        public OrderAddress Address { get; set; }

        [JsonProperty("items")]
        public IEnumerable<OrderItem> Items { get; set; }

        [JsonProperty("total")]
        public double TotalPrice { get; set; }

        [JsonProperty("order_id")]
        public string OrderId { get; set; }

        [JsonProperty("status_order")]
        public OrderStatus Status { get; set; }

        [JsonProperty("status_paid")]
        public OrderPaymentStatus PaymentStatus { get; set; }

        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }

        [JsonProperty("shipping_service")]
        public string ShippingService { get; set; }

        [JsonProperty("shipping_cost")]
        public double ShippingCost { get; set; }

        [JsonProperty("shipping_carrier_used")]
        public string ShippingCarrier { get; set; }

        [JsonProperty("seller_id")]
        public string SellerId { get; set; }

        [JsonProperty("merchant_id")]
        public int MerchantId { get; set; }

        [JsonProperty("buyer_id")]
        public string BuyerId { get; set; }

        [JsonProperty("buyer_email")]
        public string BuyerEmail { get; set; }

        [JsonProperty("buyer_phone")]
        public string BuyerPhone { get; set; }

        [JsonProperty("sub_total")]
        public double PriceWithoutDelivery { get; set; }

        [JsonProperty("created_time")]
        public DateTime CreationTime { get; set; }

        [JsonProperty("paid_time")]
        public DateTime? PaymentTime { get; set; }
    }
}