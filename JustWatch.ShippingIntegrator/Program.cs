﻿using JustWatch.CDEK;
using JustWatch.CDEK.Calculator;
using JustWatch.Common;
using JustWatch.Trimpo;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using CDEKOrder = JustWatch.CDEK.DeliveryOrder;
using EbayOrder = JustWatch.Trimpo.Order;

namespace JustWatch.ShippingIntegrator
{
    class Program
    {
        private static TrimpoService trimpoService;
        private static CdekIntegrator cdekIntegrator;
        private static CdekCalculator cdekCalculator;
        private static readonly DeliveryOrderMapper mapper = new DeliveryOrderMapper();
        private static readonly int defaultLastDays = 4;

        static void Main(string[] args)
        {
            try
            {
                ConsoleUtils.WriteApplicationInfo();

                var parametersParser = new ConsoleParametersParser();
                var parameters = parametersParser.Parse(args);
                var lastDays = parameters.LastDays ?? defaultLastDays;

                InitShippingIntegrator();

                Console.WriteLine("Получаем активные заказы на ebay...");
                var activeOrders = GetActiveEbayOrders(lastDays);

                if (activeOrders.Length == 0)
                {
                    Console.WriteLine("За последние {0} дня на ebay нет активных заказов.", lastDays);
                    ConsoleUtils.AskToPressAnyKey();
                    return;
                }

                Console.WriteLine("За последние {0} дня на ebay есть {1} активных заказов", lastDays, activeOrders.Count());
                Console.WriteLine();

                var statusReport = GetDeliveryStatusReport();

                var ordersToDeliver = new Collection<EbayOrder>();
                foreach (var ebayOrder in activeOrders)
                {
                    DisplayOrder(ebayOrder);

                    if (CheckIfDeliveryWasAlreadyOrdered(ebayOrder, statusReport))
                    {
                        if (ConsoleUtils.Ask(string.Format("Задать для заказа №{0} трекинг-номер?", ebayOrder.OrderId)))
                        {
                            SetOrderShippingDetails(ebayOrder);
                        }

                        continue;
                    }

                    if (!CheckDeliveryPosts(ebayOrder))
                    {
                        Console.WriteLine(
                          "В городе {0} нет пунктов выдачи товара для оформления заказа №{1}",
                          ebayOrder.Address.City,
                          ebayOrder.OrderId);
                    }

                    // Display delivery conditions independently if post office is located in the city or no
                    DisplayDeliveryConditions(ebayOrder);
                    
                    if (!ConsoleUtils.Ask("Оформить доставку для данного заказа?"))
                        continue;

                    ordersToDeliver.Add(ebayOrder);
                }

                if (ordersToDeliver.Count == 0)
                {
                    Console.WriteLine("Отсутствуют заказы для оформления доставки");
                    ConsoleUtils.AskToPressAnyKey();
                    return;
                }

                Console.WriteLine("Для оформления доставки было выбрано {0} заказов", ordersToDeliver.Count);

                if (!ConsoleUtils.Ask("Продолжить оформление заказа на доставку?"))
                    return;
                
                Console.WriteLine("Создание заказа на доставку...");
                SendDeliveryRequest(ordersToDeliver);

                Console.WriteLine("Задаем трекинг-номера для заказов на Ebay...");
                SetOrderShippingDetails(ordersToDeliver);

                if (ConsoleUtils.Ask("Сохранить накладные для заказов на доставку?"))
                    PrintDeliveryOrders(ordersToDeliver);

                ConsoleUtils.AskToPressAnyKey();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
                ConsoleUtils.WriteException(ex);
                ConsoleUtils.AskToPressAnyKey();
            }
        }

        private static void DisplayDeliveryConditions(EbayOrder ebayOrder)
        {
            try
            {
                var deliveryCondition = cdekCalculator.Calculate(
                    DateTime.Now,
                    new City(AppSettings.SendCityPostCode),
                    new City(ebayOrder.Address.PostalCode),
                    new int[] { 137 },
                    3,
                    new CDEK.Calculator.Good(0.5, 10, 10, 10));

                Console.WriteLine($"Стоимость доставки товара: {deliveryCondition.Price} {deliveryCondition.Currency}");
                Console.WriteLine($"Время доставки товара: {deliveryCondition.DeliveryPeriodMin}-{deliveryCondition.DeliveryPeriodMax} дня(-ей)");
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);

                Console.WriteLine($"Не удалось определить условия доставки для заказа {ebayOrder.OrderId}");
                Console.WriteLine();
            }
        }

        private static void InitShippingIntegrator()
        {
            trimpoService = CreateTrimpoService();
            cdekIntegrator = CreateCdekIntegrator();
            cdekCalculator = CreateCdekCalculator();

            mapper.SellerAddress = AppSettings.SellerAddress;
            mapper.SendCityPostCode = AppSettings.SendCityPostCode;
            mapper.ShipperAddress = AppSettings.ShipperAddress;
            mapper.ShipperName = AppSettings.ShipperName;
        }

        private static void SendDeliveryRequest(Collection<EbayOrder> ebayOrders)
        {
            var currentTime = DateTime.Now;

            var deliveryOrders = ebayOrders.Select(x => mapper.MapForward(x)).ToArray();

            var actNumber = string.Format("JUSTWATCH {0:dd-MM-yy-hh-mm}", currentTime);

            var deliveryOrderStatuses = cdekIntegrator.SendDeliveryRequest(
                actNumber,
                currentTime,
                "RUB",
                deliveryOrders)
                .ToArray();

            for (int i = 0; i < ebayOrders.Count; i++)
            {
                var ebayOrder = ebayOrders[i];

                if (i >= deliveryOrderStatuses.Length)
                {
                    Console.WriteLine("Для заказа №{0} не был получен ответ о доставке.", ebayOrder.OrderId);
                    continue;
                }

                var deliveryResponse = deliveryOrderStatuses[i];

                if (deliveryResponse.Number != ebayOrder.OrderId)
                {
                    Console.WriteLine("Нарушена последовательность заказов в ответе");
                    continue;
                }

                if (deliveryResponse.DispatchNumber == null)
                {
                    Console.WriteLine("Не удалось оформить доставку для заказа №{0}", ebayOrder.OrderId);
                    if (deliveryResponse.Msg != null)
                        Console.WriteLine(deliveryResponse.Msg);
                    continue;
                }

                ebayOrder.TrackingNumber = deliveryResponse.DispatchNumber;
                ebayOrder.ShippingCarrier = ShippingCarrier.ShopDeliveryService;

                Console.WriteLine("Доставка заказа №{0} успешно оформлена.", ebayOrder.OrderId);
            }
        }

        private static void SetOrderShippingDetails(Collection<EbayOrder> orders)
        {
            foreach (var ebayOrder in orders)
            {
                if (ebayOrder.TrackingNumber == null)
                    continue;

                if (ebayOrder.ShippingCarrier == null)
                    continue;

                SetOrderShippingDetails(ebayOrder);
            }
        }

        private static void SetOrderShippingDetails(EbayOrder ebayOrder)
        {
            try
            {
                if (ebayOrder.TrackingNumber == null)
                    throw new ArgumentNullException(string.Format("Для заказа №{0} не указан трекинг-номер", ebayOrder.OrderId));

                if (ebayOrder.ShippingCarrier == null)
                    throw new ArgumentNullException(string.Format("Для заказа №{0} не указан способ доставки", ebayOrder.OrderId));

                trimpoService.ProvideOrderShippingDetails(
                    ebayOrder.OrderId,
                    ebayOrder.TrackingNumber,
                    DateTime.Now,
                    ebayOrder.ShippingCarrier);

                Console.WriteLine("Заказу №{0} был присвоен трекинг-номер {1}", ebayOrder.OrderId, ebayOrder.TrackingNumber);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Заказу №{0} не удалось присвоить трекинг-номер", ebayOrder.OrderId);
                ConsoleUtils.WriteException(ex);
                Logger.LogException(ex);
            }
        }

        private static void PrintDeliveryOrders(IEnumerable<EbayOrder> ebayOrders)
        {
            var currentAssembly = Assembly.GetExecutingAssembly();
            var directory = Path.GetDirectoryName(currentAssembly.Location);
            var printFolder = Path.Combine(directory, "print");

            if (!Directory.Exists(printFolder))
                Directory.CreateDirectory(printFolder);

            foreach (var ebayOrder in ebayOrders)
            {
                try
                {
                    if (ebayOrder.TrackingNumber == null)
                        continue;

                    int trackingNumber;
                    if (!int.TryParse(ebayOrder.TrackingNumber, out trackingNumber))
                        continue;

                    var printStream = cdekIntegrator.PrintOrders(DateTime.Now, 4, new[] { new OrderReference(trackingNumber) });

                    var filePath = Path.Combine(printFolder, string.Format("{0}.pdf", ebayOrder.OrderId));

                    using (var fileStream = File.Create(filePath))
                    {
                        printStream.CopyTo(fileStream);
                        fileStream.Flush();
                    }

                    Console.WriteLine("Заказ №{0} сохранен как '{1}'", ebayOrder.OrderId, filePath);
                }
                catch (Exception ex)
                {
                    ConsoleUtils.WriteException(ex);
                    Logger.LogException(ex);
                }
            }
        }

        private static EbayOrder[] GetActiveEbayOrders(int lastDays)
        {
            var startDate = DateTime.Now.Date.AddDays(-lastDays);

            var options = new OrdersRequestOptions { StartDate = startDate, ItemsCount = 10 };
            var orders = trimpoService.GetOrders(options);

            if (options.ItemsCount < orders.TotalAmount)
            {
                options.ItemsCount = orders.TotalAmount;
                orders = trimpoService.GetOrders(options);
            }

            var activeOrders = orders.Where(x =>
                x.TrackingNumber == null &&
                x.ShippingCarrier == null &&
                x.Status != OrderStatus.Cancelled &&
                x.PaymentStatus == OrderPaymentStatus.Complete).ToArray();

            return activeOrders;
        }

        private static void DisplayOrder(EbayOrder order)
        {
            Console.WriteLine("Заказ №{0}", order.OrderId);

            foreach (var orderItem in order.Items)
            {
                Console.WriteLine("Товар: {0}", orderItem.ItemName);
                Console.WriteLine("Стоимость: {0:F2} руб.", orderItem.Price);
                Console.WriteLine("Количество: {0}", orderItem.Quantity);
            }

            Console.WriteLine("Получатель: {0}", order.Address.Recipient);
            Console.WriteLine("Страна: {0}", order.Address.Country);
            Console.WriteLine("Город: {0}", order.Address.City);
            Console.WriteLine("Адресс: {0}", order.Address.Street1);
            Console.WriteLine("Способ доставки: {0}", order.ShippingService);
        }

        private static StatusReport GetDeliveryStatusReport()
        {
            var startDate = new DateTime(2016, 1, 1, 0, 0, 0);
            return cdekIntegrator.GetStatusReport(startDate, null, false);
        }

        private static bool CheckIfDeliveryWasAlreadyOrdered(EbayOrder ebayOrder, StatusReport statusReport)
        {
            var existingOrder = statusReport.Orders.FirstOrDefault(x => x.Number == ebayOrder.OrderId);
            if (existingOrder != null)
            {
                Console.WriteLine("Доставка для заказа №{0} уже оформлена", ebayOrder.OrderId);

                if (existingOrder.DispatchNumber != 0)
                {
                    ebayOrder.TrackingNumber = existingOrder.DispatchNumber.ToString();
                    ebayOrder.ShippingCarrier = ShippingCarrier.ShopDeliveryService;
                }
                
                return true;
            }

            return false;
        }

        private static TrimpoService CreateTrimpoService()
        {
            if (string.IsNullOrEmpty(AppSettings.TrimpoApplicationId))
                throw new Exception("Could not find trimpo application id in configuration file");

            if (string.IsNullOrEmpty(AppSettings.TrimpoApplicationSecret))
                throw new Exception("Could not find trimpo application secret in configuration file");

            return new TrimpoService(
                "ebay",
                new Guid(AppSettings.TrimpoApplicationId), 
                Guid.ParseExact(AppSettings.TrimpoApplicationSecret, "n"));
        }

        private static CdekIntegrator CreateCdekIntegrator()
        {
            if (string.IsNullOrEmpty(AppSettings.CdekAccount))
                throw new Exception("Could not find cdek account in configuration file");

            if (string.IsNullOrEmpty(AppSettings.CdekSecurePassword))
                throw new Exception("Could not find cdek password in configuration file");

            return new CdekIntegrator(
                Guid.ParseExact(AppSettings.CdekAccount, "n"),
                Guid.ParseExact(AppSettings.CdekSecurePassword, "n"));
        }

        private static CdekCalculator CreateCdekCalculator()
        {
            if (string.IsNullOrEmpty(AppSettings.CdekAccount))
                throw new Exception("Could not find cdek account in configuration file");

            if (string.IsNullOrEmpty(AppSettings.CdekSecurePassword))
                throw new Exception("Could not find cdek password in configuration file");

            return new CdekCalculator(
                Guid.ParseExact(AppSettings.CdekAccount, "n"),
                Guid.ParseExact(AppSettings.CdekSecurePassword, "n"));
        }

        private static bool CheckDeliveryPosts(EbayOrder order)
        {
            var deliveryPosts = cdekIntegrator.GetDeliveryPosts(null, order.Address.PostalCode, null, null);
            return deliveryPosts.Any();
        }
    }
}
