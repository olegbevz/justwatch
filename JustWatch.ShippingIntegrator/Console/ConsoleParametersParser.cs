﻿namespace JustWatch.ShippingIntegrator
{
    public class ConsoleParametersParser
    {
        private const string LastDaysParameter = "-days";

        public ConsoleParameters Parse(string[] args)
        {
            var importParameters = new ConsoleParameters();

            for (var i = 0; i < args.Length - 1; i+=2)
            {
                Parse(importParameters, args[i], args[i + 1]);
            }

            return importParameters;
        }

        private void Parse(ConsoleParameters parameters, string paramName, string paramValue)
        {
            switch (paramName)
            {
                case LastDaysParameter:
                    parameters.LastDays = int.Parse(paramValue);
                    break;
            }
        }
    }
}
