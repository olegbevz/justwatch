﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using JustWatch.CDEK;
using EbayOrder = JustWatch.Trimpo.Order;
using CDEKOrder = JustWatch.CDEK.DeliveryOrder;
using JustWatch.Common;

namespace JustWatch.ShippingIntegrator
{
    public class DeliveryOrderMapper : IMapper<CDEKOrder, EbayOrder>
    {
        private const int DeliveryTariffCode = 137;
        private const string SellerName = "justwatch";
        private const string ShopLink = "http://www.ebay.com/usr/justwatchrus";

        private const int MaxWeight = 500;
        
        public string SellerAddress { get; set; }
        public string ShipperName { get; set; }
        public string ShipperAddress { get; set; }
        public string SendCityPostCode { get; set; }

        public DeliveryOrder MapForward(EbayOrder ebayOrder)
        {
            var totalItemsCount = ebayOrder.Items.Select(x => x.Quantity).Sum();
            var itemWeight = (int)Math.Round(MaxWeight / (10.0 * totalItemsCount), MidpointRounding.ToEven) * 10;
            if (itemWeight <= 10)
                throw new Exception(string.Format("Заказ №{0} содежит недопустимое количество товаров для доставки.", ebayOrder));

            var deliveryItems = new List<DeliveryItem>();
            foreach (var ebayOrderItem in ebayOrder.Items)
            {
                var deliveryItem = new DeliveryItem()
                {
                    WareKey = ebayOrderItem.ItemId,
                    CostEx = ebayOrderItem.Price,
                    Cost = ebayOrderItem.Price,
                    PaymentEx = 0,
                    Payment = 0,
                    Weight = itemWeight,
                    WeightBrutto = itemWeight,
                    Amount = ebayOrderItem.Quantity,
                    CommentEx = ebayOrderItem.ItemName,
                    Comment = ebayOrderItem.ItemName,
                    Link = ShopLink
                };

                deliveryItems.Add(deliveryItem);
            }

            var deliveryPackage = new DeliveryPackage
            {
                Number = ebayOrder.OrderId,
                BarCode = ebayOrder.OrderId,
                Items = deliveryItems,
                Weight = deliveryItems.Sum(x => x.Weight)
            };

            var deliveryAddress = new DeliveryAddress
            {
                Street = ebayOrder.Address.Street1,
                House = "-",
                Flat = "-"
            };

            var deliveryOrder = new DeliveryOrder
            {
                Number = ebayOrder.OrderId,
                DateInvoice = DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                SendCityPostCode = SendCityPostCode,
                RecCityPostCode = ebayOrder.Address.PostalCode,
                RecipientName = ebayOrder.Address.Recipient,
                RecipientEmail = ebayOrder.BuyerEmail,
                Phone = ebayOrder.BuyerPhone,
                TariffTypeCode = DeliveryTariffCode,
                Comment = string.Empty,
                SellerName = SellerName,
                SellerAddress = SellerAddress,
                ShipperName = ShipperName,
                ShipperAddress = ShipperAddress,
                Address = deliveryAddress,
                Package = deliveryPackage
            };

            return deliveryOrder;
        }

        public EbayOrder MapBack(CDEKOrder target)
        {
            throw new NotImplementedException();
        }
    }
}