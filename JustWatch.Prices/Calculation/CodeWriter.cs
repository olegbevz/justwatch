﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JustWatch.Prices
{
    internal class CodeWriter
    {
        private readonly StreamWriter streamWriter;

        public CodeWriter(StreamWriter streamWriter)
        {
            this.streamWriter = streamWriter;
        }

        public void WriteNamespaceUsage(string @namespace)
        {
            streamWriter.WriteLine(string.Format("using {0};", @namespace));
        }

        public void WriteNamespaceDeclaration(string @namespace)
        {
            streamWriter.WriteLine(string.Format("namespace {0}", @namespace));
        }

        public void StartCodeBlock()
        {
            streamWriter.WriteLine("{");
        }

        public void EndCodeBlock()
        {
            streamWriter.WriteLine("}");
        }

        public void WriteClassDeclaration(string modifier, string @class)
        {
            streamWriter.WriteLine(string.Format("{0} class {1}", modifier, @class));
        }

        public void WriteMethodDeclaration(string modifier, string method, Type outputType, Dictionary<string, Type> inputParameters)
        {
            streamWriter.WriteLine(string.Format(
                "{0} {1} {2}({3})", 
                modifier, 
                outputType, 
                method, 
                string.Join(",", inputParameters.Select(x => string.Format("{0} {1}", x.Value, x.Key)))));
        }

        public void WriteCode(string code)
        {
            streamWriter.WriteLine(code);
        }
    }
}
