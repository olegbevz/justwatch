﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.CSharp;

namespace JustWatch.Prices
{
    internal class DelegatesGenerator
    {
        private const string NamespaceName = "GeneratedNamespace";
        private const string ClassNameTemplate = "GeneratedClass_{0}";
        private const string MethodNameTemplate = "GeneratedMethod_{0}";

        private int classesCounter = 0;

        public Func<double, double>[] GenerateDelegates(string[] expressions)
        {
            var code = GenerateClassWithMethods(expressions);

            var compiledAssembly = CompileCode(code);

            return GetGeneratedDelegates(compiledAssembly, expressions.Length);
        }

        private string GenerateClassWithMethods(string[] expressions)
        {
            classesCounter++;

            using (var memoryStream = new MemoryStream())
            {
                var streamWriter = new StreamWriter(memoryStream);
                var codeWriter = new CodeWriter(streamWriter);
                codeWriter.WriteNamespaceUsage("System");
                codeWriter.WriteNamespaceDeclaration(NamespaceName);
                codeWriter.StartCodeBlock();
                codeWriter.WriteClassDeclaration("public static", string.Format(ClassNameTemplate, classesCounter));
                codeWriter.StartCodeBlock();
                for (int i = 0; i < expressions.Length; i++)
                {
                    codeWriter.WriteMethodDeclaration(
                        "public static",
                        string.Format(MethodNameTemplate, i + 1),
                        typeof(double),
                        new Dictionary<string, Type> { { "x", typeof(double) } });

                    codeWriter.StartCodeBlock();
                    codeWriter.WriteCode(string.Format("return {0};", expressions[i]));
                    codeWriter.EndCodeBlock();
                }

                codeWriter.EndCodeBlock();
                codeWriter.EndCodeBlock();

                streamWriter.Flush();

                memoryStream.Seek(0, SeekOrigin.Begin);

                var streamReader = new StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
        }

        private Assembly CompileCode(string code)
        {
            var compParms = new CompilerParameters
            {
                GenerateExecutable = false,
                GenerateInMemory = true
            };

            var csProvider = new CSharpCodeProvider();
            CompilerResults compilerResults = csProvider.CompileAssemblyFromSource(compParms, code);
            if (compilerResults.Errors.Count > 0)
                throw new Exception(
                    "Error during code compilation: \r\n" + 
                    string.Join("\r\n", compilerResults.Errors.OfType<CompilerError>().Select(error => error.ErrorText).ToArray()));

            return compilerResults.CompiledAssembly;
        }

        private Func<double, double>[] GetGeneratedDelegates(Assembly assembly, int methodsCount)
        {
            var generatedType = assembly.GetType(NamespaceName + "." + string.Format(ClassNameTemplate, classesCounter));
           
            return Enumerable.Range(1, methodsCount)
                .Select(number => generatedType.GetMethod(string.Format(MethodNameTemplate, number)))
                .Where(method => method.IsStatic)
                .Select(method => CreateDelegate(method))
                .ToArray();
        }

        private Func<double, double> CreateDelegate(MethodInfo method)
        {
            var parameter = Expression.Parameter(typeof(double));
            var call = Expression.Call(method, parameter);
            return Expression.Lambda<Func<double, double>>(call, parameter).Compile();
        }
    }
}
