﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JustWatch.Prices
{
    public class PriceCalculator
    {
        private readonly Dictionary<string, Func<double, double>> brandFunctions = 
            new Dictionary<string, Func<double, double>>();

        private readonly DelegatesGenerator delegatesGenerator = new DelegatesGenerator();

        public PriceCalculator(IEnumerable<BrandFormula> brandFormulas)
        {
            var functions = delegatesGenerator.GenerateDelegates(brandFormulas.Select(x => x.Formula).ToArray());

            for (int i = 0; i < functions.Length; i++)
                brandFunctions.Add(brandFormulas.ElementAt(i).Brand.ToUpper(), functions[i]);
        }

        public double? CalculatePrice(string brand, double originalPrice)
        {
            if (brand == null)
                return null;

            brand = brand.ToUpper();

            if (!brandFunctions.ContainsKey(brand))
                return null;

            var function = brandFunctions[brand];

            return function(originalPrice);
        }
    }
}
