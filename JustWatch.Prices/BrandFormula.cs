﻿namespace JustWatch.Prices
{
    public class BrandFormula
    {
        public string Brand { get; set; }

        public string Formula { get; set; } 
    }
}
