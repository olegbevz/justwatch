﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Excel;

namespace JustWatch.Delta
{
    public class PricesReader
    {
        private static readonly string RublesPostFix = " руб.";

        private readonly string pricesFolder;

        public PricesReader(string pricesFolder)
        {
            this.pricesFolder = pricesFolder;
        }

        public IEnumerable<PriceRecord> ReadPrices()
        {
            var pricesDocumentsPaths = Directory.GetFiles(pricesFolder, "*.xls");

            foreach (var fileName in pricesDocumentsPaths)
            {
                var brand = GetShortBrandName(fileName);

                using (var fileStream = File.OpenRead(fileName))
                {
                    using (var excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        while (excelReader.Read())
                        {
                            var model = excelReader.GetString(0);
                            var priceString = excelReader.GetString(1);
                            if (priceString == null)
                                continue;

                            priceString = priceString.Replace(RublesPostFix, string.Empty);

                            double price;
                            if (!double.TryParse(priceString, out price))
                                continue;

                            yield return new PriceRecord { Model = model, Price = price, Brand = brand };
                        }
                    }
                }
            }
        }

        private string GetShortBrandName(string fileName)
        {
            return new string(Path.GetFileNameWithoutExtension(fileName)
                .TakeWhile(@char => @char != '_').ToArray()).ToUpper();
        }
    }
}
