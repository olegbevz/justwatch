namespace JustWatch.Delta
{
    public class PriceRecord
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public double Price { get; set; }
    }
}